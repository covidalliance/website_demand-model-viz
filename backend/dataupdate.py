# ----------------------------------------------------------------------- #

# CovidVis

# File:         dataupdate.py
# Maintainer:   Clara
# Last Updated: June 2020
# Language:     Python 3.7

# ------------------------------------------------------------------------ #

# ------------------------------------------------------------------------ #
# Initialization
# ------------------------------------------------------------------------ #

# Dependency settings
# ---------------------------------------------#
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Load dependencies
# ---------------------------------------------#
import os
import sys

import pandas as pd
import numpy as np
import datetime as dt
import pickle as pkl
import torch
from datetime import datetime 

# Load internal dependencies
# ---------------------------------------------#
import modelprocessing as mp
import dataprocessing as dp

# ------------------------------------------------------------------------ #
# Define Functions
# ------------------------------------------------------------------------ #

# data_initialize
# ---------------------------------------------#
def data_initialize(s3_connection, xwalk):

	## Update date
	update_date          = datetime.now()
	update_date          = update_date.strftime("%b %d %Y %H:%M:%S UTC")

	## Download files
	s3_connection.download_file('covid-predictions', 'SIR/website_input/model.pt', 'data/model.pt')
	s3_connection.download_file('covid-predictions', 'SIR/website_input/data.pkl', 'data/data.pkl')

	## Load files
	model       = torch.load('data/model.pt')
	with open('data/data.pkl', 'rb') as f:
		county_data = pkl.load(f)

	## Generate baseline predictions
	### Parameters
	scenario_start_date  = dt.timedelta(days=30)+dt.date.today() # Date does not matter given infinite half life
	half_life            = int(100000000)

	print(scenario_start_date)
	### Generate
	pred_baseline        = mp.predict("", model, county_data, scenario_start_date, half_life)

	### Obtain dates
	latest_actual_date   = np.min(pred_baseline['actual'][~np.isnan(pred_baseline['actual']['value'])].groupby("fips")['date'].max())
	start_date           = county_data.start_date
	
	#### Format 
	pred_baseline        = mp.countystate(pred_baseline['predicted'])

	### Generate crosswalk
	crosswalk            = dp.process_county_data_crosswalk(pred_baseline, xwalk)

	### Define half life dictionary
	relax_speed_dict       = {}
	relax_speed_dict['1']  = "Immediate"
	relax_speed_dict['15'] = "Fast"
	relax_speed_dict['30'] = "Moderate"
	relax_speed_dict['60'] = "Slow"

	## Return
	return([model, county_data, pred_baseline, crosswalk, latest_actual_date, start_date, update_date, relax_speed_dict])

# ------------------------------------------------------------------------ #
# ------------------------------------------------------------------------ #
