# ----------------------------------------------------------------------- #

# CovidVis

# File:         modelprocessing.py
# Maintainer:   CovidAlliance (Modified by Clara)
# Last Updated: June 2020
# Language:     Python 3.7

# ------------------------------------------------------------------------ #

# ------------------------------------------------------------------------ #
# Initialization
# ------------------------------------------------------------------------ #

# Dependency settings
# ---------------------------------------------#
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Load dependencies
# ---------------------------------------------#

# Load external dependencies
# ---------------------------------------------#
import sys
import os

import datetime as dt
import numpy as np
import pandas as pd

import itertools as it
import pickle as pkl

from sacred import Experiment
import torch
from tqdm import tqdm

# Load internal dependencies
# ---------------------------------------------#
from covid_demand import utils

# ------------------------------------------------------------------------ #
# Define Functions
# ------------------------------------------------------------------------ #

# predict
# ---------------------------------------------#
def predict(state, model, county_data, scenario_start_date, half_life,
            forecast_days=120, dynamic_feature_lag=7):

    x, y, _, fips = county_data.get_cached_data(return_full_x=True)
    county_names = [county_data.fips_to_name(fip) for fip in fips]
    early_mobility_date = dt.date(2020, 3, 1)
    feature_names = county_data.feature_names
    stay_home_index = feature_names.index('stay_home_ratio_smooth_rel')

    pred = get_predictions(state, model, x, y, county_data, county_names, fips,
                    scenario_start_date, early_mobility_date, half_life,
                    forecast_days, stay_home_index, dynamic_feature_lag)

    # Helper function
    def add_daily(pred):
        pred['daily'] = pred.sort_values('date').groupby('fips').value.diff()

    # Add daily
    add_daily(pred['predicted']) 
    add_daily(pred['actual'])

    return pred

# countystate
# ---------------------------------------------#  
def countystate(pred):
    
    pred           = pd.concat([pred, pred['county'].str.split(",", expand=True)], axis=1)
    pred['state']  = pred[1]
    pred['county'] = pred[0]
    
    return pred

# get_predictions
# ---------------------------------------------#
def get_predictions(state, model, x, y, county_data, county_names, fips,
                    scenario_start_date, early_mobility_date,
                    half_life, forecast_days, stay_home_index,
                    dynamic_lag):
    
    lag = dt.timedelta(days=dynamic_lag)
    last_predicted_date = county_data.start_date.date() + dt.timedelta(days=x.shape[0])
    
    # How long we have to repeat the last day of features.
    n_stable = (scenario_start_date + lag - last_predicted_date).days
    
    # The index that represents "early mobility".
    idx_low = (early_mobility_date + lag - county_data.start_date.date()).days
    x_ext = utils.extend_w_scenario(
        x, n_stable=n_stable, n=forecast_days,
        dynamic_feature_indices=(stay_home_index),
        idx_low=idx_low,
        half_life=half_life,
        )
    y_hat = model(x_ext)['county_cases']

    def get_unstandardized_stay_home(x_ext, county_data, stay_home_index):
        mu = county_data.standardize_mu[:, :,stay_home_index]
        sigma = county_data.standardize_sigma[:, :,stay_home_index]
        stay_home = x_ext[:, :, stay_home_index]
        return stay_home * sigma + mu
    stay_home = get_unstandardized_stay_home(x_ext, county_data, stay_home_index)

    # Compute R0 and Rt
    sir = model.body
    a = sir.link_a.forward(x_ext).exp()
    gamma = sir.gamma
    p = sir.p * sir.get_scale()
    Rt = a / gamma * (1 - y_hat / p)
    R0 = a / gamma

    sd = county_data.start_date

    if (state!=''):

        cs = np.array(county_names)
        state_idx = np.where(np.char.find(cs, state)>=0)[0]
        state_counties = cs[state_idx]
        state_fips = np.array(fips)[state_idx]
        all_predictions = dict(
            stay_home_ratio=utils.format_predictions(stay_home[:, state_idx], state_counties, sd, fips=state_fips, dataset="stay_home_ratio"),
            predicted=utils.format_predictions(y_hat[:, state_idx], state_counties, sd, fips=state_fips, dataset="predicted"),
            R_t=utils.format_predictions(Rt[:, state_idx], state_counties, sd, fips=state_fips, dataset="R_t"),
            actual=utils.format_predictions(utils.negative_to_na(y['cases_cum'])[:, state_idx], state_counties, sd, fips=state_fips, dataset="actual")
            )
    else:

        all_predictions = dict(
            stay_home_ratio=utils.format_predictions(stay_home, county_names, sd, fips=fips, dataset="stay_home_ratio"),
            predicted=utils.format_predictions(y_hat, county_names, sd, fips=fips, dataset="predicted"),
            R_t=utils.format_predictions(Rt, county_names, sd, fips=fips, dataset="R_t"),
            actual=utils.format_predictions(utils.negative_to_na(y['cases_cum']), county_names, sd, fips=fips, dataset="actual")
            )

    return all_predictions

# ------------------------------------------------------------------------ #
# ------------------------------------------------------------------------ #

