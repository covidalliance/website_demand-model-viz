# ----------------------------------------------------------------------- #

# CovidVis

# File:         dataprocessing.py
# Maintainer:   Clara
# Last Updated: June 2020
# Language:     Python 3.7

# ------------------------------------------------------------------------ #

# ------------------------------------------------------------------------ #
# Initialization
# ------------------------------------------------------------------------ #

# Dependency settings
# ---------------------------------------------#
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Load dependencies
# ---------------------------------------------#
import pandas as pd
import numpy as np
import datetime as dt
import re

# Load internal dependencies
# ---------------------------------------------#
import modelprocessing as mp

# ------------------------------------------------------------------------ #
# Define Functions
# ------------------------------------------------------------------------ #

# process_county_data_crosswalk
# ---------------------------------------------#
def process_county_data_crosswalk(county_data, xwalk):

	# Generate crosswalk 
	county_data_crosswalk = county_data[['county','state','fips']]
	
	county_data_crosswalk = county_data_crosswalk.drop_duplicates()
	
	# Rename columns - crosswalk & state xwalk
	county_data_crosswalk.columns = ['county','state','county_id']
	xwalk.columns                 = ['state_name','state','state_fips']
	
	# Merge crosswalk & state xwalk
	county_data_crosswalk         = pd.merge(county_data_crosswalk, xwalk, on="state")
	
	# Sort generated crosswalk by state
	county_data_crosswalk         = county_data_crosswalk.sort_values(['state_name', 'county'])
	
	# Return the results
	return(county_data_crosswalk)

# process_county_data
# ---------------------------------------------#
def process_county_data(state, county_data, start_date, county_start_date, baseline, latest_actual_date, half_life, relax_speed_dict):

	# Rename columns
	baseline.rename(columns={'value': 'baseline_cumulative', 'daily': 'baseline_daily'}, inplace=True)
	county_data["actual"].rename(columns={'value': 'actual_cumulative', 'daily': 'actual_daily'}, inplace=True)
	county_data["R_t"].rename(columns={'value': 'scenario_R_t'}, inplace=True)
	county_data["predicted"].rename(columns={'value': 'scenario_cumulative', 'daily': 'scenario_daily'}, inplace=True)
	county_data["stay_home_ratio"].rename(columns={'value': 'stay_home_ratio'}, inplace=True)
	
	# Append columns - predictions & baseline
	county_data_subset                         = county_data["predicted"][["date","county","fips","scenario_cumulative","scenario_daily"]]
	county_data_subset['scenario_R_t']         = county_data["R_t"]['scenario_R_t']
	county_data_subset['stay_home_ratio']      = county_data["stay_home_ratio"]['stay_home_ratio'] 
	
	county_data_subset.reset_index(inplace=True)
	baseline.reset_index(inplace=True)

	county_data_subset['baseline_cumulative']  = baseline["baseline_cumulative"]
	county_data_subset['baseline_daily']       = baseline["baseline_daily"]
	
	# Append columns - actuals
	county_data_subset                   = pd.merge(county_data_subset, county_data["actual"][["date", "actual_cumulative", "actual_daily","fips"]], on=["date", "fips"], how="left")

	# Generate county & state
	county_data_subset                   = mp.countystate(county_data_subset)

	# Calculate aggregate numbers
	county_data_subset['temp_index']     = np.array(county_data_subset[['fips']].groupby("fips").cumcount())
	county_data_subset_agg               = county_data_subset[['temp_index','date', 'scenario_cumulative','scenario_daily','baseline_cumulative', 'baseline_daily','actual_cumulative','actual_daily']].groupby(["temp_index","date"]).sum().reset_index()
	county_data_subset_agg               = county_data_subset_agg.drop(["temp_index"], axis=1)
	county_data_subset_avg  		     = county_data_subset[['temp_index','date','scenario_R_t','stay_home_ratio']].groupby(["temp_index","date"]).mean().reset_index()
	county_data_subset_avg  		     = county_data_subset_avg.drop(["temp_index"], axis=1)
	county_data_subset_comb              = pd.merge(county_data_subset_avg, county_data_subset_agg, on="date")
	county_data_subset_comb['state']     = state
	county_data_subset_comb['county']    = 'ZZAggregate'
	county_data_subset_comb['fips']      = 0
	
	# Merge in aggregates
	county_data_subset = county_data_subset.drop(["temp_index"], axis=1)
	county_data_subset = county_data_subset.sort_values(['state', 'county'])
	county_data_subset = pd.concat([county_data_subset, county_data_subset_comb], sort=False)

	# Format actual numbers
	county_data_subset.loc[county_data_subset['date']>latest_actual_date,'actual_cumulative'] = int(-1)
	county_data_subset.loc[county_data_subset['date']>latest_actual_date,'actual_daily']      = int(-1)

	# Drop variables
	county_data_subset = county_data_subset.drop([0], axis=1)
	county_data_subset = county_data_subset.drop([1], axis=1)
	county_data_subset = county_data_subset.drop(["index"], axis=1)

	# Append selected model variables 
	county_data_subset['relax_date']            = start_date

	# Generate half life
	county_data_subset['relax_speed'] = relax_speed_dict[str(half_life)]

	# Order
	county_data_subset = county_data_subset[['state', 'county','fips', 'date', 'actual_daily','actual_cumulative','relax_date','relax_speed','stay_home_ratio','baseline_daily','baseline_cumulative','scenario_daily','scenario_cumulative', 'scenario_R_t']]

	# Return the results
	return(county_data_subset)


# ------------------------------------------------------------------------ #
# ------------------------------------------------------------------------ #

