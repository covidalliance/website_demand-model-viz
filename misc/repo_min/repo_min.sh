du -h | sort -h

# Enter folder
cd covid-demand

# Delete files
## archive
rm -rf archive

## external-models
rm -rf external-models

## features
rm -rf features

## covid-demand
rm covid_demand/data/normalized_static_data.csv

## data
rm -rf data/reference
rm -rf data/GATech_model
rm -rf data/GA_PUI_data
rm -rf data/GA_modified_medfacility_tracker
rm -rf data/GA_los
rm -rf data/GA_extra_beds
rm -rf data/GA_county_to_region
rm -rf data/GA_capacity
rm -rf data/GA_actual_hosp

## results
rm -rf results/wsj_scenarios
rm -rf results/GA_regions
rm -rf results/SI
rm -rf results/SIR/2020-04-20
rm -rf results/SIR/2020-04-25
rm -rf results/SIR/2020-04-28
rm -rf results/SIR/2020-04-29
rm -rf results/SIR/2020-04-30
rm -rf results/SIR/2020-05-01
rm -rf results/SIR/2020-05-07
rm -rf results/SIR/2020-05-09
rm -rf results/SIR/2020-05-12
rm results/predictions.csv
rm results/predictions_20200411_6pm.csv
rm results/mock_stats_byCounty.csv
rm results/coefs_byCounty.csv
rm results/best_fit_curves_byCounty.csv

## scripts
rm -rf scripts/fit_covariates

## hospitalization
rm -rf hospitalization

## test
rm -rf test

## validation
rm -rf validation

## visualization
rm -rf visualization

