# ----------------------------------------------------------------------- #

# CovidVis

# File:         application.py
# Maintainer:   Clara
# Last Updated: 2020-05-18
# Language:     Python 3.7

# ------------------------------------------------------------------------ #

# ------------------------------------------------------------------------ #
# Initialization
# ------------------------------------------------------------------------ #

# Dependency settings
# ---------------------------------------------#
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

# Load external dependencies
# ---------------------------------------------#
import os
import sys

import flask
from flask_caching import Cache
import pandas as pd
import numpy as np
import datetime as dt
import pickle as pkl
import torch
import boto3
import schedule ## locally saved version

import time
from datetime import datetime 

# External dependency settings
# ---------------------------------------------#
pd.options.mode.chained_assignment = None

# Load internal dependencies
# ---------------------------------------------#
sys.path.append('backend')
import dataprocessing as dp
import modelprocessing as mp
import dataupdate as du

# ------------------------------------------------------------------------ #
# Load data
# ------------------------------------------------------------------------ #

# crosswalk
# ---------------------------------------------#
xwalk = pd.read_csv("data/state_crosswalk.csv", encoding="utf-8", header=0)

# AWS access
# ---------------------------------------------#
s3 = boto3.client('s3')


# ------------------------------------------------------------------------ #
# Initialize data update job & flask app + cache
# ------------------------------------------------------------------------ #

# Schedule data updates
# ---------------------------------------------#

## Define global 
default_data = ""

## Define update job
def job():
	print("Starting data update")
	global default_data
	## Clear cache
	#with application.app_context():
	#	cache.clear()
	## Update data 
	default_data = du.data_initialize(s3, xwalk)
	print("Finished data update")

## Schedule job
# schedule.every().day.at("06:00").do(job)
schedule.every(12).hours.do(job)

## Schedule update every 24 hours
schedule.run_continuously()

# Initialize the data
# ---------------------------------------------#
schedule.run_all()

# Initialize the flask application
# ---------------------------------------------#
application = flask.Flask(__name__, 
	template_folder = 'frontend', 
	static_folder = 'frontend')

# Initialize the cache
# ---------------------------------------------#
config = {
	"DEBUG": True,          
	"CACHE_TYPE": "simple", 
	"CACHE_DEFAULT_TIMEOUT": 300
}
application.config.from_mapping(config)
cache = Cache(application)

# ------------------------------------------------------------------------ #
# Define views
# ------------------------------------------------------------------------ #
	
# index
# ---------------------------------------------#
@application.route('/')
@cache.cached(timeout=300)
def index_view():

	## Obtain data
	update_date        = default_data[6]

	## Render home page
	return flask.render_template('index.html', update_date = update_date)

# load_county_data_crosswalk
# ---------------------------------------------#
@application.route('/load_county_data_crosswalk')
@cache.cached(timeout=300)
def load_county_data_crosswalk_view():
	
	## Obtain data
	crosswalk          = default_data[3]

	## Render home page
	return crosswalk.to_csv()

# load_county_data
# ---------------------------------------------#
@application.route('/load_county_data')
@cache.cached(timeout=300,query_string=True)
def load_county_data_view():

	## Obtain data
	model              = default_data[0]
	county_data        = default_data[1]
	pred_baseline      = default_data[2]
	latest_actual_date = default_data[4]
	start_date         = default_data[5]
	relax_speed_dict   = default_data[7]

	## Inspect the request
	print(flask.request.args.get('state'))
	print(flask.request.args.get('relax_year'))
	print(flask.request.args.get('relax_month'))
	print(flask.request.args.get('relax_day'))
	print(flask.request.args.get('relax_speed'))

	## Format parameters
	scenario_start_date  = dt.date(int(flask.request.args.get('relax_year')), int(flask.request.args.get('relax_month')), int(flask.request.args.get('relax_day')))
	half_life            = int(flask.request.args.get('relax_speed'))
	state                = flask.request.args.get('state')
	
	## Generate predictions
	pred = mp.predict(state, model, county_data, scenario_start_date, half_life)
	
	## Format predictions
	county_data_subset = dp.process_county_data(state=state, county_data=pred,
		start_date = scenario_start_date, county_start_date=start_date, 
		baseline=pred_baseline[pred_baseline['state']==state],
		latest_actual_date = latest_actual_date, 
		half_life = half_life, relax_speed_dict=relax_speed_dict)

	# Return data
	return county_data_subset.to_csv()


# ------------------------------------------------------------------------ #
# Launch application
# ------------------------------------------------------------------------ #
if __name__ == "__main__":
	application.run(host='0.0.0.0')

# ------------------------------------------------------------------------ #
# ------------------------------------------------------------------------ #
