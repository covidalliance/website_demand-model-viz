from setuptools import setup

setup(name='covid_demand',
      version='0.1',
      description='',
      license='MIT',
      packages=['covid_demand'],
      install_requires=['torch', 'funcy', 'tqdm', 'sacred',
                        'numpy', 'pandas'],
      zip_safe=False)
