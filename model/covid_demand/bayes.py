"Priors, likelihoods, etc."
import funcy as f
import torch
import torch.distributions as tdistr
import torch.nn as nn


class BayesianParameter(nn.Parameter):
    "nn.Parameter, with additional prior likelihood."
    def __new__(cls, *args, data=None, requires_grad=True, **kwargs):
        return super().__new__(cls, data=data, requires_grad=requires_grad)

    def __init__(self, distr, data=None, requires_grad=True, **params):
        self.distr = distr
        self.params = params

    def log_likelihood(self, **kwargs):
        params = f.merge(self.params, kwargs)
        return self.distr(**params).log_prob(self)

    @classmethod
    def lognormal(cls, **kwargs):
        return cls(tdistr.LogNormal, **kwargs)

    @classmethod
    def normal(cls, **kwargs):
        return cls(tdistr.Normal, **kwargs)
   

def prior_log_likelihood(module, llh_kwargs=None):
    """
    Sums the log likelihoods of all BayesianParameters in a module.

    :param llh_kwargs: A nested dict {param_name: {kwarg_name: kwarg_val}}
    that passes extra kwargs to the llh fn of a parameter. Useful
    for complicated dependencies between priors, e.g. when the llh
    of some param depends on a function of another param.
    """
    llh_kwargs = dict() if llh_kwargs is None else llh_kwargs
    llhs = [param.log_likelihood(**llh_kwargs.get(name, dict())).sum()
            for name, param in module.named_parameters()
            if isinstance(param, BayesianParameter)]
    return sum(llhs) if len(llhs) > 0 else torch.tensor(0.)
