import copy
import warnings

import funcy as f
import numpy as np
import pandas as pd
import torch
from tqdm import trange

import covid_demand.utils as utils
import covid_demand.loss as dloss


def is_valid_model(m):
    summary = utils.summarise_model(m)
    if np.isnan(summary["param_norm"]):
        return False
    else:
        return True


def get_evaluator(loss_fn, optimizer, model, x, y):
    def evaluate(diagnostics=False):
        optimizer.zero_grad()
        y_hat = model(x)  # Get predictions
        if diagnostics:
            if isinstance(loss_fn, dloss.WeightedLoss):
                loss = loss_fn(model, y, y_hat, reduce=False)
            else:
                loss = dict(loss=loss_fn(model, y, y_hat))
        else:
            loss = loss_fn(model, y, y_hat)
            loss.backward()
        return loss
    return evaluate


def get_diagnoser(evaluator, model):
    "Summarise loss and model parameters,"
    def diagnose():
        model_summary = utils.summarise_model(model)
        loss = evaluator(diagnostics=True)
        loss_summary = f.walk_values(lambda x: x.item(), loss)
        return f.merge(model_summary, loss_summary)
    return diagnose


def get_optimizer(opt, params, **kwargs):
    ctors = dict(lbfgs=torch.optim.LBFGS,
                 asgd=torch.optim.ASGD,
                 sgd=torch.optim.SGD,
                 adam=torch.optim.Adam)
    if opt in ctors.keys():
        return ctors[opt](params, **kwargs)
    else:
        return opt


def train(model, iters=1e2,
          loss_fn=None, x=None, y=None, evaluator=None,  # Evaluator
          return_best=True, diagnoser=None, optimizer="lbfgs",
          lr=1e-2, checkpoint_iters=1):
    """
    Trains a model and returns the minimum loss iteration.

    Must either provide an evaluator closure, or args (loss_fn, x, y).
    """
    logs = []
    pbar = trange(int(iters))
    checkpoint = copy.deepcopy(model.state_dict())
    best_loss = float("Inf")

    optimizer = get_optimizer(optimizer, model.parameters(), lr=lr)

    if evaluator is None:
        evaluator = get_evaluator(loss_fn, optimizer, model, x, y)

    if diagnoser is None:
        diagnoser = get_diagnoser(evaluator, model)

    for i in pbar:
        loss = optimizer.step(evaluator)
        if torch.isnan(loss) or torch.isinf(loss):
            warnings.warn("Loss is NaN; stopping training.")
            # raise(Exception("Model has NaN parameters; stopping training."))
            break
        if i % checkpoint_iters == 0:
            log = diagnoser()
            if not is_valid_model(model):
                warnings.warn("Model has NaN parameters; stopping training.")
                # raise(Exception("Loss is NaN; stopping training."))
                break
            if loss.item() < best_loss:
                checkpoint = copy.deepcopy(model.state_dict())
                best_loss = loss.item()
            log["t"] = i
            logs.append(log)
        pbar.set_description("Loss: %.3e" % loss.item())

    model.load_state_dict(checkpoint)
    return model, pd.DataFrame.from_dict(logs)
