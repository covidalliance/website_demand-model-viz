import warnings
import re

import funcy as f
import torch
import torch.nn as nn
import torch.nn.functional as F

import covid_demand.utils as utils


def mse_loss(model, y, yhat, weight=None, mask=None):
    if mask is not None:
        y, yhat, weight = utils.apply_mask(mask, y, yhat, weight)
    losses = F.mse_loss(y, yhat, reduction="none")
    if weight is not None:
        losses = losses * weight
    return losses.mean()


def lognormal_loss(model, y, yhat, **kwargs):
    return mse_loss(model, y.log(), yhat.log(), **kwargs)


def regex_l2_loss(model, y, yhat, expr="", mean=0.):
    "Gaussian prior on all named parameters matching expr"
    losses = [((param - mean) ** 2).sum()
              for name, param in model.named_parameters()
              if re.search(expr, name) is not None]
    if len(losses) == 0:
        warnings.warn("Regex L2 Loss: no parameters matching expr %s"
                      % expr)
        return torch.tensor(0.)
    return torch.stack(losses).sum()


def use_named_outputs(loss, name):
    """
    Some models produce dicts as outputs. This decorator extracts
    `name` from y and yhat, and applies `loss` to them.
    """
    def named_loss(model, y, yhat, **kwargs):
        return loss(model, y[name], yhat[name])
    return named_loss


class WeightedLoss(nn.Module):
    def __init__(self, losses, weights, names=None):
        super(WeightedLoss, self).__init__()
        assert len(losses) == len(weights)
        assert names is None or len(names) == len(losses)
        self.loss = f.juxt(*losses)
        losses = losses
        self.weights = weights
        self.names = (["loss_%d" % i for i in range(len(losses))]
                      if names is None else names)

    def forward(self, *args, reduce=True):
        losses = {name: wt * loss for name, wt, loss
                  in zip(self.names, self.weights, self.loss(*args))}

        if any(map(lambda x: torch.isnan(x) or torch.isinf(x),
                   losses.values())):
            warnings.warn("Invalid loss: %s" % losses)

        if reduce:
            return sum(losses.values())
        else:
            return losses
