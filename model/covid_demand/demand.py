"""
Models specific to COVID demand.
Assumes convention that tensors are shaped (time, geos, features).
"""
import numpy as np
import torch
import torch.nn as nn
from typing import List
from torch import Tensor

from covid_demand import core, train, conv
import covid_demand.loss as dloss


class Censor(nn.Module):
    def __init__(self, tests):
        super(Censor, self).__init__()
        T, d = tests.shape
        self.sick_not_covid = nn.Parameter(torch.ones((1, d)) * 1000)
        self.tests = tests

    def forward(self, x):
        return self.tests * x / (self.sick_not_covid + x)


def initialize_link_fn(link, x, target, lr=1e-1, iters=50,
                       optimizer="lbfgs", **kwargs):
    "Match the output of a link function to some target."
    if not isinstance(target, torch.Tensor):
        target = target * torch.ones_like(link(x))
    train.train(link, iters, loss_fn=dloss.mse_loss, x=x,
                y=target, optimizer=optimizer, lr=lr, **kwargs)


class SIR(nn.Module):
    """
    Implements a discretized SIR model, where a is constant within
    each timestep.

    Follows TorchScript JIT examples: https://bit.ly/3aIlFFc
    """
    def __init__(self, n_geos, p, link_a, 
                 scale_default, alpha_init=1., 
                 one_scale_param=False, learn_scale_mask=None,
                 y0=1., gamma=0.1):
        """
        `scale_default` vector of length n_geos, which specifies
          the initialization of the scale parameter.
        `learn_scale_mask` is a boolean vector of length n_geos, which is True
         in the positions in which the scale should be learned.
        If `one_scale_param` is true, learns a scalar alpha parameter
         that is used for every region.
        """
        super(SIR, self).__init__()
        self.y0 = nn.Parameter(torch.ones((1, n_geos)) * y0)
        self.link_a = link_a
        self.p = p
        self.gamma = gamma
        self.lag_diff = conv.Differencer()
        self.fixed_scale = nn.Parameter(scale_default, requires_grad=False)
        self.learned_scale = nn.Parameter(scale_default, requires_grad=True)
        self.learn_scale_mask = learn_scale_mask
        if learn_scale_mask is None:
            self.learn_scale_mask = torch.zeros(n_geos, dtype=torch.bool) 
        self.alpha = nn.Parameter(torch.tensor(alpha_init),
                                  requires_grad=one_scale_param)
        # import pdb; pdb.set_trace()

    def get_scale(self):
        return torch.where(self.learn_scale_mask,
                           self.learned_scale,
                           self.fixed_scale) * self.alpha

    def forward(self, x):
        # Static params
        T = x.shape[0]
        a = self.link_a(x).exp()
        p = self.get_scale() * self.p

        # Dynamic state
        # Sc = Not susceptible; i.e. cumulative infections
        Sc = torch.jit.annotate(List[Tensor], [self.y0.exp()])
        I = torch.jit.annotate(List[Tensor], [self.y0.exp()])
        for t in range(T):
            new_infections = a[t] * I[t] * (p - Sc[t]) / p
            It = I[t] * (1 - self.gamma) + new_infections
            Sct = Sc[t] + new_infections
            Sc += [Sct]
            I += [It]
        return torch.cat(Sc[:-1], dim=0)

    @classmethod
    def SI(cls, T, n_geos, *args, **kwargs):
        R_kernel = conv.FixedTSConv(torch.ones(n_geos, 1, T))
        return cls(n_geos, *args, R_kernel=R_kernel, **kwargs)


class Diffusion(nn.Module):
    def __init__(self, link_p, link_a, link_b, p_intercept=0.):
        "Sigmoid diffusion model."
        super(Diffusion, self).__init__()
        self.p_intercept = p_intercept
        self.link_p = link_p
        self.link_a = link_a
        self.link_b = link_b

    def forward(self, x):
        "Assumes tensor x has shape (time, geos, features)"
        T, _, _ = x.shape
        t = torch.range(1, T).view(T, 1)  # Add time variable
        yhat = core.scaled_sigmoid(*self.get_links(x), x=t)
        return yhat

    def get_links(self, x):
        p = self.link_p(x).exp()
        a = self.link_a(x).exp()
        b = self.link_b(x)
        return self.p_intercept + p, a, b

    # Parameter initializations
    # -------------------------------------------------------------------------
    def initialize_link_fns(self, x, y, T, b0=None, **kwargs):
        """
        Initialize link functions to match heuristically good
        parameter settings. Important for optimization to work.
        """
        b0 = T if b0 is None else b0
        link_p0, link_a0 = self.get_target_initializations(y, T, b0)
        initialize_link_fn(self.link_b, x, b0, **kwargs)
        initialize_link_fn(self.link_a, x, link_a0, **kwargs)
        initialize_link_fn(self.link_p, x, link_p0, **kwargs)

    @staticmethod
    def get_target_initializations(y, T, b0):
        """
        Find initial parameters p, a that match y at t=0 and t=T,
        given b0.
        """
        max_y = y.max(dim=0).values
        p_candidates = (torch.arange(1, 10, 0.01)
                        .repeat((y.shape[1], 1))
                        .transpose(0, 1) * max_y)
        p_targets = (1 + torch.exp((p_candidates / y[0] - 1).log() *
                                   (T - b0) / b0)) * max_y
        best_p_idx = (p_candidates - p_targets).abs().min(dim=0).indices
        p_prior = p_candidates[best_p_idx, torch.arange(y.shape[1])]
        a_prior = 1 / (b0 - 1) * np.log((p_prior - y[0]) / y[0])
        return np.log(p_prior - max_y), np.log(a_prior)

    # Constructors
    # -------------------------------------------------------------------------
    @classmethod
    def init_from_config(cls, n_geos, n_features, config, **kwargs):
        args = [cls.initialize_link(
            n_geos, n_features, **config.get(param, dict()))
            for param in ["p", "a", "b"]]
        return cls(*args, **kwargs)

    @staticmethod
    def initialize_link(n_geos, n_features, hidden_sizes=None, **kwargs):
        hidden_sizes = [] if hidden_sizes is None else hidden_sizes
        layers = [n_features] + hidden_sizes
        mlp = core.MLP(layers)
        mixed = core.MixedEffectsLinear(n_geos, layers[-1], **kwargs)
        return nn.Sequential(mlp, mixed)

    @classmethod
    def random_effects(cls, n_random):
        links = [core.MixedEffectsLinear(n_random, 0, suppress_fixed=True)
                 for _ in range(3)]
        return cls(*links)
