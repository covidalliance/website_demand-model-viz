import torch
import torch.distributions as tdistr
import torch.nn as nn

import covid_demand.utils as utils


class TimeSeriesConv(nn.Module):
    def __init__(self, kernel_size, channels):
        """
        Convolves a time series (time, units) with kernel, where
        the kernel is assumed to begin at t=0.

        :param kernel_size: Convolution kernel.
        :param lag: How far back to apply the center of the kernel.
        """
        super(TimeSeriesConv, self).__init__()
        self.kernel_size = kernel_size
        self.channels = channels
        self.pad = kernel_size - 1

    def forward(self, x):
        # Assume x is 2D (time, units)
        T, n = x.shape
        xt = x.permute(1, 0).unsqueeze(0)
        kernel = self.get_kernel()
        convolved = nn.functional.conv1d(
            xt, kernel, padding=self.pad, groups=self.channels)
        return convolved.squeeze()[:, :T].permute(1, 0)

    def get_kernel(self):
        raise NotImplementedError()


class FixedTSConv(TimeSeriesConv):
    def __init__(self, kernel):
        """
        Convolution with fixed (i.e. not optimized) kernel.
        Kernels must have shape (channels, 1, T).
        """
        super(FixedTSConv, self).__init__(
            kernel.shape[2], kernel.shape[0])
        self.kernel = kernel

    def get_kernel(self):
        return self.kernel


class LogNormalTSConv(TimeSeriesConv):
    def __init__(self, kernel_size, channels, loc=0., scale=1.):
        super(LogNormalTSConv, self).__init__(kernel_size, channels)
        self.loc = nn.Parameter(utils.to_tensor((channels, ), loc))
        self.scale = nn.Parameter(utils.to_tensor((channels, ), scale))
        self.t = (torch.linspace(float(kernel_size), 0,
                                 steps=kernel_size + 1)
                  .view(-1, 1).repeat(1, channels))

    def get_kernel(self):
        distr = tdistr.LogNormal(self.loc, self.scale)
        kernel = distr.cdf(self.t).transpose(0, 1).unsqueeze(1)
        return kernel


class Differencer(nn.Module):
    def __init__(self, dim=0, pad=0):
        super(Differencer, self).__init__()
        self.dim = dim
        self.pad = pad

    def forward(self, x):
        x0 = x.index_select(0, torch.tensor([0]))
        pad = torch.ones_like(x0) * self.pad
        lagged = torch.cat((pad, x[:-1]), dim=self.dim)
        return x - lagged
