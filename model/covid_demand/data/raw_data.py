# import numpy as np
import os
import datetime as dt
import pandas as pd
import pytz
import yaml

from covid_demand.data.county_data import CountyData

rename_cols = {
    "State": "state",
    'fips': 'county_fips',
    'CY Population': 'cy_population',
    'stay_home_ratio_smooth_rel.y.y.y': 'stay_home_ratio_smooth_rel',
    'stay_home_ratio_bin.y.y.y': 'stay_home_ratio_bin',
    'stay_home_ratio_smooth_rel.y': 'stay_home_ratio_smooth_rel',
    'stay_home_ratio_bin.y': 'stay_home_ratio_bin'
}

def get_s3_directories(s3_base, date):
    if date is None:
        date = dt.datetime.strftime(dt.datetime.now(pytz.timezone('US/Eastern')), "%Y-%m-%d")
    # date = str(dt.date.today() - dt.timedelta(days=1))
    public = os.path.join(s3_base, 'archive/public/%s/clean' % date)
    safegraph = os.path.join(s3_base, 'archive/safegraph/%s/clean' % date)
    return {
        "S3_PUBLIC": public,
        "S3_SG": safegraph,
    }

class RawData:
    def __init__(self, directory, geo_ID, geo_name, daily_file, 
                 state_daily_file, static, dynamic, s3_base, 
                 state=None, date=None):
        self.special_dirs = get_s3_directories(s3_base, date)
        self.geo_ID = geo_ID
        self.geo_name = geo_name
        self.state = state
        self.static_data = self.extract_features(directory, static)
        self.dynamic_data = self.extract_features(directory, dynamic, 
                                                  merge_cols=['date'])
        self.daily_data = self.read_daily_file(directory, daily_file)
        self.postprocess(self.daily_data, self.geo_ID)
        self.state_daily_data = self.read_daily_file(directory, state_daily_file)

    @classmethod
    def from_config_file(cls, config_file, date=None):
        with open(config_file) as f:
            config = yaml.load(f)
        return cls(**config, date=date)

    def get_dir(self, directory):
        return self.special_dirs.get(directory, directory)

    def remove_state(self, state):
        if 'state' in self.static_data:
            self.static_data = self.static_data[self.static_data.state != state]
        if 'state' in self.dynamic_data:
            self.dynamic_data = self.dynamic_data[self.dynamic_data.state != state]
        if 'state' in self.daily_data:
            self.daily_data = self.daily_data[self.daily_data.state != state]

    def keep_state(self, state):
        if 'state' in self.static_data:
            self.static_data = self.static_data[self.static_data.state == state]
        if 'state' in self.dynamic_data:
            self.dynamic_data = self.dynamic_data[self.dynamic_data.state == state]
        if 'state' in self.daily_data:
            self.daily_data = self.daily_data[self.daily_data.state == state]

    def to_countydata(self):
        return CountyData(None, 
                          daily_data=self.daily_data, 
                          static_data=self.static_data, 
                          stay_home_data=self.dynamic_data, 
                          state_daily_data=self.state_daily_data,
                          ID_col='geo_ID', name_col='geo_name')

    def combine(self, other):
        """
        Incorporate another RawData instance into this one.
        """
        def _concat_dfs(df1, df2):
            columns = [c for c in df1.columns if c in df2.columns]
            return pd.concat((df1[columns], df2[columns])).reset_index(drop=True)
        self.static_data = _concat_dfs(self.static_data, other.static_data)
        self.dynamic_data = _concat_dfs(self.dynamic_data, other.dynamic_data)
        self.daily_data = _concat_dfs(self.daily_data, other.daily_data)

    def extract_features(self, directory, feature_info, merge_cols=None):
        geo_ID = feature_info.get('geo_ID', self.geo_ID)
        direc = feature_info.get('directory', directory)
        main= self.read_and_preprocess(direc, feature_info['main_file'])
        for other_file in feature_info['other_files']:
            filename = other_file['filename']
            direc = other_file.get('directory', direc)
            df = self.read_and_preprocess(direc, filename)
            merge_col = other_file.get('merge_col', self.geo_ID)
            merge_by = merge_col if merge_cols is None else [merge_col] + merge_cols
            main = main.merge(df, on=merge_by, how='left')
        main = self.postprocess(main, geo_ID)
        return main

    def read_daily_file(self, directory, info):
        direc = info.get('directory', directory)
        return self.read_and_preprocess(direc, info['filename'])

    def read_and_preprocess(self, directory, filename):
        direc = self.get_dir(directory)
        filepath = os.path.join(direc, filename)
        print('reading', filepath)
        df = pd.read_pickle(filepath)
        df = self.preprocess(df)
        return df

    def preprocess(self, df):
        df = df.rename(columns=rename_cols)
        # remove duplicate columns
        df = df.loc[:,~df.columns.duplicated()]
        if 'date' in df:
            df['date']= pd.to_datetime(df['date'])
        if 'state' not in df and self.state is not None:
            df['state'] = self.state
        elif 'state' not in df and self.geo_ID == 'region':
            df['state'] = df[self.geo_ID].str.split().str.get(-1)
        return df

    def postprocess(self, df, geo_ID):
        if geo_ID in df:
            if geo_ID == 'county_fips':
                df[geo_ID] = df[geo_ID].astype(float)
            df['geo_ID'] = df[geo_ID].astype(str)
        # else:
        #     print("didn't find", geo_ID, 'in', df.head())
        if self.geo_name in df:
            df['geo_name'] = df[self.geo_name]
        return df



