from collections import defaultdict
from copy import copy
import datetime as dt
import numpy as np
import pandas as pd
import torch as t
import os

from sklearn.linear_model import Lasso, Ridge

from covid_demand.data.time_to_peak import get_inflection_for_county

"""
USAGE
--------------
Initialize object and run `perform_filters`.
Use `x, y, fips = get_data(features)` to get the required features and timeseries
of infections needed for training. 

EXAMPLE
--------------
county_data = CountyData('../../../data')
county_data.perform_filters(dt.datetime(2020, 3, 1), states=['NY', 'CA'], threshold_num_cases=100)
# List of columns from `county_static_data.pkl` to be used.
county_features = ['% CY Pop, Age 30-39', '% CY Pop, Age 70-79', '% CY Families', 'med_income', 'urban_category']
# List of columns from `sg_sd_county_daily_data.pkl` to be used.
dynamic_features = ['stay_home_ratio']
align_threshold = 0.0005
x, y, fips = county_data.get_data(county_features, 
                                  dynamic_features=dynamic_features, 
                                  state_encoding=True, 
                                  align_threshold=align_threshold)
y_mask = county_data.y_mask
county_names = [county_data.fips_to_name(fip) for fip in fips]
"""

def date2dt(d):
    if isinstance(d, dt.date):
        return dt.datetime(d.year, d.month, d.day)
    elif isinstance(d, str):
        return dt.datetime.strptime(d, "%Y-%m-%d")
    else:
        return d

def force_nondecreasing(array):
    """
    Mutate array to be non-decreasing.
    """
    for i, x in enumerate(array):
        if i > 0 and array[i] < array[i-1]:
            array[i] = array[i-1]

class CountyData:
    def __init__(self, data_repo_directory=None, static_data=None,
                 daily_data=None, stay_home_data=None, state_daily_data=None,
                 state_static_data=None, ID_col='county_fips',
                 name_col='county'):
        self.ID_col = ID_col
        self.name_col = name_col
        self.deaths_col = 'daily_deaths'
        self.fips_to_county = dict()
        self.is_region = defaultdict(bool)
        self.cached = False
        self.dynamic_feature_lag = 7

        if data_repo_directory is not None:
            # ID_col, state, features
            if static_data is None:
                static_data = pd.read_pickle(os.path.join(data_repo_directory, 'county_static_data.pkl'))

            # date, ID_col, state, features
            if daily_data is None:
                daily_data = pd.read_pickle(os.path.join(data_repo_directory, 'county_daily_data.pkl'))

            # date, ID_col, dynamic_features
            if stay_home_data is None:
                inflow = pd.read_pickle(os.path.join(data_repo_directory, 'sg_inflow.pkl'))
                outflow = pd.read_pickle(os.path.join(data_repo_directory, 'sg_outflow.pkl'))
                stay_home_data = pd.read_pickle(os.path.join(data_repo_directory, 'sg_sd_county_daily_data.pkl'))
                stay_home_data = stay_home_data.merge(inflow, on=['date', self.ID_col])
                stay_home_data = stay_home_data.merge(outflow, on=['date', self.ID_col])

            # state, features
            if state_static_data is None:
                state_static_data = pd.read_pickle(os.path.join(data_repo_directory, 'state_static_data.pkl'))

            # date, state, features
            if state_daily_data is None:
                state_daily_data = pd.read_pickle(os.path.join(data_repo_directory, 'state_daily_data.pkl'))

        self.state_static_data = state_static_data
        self.static_data = self._process_static_data(static_data, state_static_data)
        self.daily_data = self._process_daily_data(daily_data)
        self.dynamic_data = self._process_dynamic_data(stay_home_data)
        self.state_daily_data = state_daily_data

    def _process_static_data(self, df, state_df):
        if df is None:
            return None
        df['region_name'] = df[self.name_col].astype(str) + ',' + df.state
        if 'population' not in df:
            df['population'] = df['cy_population']
        if state_df is not None:
            df = df.merge(state_df, left_on='state', right_on='state', how='left')
        df = df.set_index(self.ID_col)
        return df

    def _process_daily_data(self, df):
        df['date']= pd.to_datetime(df['date']) 
        df['cases_cum'] = df['cases_cum'].astype(float)
        df['region_name'] = df[self.name_col].astype(str) + ',' + df.state
        df['daily_deaths'] = 0
        df['totalTestResults'] = 1
        if 'is_region' in df:
            self.is_region = pd.Series(df.is_region.values.astype(bool),
                                       index=df[self.ID_col]).to_dict()
        # Add population info.
        if 'population' not in df:
            if self.static_data is None:
                # HACK
                df['population'] = 10000
            else:
                df = df.merge(self.static_data['population'], left_on=self.ID_col, right_index=True)
        # df = df[df.county != 'Unknown']
        self.fips_to_county = pd.Series(df.region_name.values, index=df[self.ID_col]).to_dict()
        self.county_to_fips = pd.Series(df[self.ID_col].values, index=df.region_name).to_dict()
        return df

    def _process_dynamic_data(self, df):
        if df is None:
            return None
        # Add add lag to the time-dependent features.
        df['date']= pd.to_datetime(df['date'])
        # df[self.ID_col] = df[self.ID_col]
        return df

    def fips_to_name(self, fips):
        return self.fips_to_county[fips]

    def name_to_fips(self, name):
        return self.county_to_fips[name]

    def perform_filters(self, start_date=None, end_date=None, counties_to_include=None,
                        states=None, threshold_time=None, threshold_num_cases=None,
                        threshold_percent_infected=None,
                        rolling_window=1, dynamic_feature_lag=0, include_all=False):
        """
        Applies the specified filters.

        :param start_date: (YYYY-MM-DD) the timeseries should start at this date. 
        :param end_date (YYYY-MM-DD) the timeseries will end at this date.
        :param counties_to_include: list of counties to include (e.g. ["Middlesex,MA"])
        :param states: list of states to include
        :param threshold_time: filter out counties that have not had any infection by this date.
        :param threshold_num_cases: filter out counties that have less than this number of infections
                                    at end_date. 
        :param threshold_percent_infected: filter out counties that have less than this percentage of
                                           infections. 1% should be encoded as 0.01.
        """
        self.dynamic_feature_lag = dynamic_feature_lag
        df = self.daily_data.copy()
        threshold_time = date2dt(threshold_time)

        if start_date is not None and isinstance(start_date, str):
            start_date = dt.datetime.strptime(start_date, '%Y-%m-%d')
        if end_date is not None and isinstance(end_date, str):
            end_date = dt.datetime.strptime(end_date, '%Y-%m-%d')
        start_date = date2dt(start_date)
        end_date = date2dt(end_date)

        if end_date is not None:
            df = df[df.date <= end_date]

        # Don't perform the other filters if the counties are specified.
        if include_all:
            self.included_counties= sorted(df[self.ID_col].unique())
        elif counties_to_include is not None:
            self.included_counties = [self.county_to_fips[x] for x in counties_to_include]
        else:
            # -------------------------
            # Perform the main filters
            # -------------------------
            if states is not None:
                df = df[df.state.isin(states)]
            self.included_counties = []
            for county, group in df.groupby(self.ID_col):
                first_case = group.date.min()
                total_cases = group.cases_cum.max()
                population = group.population.max()
                if threshold_time is not None and first_case > threshold_time:
                    continue
                if threshold_num_cases is not None and total_cases < threshold_num_cases:
                    continue
                if threshold_percent_infected is not None and total_cases/population < threshold_percent_infected:
                    continue
                self.included_counties.append(county)

        df = df[df[self.ID_col].isin(self.included_counties)]
        df = df.sort_values(by='date')

        self._compute_moving_averages(df, rolling_window)

        if self.dynamic_data is not None:
            if end_date is not None:
                # Don't use any data after end_date.
                self.dynamic_data = self.dynamic_data[self.dynamic_data.date <= end_date]
            self.dynamic_data['date'] = self.dynamic_data['date'] + dt.timedelta(days=dynamic_feature_lag)

        self.filtered_df = self._filter_start_end_dates(df, start_date, end_date)
        self.filtered_df = self._incorporate_testing_numbers(self.filtered_df)
        self.included_counties = sorted(self.filtered_df[self.ID_col].unique())
        print('end_date', end_date)
        print('self.end_date', self.end_date)

    def _filter_start_end_dates(self, daily_data, start_date, end_date):
        first_date_with_data = daily_data.date.min()
        last_date_with_data = daily_data.date.max()
        if self.dynamic_data is not None:
            first_date_with_data = max(first_date_with_data, self.dynamic_data.date.min())
            last_date_with_data = min(last_date_with_data, self.dynamic_data.date.max())
        start_date = first_date_with_data if start_date is None else max(start_date, first_date_with_data)
        end_date = last_date_with_data if end_date is None else min(end_date, last_date_with_data)
        self.start_date = date2dt(start_date)
        self.end_date = date2dt(end_date)
        daily_data = daily_data[daily_data.date <= self.end_date]
        daily_data = daily_data[daily_data.date >= self.start_date]
        return daily_data

    def _compute_moving_averages(self, df, rolling_window):
        df['cases_cum'] = df.groupby(self.ID_col)['cases_cum'].rolling(rolling_window, center=True).mean().reset_index(0,drop=True)
        df.dropna(subset=['cases_cum'], inplace=True)

    def _compute_daily_deaths(self, df):
        # def rolling_diff(group):
        #     return group.rolling(window=2, on='date').deaths.apply(lambda x: x.iloc[1] - x.iloc[0])
        # df['daily_deaths'] = df.groupby(self.ID_col,group_keys=False).apply(rolling_diff)
        # compute daily deaths
        df.dropna(subset=['deaths_cum'], inplace=True)
        for county, group in df.groupby(self.ID_col):
            prev = 0
            for row_index, row in group.iterrows():
                curr = row['deaths_cum']
                df.at[row_index, 'daily_deaths'] = curr - prev
                prev = curr

    def _incorporate_testing_numbers(self, daily_data):
        if self.state_daily_data is None:
            return daily_data
        df = self.state_daily_data
        # df['date']= pd.to_datetime(df['date'])
        df = df[df.date <= self.end_date]
        df = df[['date', 'state', 'totalTestResults']]
        df = df.fillna(0)
        # weight values of totalTestResults by the last day.
        for state, group in df.groupby('state'):
            max_num_tests = group['totalTestResults'].max()
            for row_index, row in group.iterrows():
                curr = row['totalTestResults']
                df.at[row_index, 'totalTestResults'] = curr/max_num_tests
        # merge state data with daily data
        return daily_data.merge(df, on=['state', 'date'], how='left', suffixes=('_x', ''))

    def get_cached_data(self, return_full_x):
        length = self.x.shape[0] if return_full_x else self.T
        return self.x.float()[:length, :, :], self.y_data, self.y_data_state, self.included_counties

    def get_data(self, static_features, dependent_vars, dynamic_features=None, 
                 state_encoding=False, align_threshold=None, standardize=True, 
                 normalize_by_pop=True, include_intercept=True, return_full_x=True):
        """
        Returns x, y, ids.
        x and y are PyTorch tensor objects, ids is a list of fips.

        :param static_features: a list of columns from county_static_data.pkl
        :param dynamic_features: a list of columns from sg_sd_county_daily_data.pkl
        :param state_encoding: if True, include a one-hot encoding of the states
        :param align_threshold: if it is a number, then for every county, the timeseries
            starts at the time when the infection rate is above this threshold.
            In this case, y is still rectangular - use self.y_mask to check which
            entries are valid. 
        :param standardize: standardize the features
        :param normalize_by_pop: divide the # of infections by county population
        """
        if self.cached:
            return self.get_cached_data(return_full_x)

        self.included_counties = self._initial_county_filter_for_na(static_features, 
                                                                    dynamic_features, 
                                                                    self.included_counties)
        self.included_states = np.array(self.static_data[self.static_data.index.isin(self.included_counties)].state.unique())
        print('states:', self.included_states)

        self.y_data, self.included_counties = self._extract_dependent_vars(
            dependent_vars['region'], self.filtered_df, self.included_counties)
        self.y_data_state, self.included_states = self._extract_dependent_vars(
            dependent_vars['state'], self.state_daily_data, self.included_states, 'state')

        # -------------------
        # Static Features
        # -------------------
        if self.static_data is None or static_features is None:
            self.static_x = np.array([])
            self.feature_names = []
        else:
            self.static_x, self.feature_names = self._get_static_features(self.included_counties, static_features)
        if state_encoding:
            state_1hot, state_names = self._get_one_hot_state(self.included_counties)
            self.static_x = np.concatenate((self.static_x, state_1hot), axis=1)
            self.feature_names = self.feature_names + state_names
        x_numpy = np.array([self.static_x]).astype(float).repeat(self.T, axis=0)

        # -------------------
        # Dynamic Features
        # -------------------
        if dynamic_features is not None:
            dynamic_x, dynamic_feature_names = self._get_dynamic_features(self.included_counties, dynamic_features)
            self.feature_names = self.feature_names + dynamic_feature_names
            self.full_T = dynamic_x.shape[0]
            if self.full_T > self.T:
                x_numpy = np.array([self.static_x]).astype(float).repeat(self.full_T, axis=0)
            x_numpy = np.concatenate((x_numpy, dynamic_x), axis=2)

        self.post_process_x(standardize, include_intercept, x_numpy)

        self.cached = True
        return self.get_cached_data(return_full_x)

    def _extract_dependent_vars(self, columns, df, locs, ID_col=None):
        """
        Outputs a dictionary where the key is either a column or column + '_mask',
        and the value is the tensor corresponding to this key.
        The rows of the tensor correspond to the time step,
        and the columns correspond to locs.
        """
        ts_data = dict()
        for col in columns:
            end_dates = dict()
            ts_data[col] = self._extract_timeseries(
                df, col, locs, 
                normalize_by_pop=False, 
                should_be_nondecreasing=True,
                ID_col=ID_col)
            # update locs to be the locs included in this timeseries.
            locs = sorted(ts_data[col].keys())

        output = dict()
        for col, ts in ts_data.items():
            y, y_mask = self._rectangularize_timeseries(ts, locs)
            self.T = y.shape[1]
            output[col] =  t.from_numpy(y.T).float()
            mask_col = col + '_mask'
            output[mask_col] =  t.from_numpy(y_mask.T).float()
            setattr(self, col, output[col])
            setattr(self, mask_col, output[mask_col])
        return output, locs

    def post_process_x(self, standardize, include_intercept, x):
        # Turn to PyTorch tensor.
        self.x = t.from_numpy(x).float()
        if standardize:
            mu = self.x.mean(dim=(0, 1), keepdim=True)
            sigma = self.x.std(dim=(0, 1), keepdim=True)
            mu[sigma == 0] = 0.
            sigma[sigma == 0] = 1.

            self.x = (self.x - mu) / sigma
            self.standardize_mu = mu
            self.standardize_sigma = sigma

        if include_intercept:
            num_geos = len(self.included_counties)
            intercept = np.array([[[1] for _ in range(num_geos)] for _ in range(self.full_T)])
            if x.shape[1] == 0:
                self.x = t.from_numpy(intercept).float()
            else:
                self.x = t.cat((self.x, t.from_numpy(intercept).float()), 2)

        self.x_numpy = self.x.numpy()

    def get_training_mask(self, y_mask, num_test_days=7):
        y_mask_all = y_mask.numpy().T
        training_mask = []
        for county_mask in y_mask_all:
            # county_mask is an binary array: [1, 1, 1, ..., 0, 0]
            first_zero_index = len(county_mask)
            if np.any(county_mask < 0.5):
                first_zero_index = next(i for i, y in enumerate(county_mask) if y < 0.5)
            new_first_zero = max(first_zero_index-num_test_days, 0)
            county_training_mask = np.zeros(len(county_mask))
            county_training_mask[:new_first_zero] = 1
            training_mask.append(county_training_mask)
        return t.from_numpy(np.array(training_mask).T)

    def _initial_county_filter_for_na(self, static_features, dynamic_features, 
                                      current_included_counties):
        """
        Returns a list of counties that have all the required features.
        """
        T = (self.end_date-self.start_date).days + 1
        loc_to_start_date = dict()
        loc_to_end_date = dict()

        # ----------------------
        # Check timeseries
        # ----------------------
        timeseries_counties = copy(current_included_counties)
        df = self.filtered_df
        for county in current_included_counties:
            group = df[df[self.ID_col] == county]
            loc_to_start_date[county] = max(self.start_date, group.date.min())
            if group.date.max() < self.end_date - dt.timedelta(days=7):
                print('removing', county, 'because last date is too outdated.')
            loc_to_end_date[county] = min(self.end_date, group.date.max())
            group = group[group.date <= loc_to_end_date[county]]
            if group.shape[0] != (loc_to_end_date[county] - loc_to_start_date[county]).days + 1:
                timeseries_counties.remove(county)
                print('removing', self.fips_to_name(county), 'since missing cases data.')

        # ----------------------
        # Check static features
        # ----------------------
        static_counties = timeseries_counties
        if static_features:
            features_df = self.static_data.loc[timeseries_counties][static_features]
            to_include = set(features_df[static_features].dropna().index)
            static_counties = [x for x in timeseries_counties if x in to_include]
            if len(static_counties) != len(timeseries_counties):
                excluded = [self.fips_to_name(x) for x in timeseries_counties if x not in to_include]
                print('removing', excluded, 'since missing some static features.')

        # ----------------------
        # Check dynamic features
        # ----------------------
        counties_to_include = copy(static_counties)
        if dynamic_features:
            dynamic_df = self.dynamic_data[self.dynamic_data[self.ID_col].isin(counties_to_include)]
            dynamic_df = dynamic_df.dropna(subset=dynamic_features)
            dynamic_end_date = dynamic_df.date.max()
            removed = []
            for county in static_counties:
                group = dynamic_df[dynamic_df[self.ID_col] == county]
                group = group[(group.date >= self.start_date) & \
                              (group.date <= dynamic_end_date)]
                # Check that we have all the days in between and including the
                # start date and end date.
                if group.shape[0] != (dynamic_end_date-self.start_date).days + 1:
                    counties_to_include.remove(county)
                    removed.append(self.fips_to_name(county))
                    # print('removing', county, 'since missing dynamic data.')
                    # print(loc_to_start_date[county], loc_to_end_date[county])
                    # print(group)
            if removed:
                print('removing',  removed, 'since missing some dynamic features.')

        self.fip_to_start_date = loc_to_start_date
        self.fip_to_end_date = loc_to_end_date
        return np.array(sorted(counties_to_include))


    def _get_static_features(self, counties, static_features):
        features_df = self.static_data.loc[counties][static_features]
        assert features_df.shape == (len(counties), len(static_features))
        return features_df.to_numpy(), static_features

    def _get_one_hot_state(self, counties):
        county_states = [self.fips_to_name(x)[-2:] for x in counties]
        # states are listed in alphabetical order.
        states_sorted = sorted(list(set(county_states)))
        state_1hot = np.array([[int(county_state.endswith(state)) for state in states_sorted] 
                                for county_state in county_states])
        assert state_1hot.shape == (len(counties), len(states_sorted))
        return state_1hot, states_sorted

    def _get_dynamic_features(self, counties, dynamic_features):
        df = self.dynamic_data[self.dynamic_data[self.ID_col].isin(counties)]
        # find the last date that we have the dynamic features for all counties
        dynamic_end_date = df.dropna(subset=dynamic_features).date.max()
        print('self.dynamic_end_date', dynamic_end_date)
        print('self.end_date', self.end_date)
        features = []
        for county in counties:
            group = df[df[self.ID_col] == county]
            group = group[(group.date >= self.start_date) & (group.date <= dynamic_end_date)]
            group = group.sort_values(by='date').dropna(subset=dynamic_features)

            # Check that we have all the days in between and including the
            # start date and end date.
            if group.shape[0] != (dynamic_end_date-self.start_date).days + 1:
                print('ERROR:', county)

            # Rows are dates, columns are features.
            data = group[dynamic_features].to_numpy()
            # We need this many # of data points to fill up to self.T time steps.
            under = int(self.T - data.shape[0])
            if under > 0:
                data = np.concatenate((data, [data[-1]]*under))
            features.append(data)

        # Swap counties and time axis.
        # Results in [t1, t2,...] where t_ is a matrix of county by features.
        features = np.swapaxes(np.array(features), 0, 1)

        return features, dynamic_features

    def _extract_timeseries(self, daily_data, col, locations, 
                            normalize_by_pop=False, 
                            should_be_nondecreasing=True,
                            ID_col=None):
        output = dict()
        if ID_col is None:
            ID_col = self.ID_col
        for loc in locations:
            group = daily_data[daily_data[ID_col] == loc]
            group = group[(group.date >= self.start_date) & (group.date <= self.end_date)]
            group = group.sort_values(by='date')
            loc_end_date = group.iloc[-1].date

            # timeseries = np.array(group[col]/group.population)
            timeseries = np.nan_to_num(np.array(group[col]))
            if should_be_nondecreasing:
                force_nondecreasing(timeseries)

            # No alignment - every timeseries should start on self.start_date
            first_case = group.date.min()
            lag = int((first_case - self.start_date).days)
            timeseries = [0]*lag + list(timeseries)

            # If for some reason the # of data points isn't the same, skip this county.
            if len(timeseries) < (loc_end_date - self.start_date).days + 1:
                print('Some data missing for', loc)
                continue

            output[loc] = np.array(timeseries)
        return output

    def _rectangularize_timeseries(self, ts_data, locs, default=-1):
        # The length that all the timeseries objects should be
        length = max([len(ts) for ts in ts_data.values()])

        extended_data = dict()
        masks = dict()
        for loc in locs:
            ts = ts_data[loc]
            mask = np.zeros(length)
            extended_data[loc] = list(ts) + [default for _ in range(length-len(ts))]
            mask[:len(ts)] = 1
            masks[loc] = mask
            assert len(extended_data[loc]) == length

        y = np.array([extended_data[county] for county in locs])
        y_mask = np.array([masks[county] for county in locs])

        return y, y_mask

    def determine_prior_num_days(self, inflection_date, current_date, start_date, 
                                 default_prior, adjustment_step_size, 
                                 earlier_if_detected, later_if_not_detected):
        if inflection_date is not None and earlier_if_detected:
            return (inflection_date-start_date).days
        if not later_if_not_detected:
            return default_prior
        current_num_days = (current_date - start_date).days
        if current_num_days <= default_prior:
            return default_prior
        # Number of days after the default prior.
        delta = current_num_days - default_prior
        return default_prior + adjustment_step_size*(np.ceil(delta/adjustment_step_size))

    def b_priors_from_inflection_dates(self, geo_ids, inflection_dates, train_end_date,
                                       default=20, adjustment_step_size=6,
                                       earlier_if_detected=True,
                                       later_if_not_detected=True):
        """
        inflection_dates is a map from geo_ID -> inflection date, already 
        computed from time_to_peak. 
        for the prior is default.
        """
        b_prior = []
        df = self.filtered_df
        for geo_id in geo_ids:
            group = df[df[self.ID_col] == geo_id]
            inf_date = inflection_dates.get(geo_id, None)
            start_date = self.start_date
            current_date = min(train_end_date, self.fip_to_end_date[geo_id])
            # if len(geo_id) == 1:
            #     print(geo_id, inf_date, current_date, start_date, (current_date-start_date).days)
            prior_num_days = self.determine_prior_num_days(inf_date, 
                                                           current_date,
                                                           start_date,
                                                           default, 
                                                           adjustment_step_size,
                                                           earlier_if_detected,
                                                           later_if_not_detected)
            b_prior.append(prior_num_days)
        return np.array(b_prior)


if __name__ == '__main__':
    region_daily_data = pd.read_pickle('../../data/GA_county_to_region/GA_region_daily_data.pkl')
    state_data = pd.read_pickle('../../../data/state_daily_data.pkl')
    county_data = CountyData(None, 
                             daily_data=region_daily_data, 
                             state_data=state_data,
                             ID_col='region', name_col='region')
    cd = county_data
    # end_date = dt.datetime(2020, 4, 2)
    # county_data.perform_filters(dt.datetime(2020, 3, 1), counties_to_include=['Middlesex,MA', 'Suffolk,MA'])
    county_data.perform_filters(dt.datetime(2020, 3, 1), states=['GA'], threshold_num_cases=100)
    # List of columns from `county_static_data.pkl` to be used.
    county_features = ['% CY Pop, Age 30-39', '% CY Pop, Age 70-79', '% CY Families', 'med_income', 'urban_category']
    dynamic_features = ['stay_home_ratio']
    cd = county_data
    align_threshold = 0.00001
    x, y, y_deaths, fips = county_data.get_data([], # static features
                                                dynamic_features=None, 
                                                state_encoding=False, 
                                                align_threshold=align_threshold,
                                                normalize_by_pop=False,
                                                include_intercept=False)
    # county_names = [county_data.fips_to_name(fip) for fip in fips]

