import datetime as dt
import numpy as np

def rolling_diff(group): 
    return group.rolling(window=2, on='date')['cases_cum'].apply(lambda x: x.iloc[1] - x.iloc[0])

def calculate_inflection_points(daily_data, geo_IDs=None, nonincreasing_threshold=3,
                                rolling_avg_window=7, ID_col='geo_ID',
                                end_date=None, rel_threshold=0.75, infection_threshold=100):
    df = daily_data[daily_data[ID_col].isin(geo_IDs)] if geo_IDs is not None else daily_data
    df = df.copy()
    inflection_dates = dict()
    if end_date is not None:
        df = df[df.date <= end_date]
    if rolling_avg_window is not None:
        df['cases_cum'] = df.groupby(ID_col)['cases_cum'].rolling(rolling_avg_window, center=True).mean().reset_index(0,drop=True)
    if 'daily_infections' not in daily_data:
        df['daily_infections'] = df.groupby(ID_col, group_keys=False).apply(rolling_diff)
    for county, group in df.groupby(ID_col):
        if geo_IDs is not None and county not in geo_IDs:
            continue
        if group.cases_cum.max() < infection_threshold:
            continue
        inflection_date = get_inflection_for_county(county, group, nonincreasing_threshold,
                                                    rel_threshold=rel_threshold)
        if inflection_date is not None:
            inflection_dates[county] = inflection_date
    return inflection_dates, df

def check_nonincreasing(a, relative_tol=0.03):
    """
    relative_tol of 0.03 is a 3% tolerance.
    """
    if len(a) <= 1:
        return True
    prev = a[0]
    # we only allow the tolerance to be violated once.
    for x in a[1:]:
        if x > (1+relative_tol)*prev:
            return False
        prev = x
    return True

def determine_inflection(check_array, upper_bd=None):
    """
    [0, 0, 0, 1, 1, 1, 1]  should return 3.
    [0, 0, 0, 1, 0, 1, 1]  should return 5.
    If upper_bd is not None, then the index returned is
    at most the upper_bd.
    """
    if upper_bd is None and check_array[-1] < 0.5:
        return None
    for i, x in reversed(list(enumerate(check_array))):
        if upper_bd is not None and i >= upper_bd:
            continue
        if x < 0.5:
            return i+1
    return 0

def initial_inflection_upper_bound(array, rel_threshold, 
                                   current_week_length=5):
    """
    As an initial starting point, loop through the list in reverse 
    order as long as the daily infections is at most rel_threshold
    percentage of the second highest daily infections we've seen for
    that region.
    """
    # take second highest point
    if len(array) < current_week_length + 2:
        return None
    highest = np.sort(array)[-2]
    threshold = highest*rel_threshold

    # For there to be a peak, the maximum in entire the current week
    # must be less than the threshold.
    x = array[-current_week_length:]
    x = x[~np.isnan(x)]
    if np.max(x) > threshold:
        return None

    i = 1
    while np.isnan(array[-i]):
        i += 1
    if array[-i] > threshold:
        return None
    for i, x in reversed(list(enumerate(array))): 
        if x > threshold:
            return i+1

def get_inflection_for_county(county, county_df, nonincreasing_threshold, 
                              relative_tol=0, rel_threshold=0.75):
    """
    Step 1) First, go back in time until the daily infections is less than rel_threshold
    of the highest daily infections. If this point doesn't exist, then there is no peak.
    Step 2) From that point, go back in time as long as the rate as been decreasing.
    """
    county_df = county_df.sort_values(by='date').dropna(subset=['daily_infections'])
    # Step 1) upper_bd is the last day that the daily infection was below
    # the (highest daily infection rate) * rel_threshold
    upper_bd = initial_inflection_upper_bound(county_df.daily_infections.values, rel_threshold)
    if upper_bd is None:
        return None

    # 2) From the upper bound, go back in time as long as the rate has been decreasing.
    f = lambda x: check_nonincreasing([x.iloc[i] for i in range(nonincreasing_threshold)], 
                                      relative_tol)
    # check[i] contains whether the rate starting decreasing from point i
    check = county_df.rolling(window=nonincreasing_threshold, on='date').daily_infections.apply(f)
    index = determine_inflection(check.values, upper_bd)
    if index < upper_bd:
        # make the inflection point the day AFTER the highest peak 
        index -= (nonincreasing_threshold - (nonincreasing_threshold-1))
    if index is not None and index < len(check.values)-1:
        row_index = check.index[index]
        inflection_date = county_df.loc[row_index].date 
        return inflection_date
    return None

