import os

import datetime as dt
import numpy as np
import pandas as pd
import torch


def summarise_model(model):
    return dict(grad_norm=sum([torch.sum(x.grad ** 2)
                               for x in model.parameters()
                               if x.grad is not None]).item(),
                param_norm=sum([torch.sum(x.data ** 2)
                                for x in model.parameters()]).item())


def apply_mask(mask, *args):
    bool_mask = (mask != 0).bool()
    return [None if arg is None else arg.masked_select(bool_mask)
            for arg in args]


def getter(mapping):
    def get(x):
        try:
            y = mapping.get(x, x)
        except TypeError:
            return x
        else:
            return y
    return get


def numpify(f):
    "Converts all tensor arguments of f to numpy before passing to f."
    def f_np(*args, **kwargs):
        args_np = [x.detach().numpy() if isinstance(x, torch.Tensor) else x
                   for x in args]
        return f(*args_np, **kwargs)
    return f_np


def negative_to_na(y):
    y[y<0] = np.nan
    return y

@numpify
def format_predictions(y, names,
                       start_date, fips,
                       var_name="county",
                       value_name="value", **kwargs):
    df = pd.DataFrame(y)
    df.columns = [names, fips]
    df['date'] = start_date + days_delta(np.arange(y.shape[0]))
    for var, val in kwargs.items():
        df[var] = val
    melted = df.melt(id_vars=["date"] + list(kwargs.keys()),
                     var_name=["county", "fips"],
                     value_name=value_name)
    return melted

def days_delta(t):
    #return np.array([dt.timedelta(days=x) for x in t])
    # return pd.Series([pd.Timedelta(days=x) for x in t])
    return pd.to_timedelta(t, 'd')


def to_datetime(d):
    if isinstance(d, dt.date):
        return dt.datetime(d.year, d.month, d.day)
    elif isinstance(d, str):
        return dt.datetime.strptime(d, "%Y-%m-%d")
    else:
        return d


def datestring(d):
    return d.strftime("%Y-%m-%d")


def check_nonincreasing(a):
    if len(a) <= 1:
        return True
    prev = a[0]
    for x in a[1:]:
        if x > prev:
            return False
        prev = x
    return True


def extend(x, n, dim=0, method="replicate", value=0., flip=False):
    """
    Extends the shape of x by n in dim, by replicating the
    last element in dim n times.
    """
    x = torch.flip(x, dims=[dim]) if flip else x
    idx = torch.tensor([x.shape[dim] - 1])
    last = x.index_select(dim, idx)
    if method == "replicate":
        extend_one = last
    elif method == "constant":
        extend_one = torch.ones_like(last) * value
    repeats = [1] * len(x.shape)
    repeats[dim] = n
    extension = extend_one.repeat(repeats)
    result = torch.cat((x, extension), dim=0)
    result = torch.flip(result, dims=[dim]) if flip else result
    return result


def extend_w_scenario(x, n_stable, n, dynamic_feature_indices, dim=0,
                      idx_low=0, half_life=1., flip=False):
    """
    Extends the shape of x by n in dim, by replicating the last element in dim
    n_stable times, adding n_decrease entries going from last element to
    element low_value days before last entry, and then staying there for rest up to n days.
    """
    x = torch.flip(x, dims=[dim]) if flip else x
    idx_high = torch.tensor([x.shape[dim] - 1])
    idx_low = torch.tensor([idx_low])
    last = x.index_select(dim, idx_high)
    early = x.index_select(dim, idx_low)
    
    # stable period
    repeats = [1] * len(x.shape)
    repeats[dim] = n_stable
    extension_stable = last.repeat([n_stable, 1, 1])

    # only extend the dynamic features
    extend_high = last[:, :, dynamic_feature_indices]
    extend_low = early[:, :, dynamic_feature_indices]
    
    # decrease period
    repeats = [1] * (len(x.shape)-1)
    repeats[dim] = n - n_stable
    extension_high = extend_high.repeat(repeats)
    extension_low = extend_low.repeat(repeats)

    weight_high = 0.5 ** (torch.arange(extension_high.shape[0]).view(-1, 1).float() / half_life)
    weight_low = 1 - weight_high
    decrease_dynamic_features = (extension_high * weight_high + extension_low * weight_low)
    extension_decrease = last.repeat(repeats + [1])
    extension_decrease[:, :, dynamic_feature_indices] = decrease_dynamic_features

    result = torch.cat((x, extension_stable, extension_decrease), dim=0)
    result = torch.flip(result, dims=[dim]) if flip else result
    return result



def to_tensor(shape, x):
    if isinstance(x, float):
        return torch.ones(shape) * x
    else:
        return torch.tensor(x).view(shape)


def write_csv(df, root, name):
    os.makedirs(root, exist_ok=True)
    df.to_csv(os.path.join(root, name), index=False)
