"Generic modules and functions."
import itertools as it

import funcy as f
import torch
import torch.nn as nn

activations = {'tanh': nn.Tanh, 'relu': nn.ReLU}


def scaled_sigmoid(p, a, b, x):
    link = a * (x - b)
    return p * torch.sigmoid(link)


def single_step_sigmoid(p, a, y0):
    return p / (1 + (p / y0 - 1) * torch.exp(-a))


class MultiHead(nn.Module):
    def __init__(self, body, heads):
        """
        Simple structure for a models with a shared first stage (body)
        and multiple outputs (heads).

        :param body: nn.Module
        :param heads: dict {name: module} of nn.Modules.
        """
        super(MultiHead, self).__init__()
        self.body = body
        self.heads = nn.ModuleDict(heads)

    def forward(self, x, **kwargs):
        latent = self.body(x)
        return {name: head(latent, **kwargs.get(name, dict()))
                for name, head in self.heads.items()}


class MixedEffectsLinear(nn.Module):
    def __init__(self, n_random, n_fixed,
                 suppress_random=False, suppress_fixed=False,
                 init_random=0., init_fixed=0.):
        """
        Mixed effects linear model, with options to suppress
        the fixed or random effects.

        Assumes tensors are shape (batch, effects, features)
        """
        super(MixedEffectsLinear, self).__init__()
        self.random_effects = self.init_parameter(
            shape=(1, n_random),
            requires_grad=not suppress_random,
            init=init_random)
        self.fixed_effects = self.init_parameter(
            shape=(1, n_fixed, 1),
            requires_grad=not suppress_fixed,
            init=init_fixed)

    def forward(self, x):
        fixed = x.matmul(self.fixed_effects).squeeze(2)
        return fixed + self.random_effects

    @staticmethod
    def init_parameter(shape, requires_grad=True, init=0.):
        if isinstance(init, torch.Tensor):
            data = init.view(shape)
        else:
            data = torch.ones(shape) * init
        return nn.Parameter(data, requires_grad=requires_grad)


def MLP(layers, activation="relu", output_squeeze=False, bias=False):
    layer_list = []
    if isinstance(activation, str):
        activation = activations[activation]
    for prev_size, next_size in f.pairwise(layers):
        new_layer = nn.Linear(prev_size, next_size, bias=bias)
        layer_list.append(new_layer)
        layer_list.append(activation())
    return nn.Sequential(*layer_list[:-1])


class AddOne(nn.Module):
    def forward(self, x):
        return x + 1.


class Scale(nn.Module):
    def __init__(self, shape):
        super(Scale, self).__init__()
        self.scale = nn.Parameter(torch.ones(shape))

    def forward(self, x):
        return self.scale * x
