// ----------------------------------------
// ### MOBILE DEBUGGING
// ----------------------------------------

//if (!mobileConsole.status.initialized) {
//    mobileConsole.init();
//}

// ----------------------------------------
// ### INITIALIZATION 
// ----------------------------------------

// ### VARIABLES
//--------------------

/// * CANVAS

/// Basic variables
width     = 1500
height    = 1000

windowWidth    = $(window).width();
mobileWidth    = 850;

/// Graph-specific variables
height_top        = height/6
height_middle     = height/27

height_vis_main   = height/1.6
width_vis_main    = width/2
height_vis_mini   = height/3
width_vis_mini    = width/4

if (windowWidth>mobileWidth) {
  height_vis_bottom = height_vis_main*1.75
} else {
  height_vis_bottom = height_vis_main*2.3
}
padding_top          = 0
padding_top_main     = height_vis_main/6
padding_bottom_main  = height_vis_main/18
padding_hor_main     = 30
padding_top_mini     = (height_vis_mini/6) 
padding_bottom_mini  = (height_vis_mini/18) 
padding_hor_mini     = 30

button_width_main    = width/14
button_height_main   = height/25

button_width_mini    = (width/14)/2
button_height_mini   = (height/25)/2

legend_circle_radius      = 0
legend_circle_radius_mini = 0

/// * HELPERS

/// Colour scale
color_min = 0
color_max = 5

step_good  = d3.scaleLinear()
               .domain([1, 3])

step_bad  = d3.scaleLinear()
              .domain([1, 3])

color_good = d3.scaleLinear()
          .range(["#1a9850","#66bd63", "#a6d96a", "#d9ef8b"])
          .interpolate(d3.interpolateHcl) 

color_bad = d3.scaleLinear()
          .range(["#d73027", "#f46d43", "#fdae61", "#fee08b"])
          .interpolate(d3.interpolateHcl) 

// Time format
timeConv     = d3.timeParse("%Y-%m-%d")
timeConv_org = d3.timeParse("%m/%-d/%y")
timeForm     = d3.timeFormat("%m/%-d/%y")
timeForm_org = d3.timeFormat("%Y-%m-%d")
year         = d3.timeFormat("%Y")
month        = d3.timeFormat("%m")
day          = d3.timeFormat("%d")

// Bisect
function bisect(array, value, x) {
  value = timeForm(value)
  date_list=[]
  array.forEach(function(d) {
      date_list.push(timeForm(d.date))
  })

  if (date_list.indexOf(value)!=-1) {
    index = date_list.indexOf(value)
  } else {
    index = d3.bisect(date_list, value)
  }  
  return(index)
} 
 


// Axes
xScale_main = d3.scaleTime().range([0, width_vis_main - padding_hor_main*2 ])
yScale_main = d3.scaleLinear().rangeRound([(height_vis_main*0.9-legend_circle_radius*3),
  (0.05*height_vis_main+legend_circle_radius*3+padding_top_main)])

xScale_mini       = d3.scaleTime().range([0,width_vis_mini - padding_hor_mini*2])
yScale_mini       = d3.scaleLinear().rangeRound([(height_vis_mini*0.9-legend_circle_radius_mini*3), 
  (0.05*height_vis_mini+legend_circle_radius_mini*3+padding_top_mini)])
yScale_mini_array = []      

// Format numbers
function formatNumber(num) {
  num = num.toFixed()
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}
/// * MISC GLOBALS

// Dates
today_raw = new Date()
dd        = String(today_raw.getDate()).padStart(2, "0")
mm        = String(today_raw.getMonth() + 1).padStart(2, "0")
today     = "2021-" + mm + "-" + dd

// Data
county_data_crosswalk_current = ""
county_data_master            = ""
county_data_raw_master        = ""
county_data_current           = ""
map_data_current              = ""

// Parameters
relax_date_default_minus = timeConv(today)
relax_date_default_minus = timeForm_org(relax_date_default_minus.setDate(relax_date_default_minus.getDate() - 1))
relax_date_default       = today 
relax_date_default_plus  = timeConv(today)
relax_date_default_plus  = timeForm_org(relax_date_default_plus.setDate(relax_date_default_plus.getDate() + 1))
relax_date_current       = relax_date_default

relax_speed_default      = 30
relax_speed_current      = relax_speed_default

state_default            = "MA"
state_default_id         = 25
county_default           = "Suffolk"
county_default_id        = 25025

data_column_default      = "cumulative"
data_column_current      = data_column_default

actual_hide              = false
drag                     = false
init                     = 0
info_close               = true
county_mode              = false
county_selected          = ''

// CSV
headersVal = Papa.unparse({ fields: ['State', 
                  'County',
                  'FIPS',
                  'Date',
                  'Actual - Daily Cases',
                  'Actual - Cumulative Cases',
                  'Relax Date',
                  'Relax Speed',
                  'Stay Home Ratio',
                  'Baseline - Daily Cases',
                  'Baseline - Cumulative Cases',
                  'Scenario - Daily Cases',
                  'Scenario - Cumulative Cases',
                  'Scenario - R_t'], data: [] });

// Social mobility
function stay_home_calc(mode, county_lookup, max_date="") {

  if (mode=="current") {
    date_lookup     = timeConv(today)
    index           = bisect(county_data_current[county_data_current.length-1].values, date_lookup, 1)
    stay_home_ratio = 0
    county_data_current.forEach(function(d,i) {
      if (d.key == county_lookup) {
        stay_home_ratio = d.values[index].stay_home_ratio * 100 
      }
    })
} else if (mode=="one_month") {


    date_lookup     = timeConv(timeForm_org(relax_date_current))
    if (date_lookup.getHours()!=0) {
      date_lookup     = timeConv(timeForm_org(date_lookup.setDate(date_lookup.getDate() +30 )))
    } else {
      date_lookup     = timeConv(timeForm_org(date_lookup.setDate(date_lookup.getDate() +31 )))

    }

    if (xScale_main(date_lookup)<xScale_main(max_date)) {


    date_lookup     = date_lookup
    
    index           = bisect(county_data_current[county_data_current.length-1].values, date_lookup, 1)
    stay_home_ratio = 0
    county_data_current.forEach(function(d,i) {
      if (d.key == county_lookup) {
        stay_home_ratio = d.values[index].stay_home_ratio * 100
      }
    })
    } else {

    date_lookup     = date_lookup
    stay_home_ratio = 0
      //date_lookup     = timeConv(graphsetup.latest_prediction_date)
      //date_lookup     = timeConv(timeForm_org(date_lookup.setDate(date_lookup.getDate() +10 )))
      
    }


  } else if (mode=="latest_date") {

    date_lookup     = max_date
    index           = bisect(county_data_current[county_data_current.length-1].values, date_lookup, 1)
    stay_home_ratio = 0
    county_data_current.forEach(function(d,i) {
      if (d.key == county_lookup) {
        stay_home_ratio = d.values[index].stay_home_ratio * 100
      }
    })  
  }
  
  return([date_lookup, stay_home_ratio])
}


// ----------------------------------------
// ### HELPER FUNCTIONS
// ----------------------------------------

// states
function states(crosswalk_data) {
   states_list      = []
   states_name_list = []
   crosswalk_data.forEach(function(d) {
      states_list.push(d.key)
      states_name_list.push(d.values[0].state_name)
    }) 
  return([states_list,states_name_list] ) 
}

// state_conv
function state_conv(crosswalk_data, state, mode="id") {
   state_id   = ""
   state_name = ""
   crosswalk_data.forEach(function(d) {
      if (d.key==state) {
        state_id   = d.values[0].state_fips
        state_name = d.values[0].state_name
      }
   }) 
   if (mode=="id") {
    return(state_id) 
  } else if (mode=="name") {
    return(state_name) 
  }
}

// counties
function counties(crosswalk_data, state, lowercase=false) {

  counties_list_actual = []

  crosswalk_data.forEach(function(d) {
    if (d.key==state) {
      d.values.forEach(function(e) {
        if (lowercase==false) {
          counties_list_actual.push(e.county)
        } else if (lowercase==true) {
          counties_list_actual.push(d.county.toLowerCase())
        }
      })
    }
  })

  return(counties_list_actual) 
}


// ----------------------------------------
// ### MAIN
// ----------------------------------------

// Make client-side API call to obtain the user"s location
$.getJSON("http://ip-api.com/json", function (longlatdata, status) {

    // Inspect the data 
    console.log(longlatdata)
    latitude  = longlatdata.lat
    longitude = longlatdata.lon

    // Testing with various lat/long
    //latitude  = 40.730610
    //longitude = -73.935242

    //latitude  = 42.361145
    //longitude = -71.057083

    //latitude  = 42.49 
    //longitude = -71.39

    //latitude  = 41.881832
    //longitude = -87.623177
    
    // Make reverse geocoding request to obtain county
    rev_geocoding_request = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+
      longitude+"&result_type=administrative_area_level_2&key=AIzaSyAttZrqwRZ-38y-a4AFr8SfO2qiNlJusLc"
    $.getJSON(rev_geocoding_request, function (locationdata, status) {

      console.log(locationdata)

     // Obtain address
     try {
        if (locationdata.status=="OK" && locationdata.results[0].formatted_address.split(",").length>=3) {
          
          address  = locationdata.results[0].formatted_address
          console.log(address)
          county   = address.split(",")[address.split(",").length-3].trim().split(" ")[0].trim()
          state    = address.split(",")[address.split(",").length-2].trim()
          country  = address.split(",")[address.split(",").length-1].trim()
          
        } else {

          address  = "Longitude: " + longitude + " / Latitude: " + latitude
          county   = ""
          state    = ""
          country  = ""

        }  
      } catch(err) { 
          address  = "Longitude: " + longitude + " / Latitude: " + latitude
          county   = ""
          state    = ""
          country  = ""   
      } 

      // Inspect the data 
      console.log(address)
      console.log(county)
      console.log(state)
      console.log(country)

      // Load the visualization crosswalk
      county_data_crosswalk_link = d3.csv("/load_county_data_crosswalk")

      county_data_crosswalk_link.then(function(county_data_crosswalk) {

        // Initialize the data
        county_data_crosswalk_current = d3.nest() 
                                          .key(function(d) {return d.state})
                                          .entries(county_data_crosswalk)       


        // Determine state & county choice
        if (country=="USA" & states(county_data_crosswalk_current)[0].includes(state)) {

          // Customizing state
          state_choice      = state
          state_stored      = state
          state_choice_rank = states(county_data_crosswalk_current)[0].indexOf(state_choice)
          state_choice_id   = +county_data_crosswalk_current[state_choice_rank].values[0].county_id.slice(0,-3)

          // Customizing county
          if (counties( county_data_crosswalk_current, state).includes(county)) {
            county_choice      = county
            county_stored      = county
            county_choice_rank = counties(county_data_crosswalk_current, state_choice).indexOf(county)
            county_choice_id   = +county_data_crosswalk_current[state_choice_rank].values[county_choice_rank].county_id
            county_stored_id   = +county_data_crosswalk_current[state_choice_rank].values[county_choice_rank].county_id
          
          } else {
            county_choice    = "none"
            county_stored    = "none"
            county_choice_id =  0
            county_stored_id =  0
          }
          
        } else {

          // Stick with default location
          state_choice        = state_default
          state_stored        = state_default
          state_choice_id     = state_default_id
          state_choice_rank   = states(county_data_crosswalk_current)[0].indexOf(state_choice)
          county_choice       = county_default
          county_stored       = county_default
          county_choice_id    = county_default_id
          county_stored_id    = county_default_id
        }

        // Load the visualization data
        county_data_link = d3.csv("/load_county_data?state="+state_choice+"&relax_year="+
          year(timeConv(relax_date_default))+"&relax_month="+
          month(timeConv(relax_date_default))+"&relax_day="+day(timeConv(relax_date_default))+"&relax_speed="+relax_speed_current)
        map_data_link    = d3.json("/frontend/data/map/us_temp_final_"+state_conv(county_data_crosswalk_current,state_choice, "id")+".json")

        county_data_link.then(function(county_data_raw) {
          map_data_link.then(function(map_data_raw) {

          // ----------------------------------------
          // ### FUNCTIONS
          // ----------------------------------------

          /// InitializeData
          // ----------------------------------------
          function InitializeData(raw_data) {

            /// Format data
            raw_data.forEach(function(d) {
                d.county                          = d.county
                d.state                           = d.state
                d.county_id                       = +d.fips
                d.r_t                             = +d.scenario_R_t
                d.stay_home_ratio                 = +d.stay_home_ratio
                d.date                            = timeConv(d.date)
                
                d.cumulative                      = +d.scenario_cumulative
                d.daily                           = +d.scenario_daily 

                d.baseline_cumulative             = +d.baseline_cumulative 
                d.baseline_daily                  = +d.baseline_daily 

                d.actual_cumulative               = +d.actual_cumulative   
                d.actual_daily                    = +d.actual_daily

            })
          
            /// Group data
            raw_data_group = d3.nest() 
                             .key(function(d) {return d.county})
                             .entries(raw_data)
          
            /// Return data
            return(raw_data_group)
          
          }
        

         function InitializeDownload(raw_data) {

            /// Format data
            raw_data.forEach(function(d) {

           //d.fips = jQuery.extend(true, [], d.county_id)
            //delete d.county_id

            d.fips = +d.fips
            d.fips = d.fips.toFixed(0)

            d.scenario_daily = +d.scenario_daily
            d.scenario_cumulative = +d.scenario_cumulative
            d.scenario_R_t = +d.scenario_R_t

            d.baseline_daily = +d.baseline_daily
            d.baseline_cumulative = +d.baseline_cumulative

            d.actual_daily = +d.actual_daily
            d.actual_cumulative = +d.actual_cumulative

            d.scenario_daily = d.scenario_daily.toFixed(0)
            d.scenario_cumulative = d.scenario_cumulative.toFixed(0)
            d.scenario_R_t = d.scenario_R_t.toFixed(2)

            d.baseline_daily = d.baseline_daily.toFixed(0)
            d.baseline_cumulative = d.baseline_cumulative.toFixed(0)

            d.actual_daily = d.actual_daily.toFixed(0)
            d.actual_cumulative = d.actual_cumulative.toFixed(0)

            d.stay_home_ratio =  +d.stay_home_ratio 
            d.stay_home_ratio =  d.stay_home_ratio.toFixed(3)

            delete d[""]

            if(d.actual_daily==-1) {
                d.actual_daily=""
            }

            if(d.actual_cumulative==-1) {
                d.actual_cumulative=""
            }
              
            if(d.county=="ZZAggregate") {
                d.county="Aggregate"
            }
              
            })


            return(raw_data)
          }


          
          /// InitializeGraph
          // ----------------------------------------
          function InitializeGraph(data_column_selected=data_column_current) {
          
            /// xscale
            minDate = d3.min(county_data_current, function(d) {
                return d3.min(d.values, function (f) {
                    return f.date
                })
            })
            maxDate = d3.max(county_data_current, function(d) {
                return d3.max(d.values, function (f) {
                    return f.date
                })
            })

            latest_prediction_date   = timeConv(timeForm_org(maxDate))
            latest_prediction_date   = timeForm_org(latest_prediction_date.setDate(latest_prediction_date.getDate() - 6))

            maxDate_plus_one   = timeConv(timeForm_org(maxDate))
            maxDate_plus_one   = timeConv(timeForm_org(maxDate_plus_one.setDate(maxDate_plus_one.getDate() + 1)))
            
            /// yscale
            minValue_array = []
            county_data_current.forEach(function(d) {
              d.values.forEach(function(f) {
                minValue_array.push(+f[data_column_selected])
                minValue_array.push(+f["baseline_"+data_column_selected])
                minValue_array.push(+f["actual_"+data_column_selected])
              }) 
            })
            minValue = Math.max(0,d3.min(minValue_array))

            maxValue_main_array = []
            county_data_current[county_data_current.length-1].values.forEach(function(f) {
                maxValue_main_array.push(+f[data_column_selected])
                maxValue_main_array.push(+f["baseline_"+data_column_selected])
                maxValue_main_array.push(+f["actual_"+data_column_selected])
            })
            maxValue_main = d3.max(maxValue_main_array)

            maxValue_mini_array = []
            county_data_current.slice(0,-1).forEach(function(d,i) {
              maxValue_mini_array_temp = []
              d.values.forEach(function(f) {
                maxValue_mini_array_temp.push(+f[data_column_selected])
                maxValue_mini_array_temp.push(+f["baseline_"+data_column_selected])
                maxValue_mini_array_temp.push(+f["actual_"+data_column_selected])
              })
              maxValue_mini = d3.max(maxValue_mini_array_temp) *1.2
              maxValue_mini_array.push(maxValue_mini)
            })
            
            return {
                  minDate: minDate,
                  maxDate: maxDate,
                  maxDate_plus_one: maxDate_plus_one,
                  minValue: minValue,
                  maxValue_main: maxValue_main*1.2,
                  maxValue_mini: maxValue_mini_array,
                  maxValue_mini_agg: d3.max(maxValue_mini_array),
                  latest_prediction_date: latest_prediction_date,
              }
          }
          
          /// DrawTop
          // ----------------------------------------
          function DrawTop() {
             
            svg_top.append("g")
                   .append("text")
                   .attr("transform", "translate(" + (0) + "," + (height_top/7)*0.5 +")")
                   .text("COVID-19 Infection Trajectories")
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")

            svg_top.append("g")
                   .append("text")
                   .attr("id", "svg_top_small")
                   .attr("transform", "translate(" + (0) + "," + (height_top/7)*2 +")")
                   .text(function() {
                    if (windowWidth > mobileWidth) {
                    return("We, a team affiliated with MIT's COVID-19 Policy Alliance, provide county-level infection projections with configurable scenarios based on social mobility.")
                   } else {
                    return("We provide county-level infection projections with configurable scenarios based on social mobility.")

                   }})
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")
                   .style("font-weight","normal")
                   .style("opacity","0.75")

            svg_top.append("g")
                   .append("text")
                   .attr("id", "svg_top_small")
                   .attr("transform", "translate(" + (0) + "," + (height_top/7)*3 +")")
                   .html(function() {
                    if (windowWidth>mobileWidth ) {
                      return("Scenarios are specified by relax dates (the date when mobility starts to increase) and a speed of mobility changes (how quickly mobility returns to pre-COVID levels).")
                    } else {
                      //return("Scenarios are specified by relax dates <tspan class='fa info-icon' id='info-icon-1' data-toggle='tooltip' title='The date when mobility starts to increase'>\uf059</tspan> and speed of mobility changes <tspan class='fa info-icon' id='info-icon-2' data-toggle='tooltip' title='How quickly mobility returns to pre-COVID levels'>\uf059</tspan>")
                      return("Scenarios are specified by relax dates and speed of mobility changes.")

                      return
                    }
                  })

                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")
                   .style("font-weight","normal")
                   .style("opacity","0.75")

            //d3.selectAll(".info-icon")
            //  .style("font-size", "13px")
            //  .style("dominant-baseline", "ideographic")
             // .style("font-weight", "bold")

            //$('#info-icon-1').tooltip()
            //$('#info-icon-2').tooltip()

            svg_top.append("g")
                   .append("text")
                   .attr("id", "svg_top_small")
                   .attr("transform", "translate(" + (0) + "," + (height_top/7)*4 +")")
                   .html("<a href='#' class='sliding-link'>Learn more about the model</a> | <a href=https://www.covidalliance.com/ class='sliding-link-2'>Visit MIT's COVID-19 Policy Alliance</a>")
                   .style("dominant-baseline", "central")

            d3.selectAll(".sliding-link")
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")
                   .style("font-weight","bold")
                   .style("opacity","0.75")
                   .on("mouseover", function() {
                      d3.select(this).style("cursor", "pointer")
                   })
                   .on("mouseout", function() {
                      d3.select(this).style("cursor", "")
                   })

            d3.selectAll(".sliding-link-2")
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")
                   .style("font-weight","bold")
                   .style("opacity","0.75")
                   .on("mouseover", function() {
                      d3.select(this).style("cursor", "pointer")
                   })
                   .on("mouseout", function() {
                      d3.select(this).style("cursor", "")
                   })


            $(".sliding-link").click(function(e) {
              e.preventDefault();
              target = "#svg_bottom_container";
              $('html,body').animate({scrollTop: $(target).offset().top},'slow');
            });



          if (windowWidth > mobileWidth) {
            menu_main = svg_top.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (0)+ "," + (height_top/7)*4.75 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*2)
                  .attr("height",button_height_main*4)
                  .attr("id","menu_1")
               
              states_name_list_dropdown = states(county_data_crosswalk_current)[1]
              states_list_dropdown      = states(county_data_crosswalk_current)[0]
              
              $select = $('<select/>', {
                'class':"selectpicker", 
                'id':"menu-2",
                'data-live-search':true,
                'data-size':3, 
                'title':'Select a state'

              });

              for (var i = 0; i < states_list_dropdown.length; i++) {
                    $select.append('<option id='+states_list_dropdown[i]+' value=' + states_list_dropdown[i] + 
                      '>' + states_name_list_dropdown[i]+ '</option>');
              }

              $select.appendTo('#menu_1').selectpicker('val', [state_choice]);

              d3.selectAll('.btn-light').attr("class", "btn dropdown-toggle btn-xss-mod")

              $('#menu-2').on('change', function(e) {
                selector='#'+this.value
                UpdateData(state = this.value)  
  
              })
            } else {

            menu_main = svg_top.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (0)+ "," + (height_top/7)*4.75 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*2)
                  .attr("height",button_height_main*4)
                  .attr("id","menu_1")
               

              menu_main.append("xhtml:select")
                    .attr("id","button_mode_"+(101))
                    .attr("class","dropdown dropdown-menu-1 btn btn-xss-mod-mobile")
                    .html("Select a state")
              
            
              menu              = document.querySelector('.dropdown-menu-1');

              states_name_list_dropdown = states(county_data_crosswalk_current)[1]
              states_list_dropdown      = states(county_data_crosswalk_current)[0]

                
                menu.innerHTML += "<option selected disabled>Select a state</option>"
                for (var i = 0; i < states_list_dropdown.length; i++) {
                  menu.innerHTML += "<option id='" + states_list_dropdown[i] + "' class='dropdown-item dropdown-item-1' href='#'>"+states_name_list_dropdown[i]+"</option>";
                }

                $('#button_mode_101').on('change', function(e) {
                    
                    selector = states_list_dropdown[states_name_list_dropdown.indexOf(this.value)]
                    UpdateData(state = selector)  
               
                })

                // Select default
                $('#button_mode_101').val(states_name_list_dropdown[states_list_dropdown.indexOf(state_choice)])
               

            }

          }
 

          /// DrawMap
          // ----------------------------------------
          function DrawMap() {
          
            // Clear
            svg_map_temp = d3.selectAll("#svg_map")
            svg_map_temp.selectAll("*").remove();
            d3.select("#svg_map").on(".zoom", null)
            
            // Initialize 
            graphsetup = InitializeGraph()

            // Intialize map objects
            states_data = topojson.feature(map_data_current, map_data_current.objects.states)
            state       = states_data.features.filter(function(d) { return d })[0]
           
            projection = d3.geoAlbers()
            projection.scale(1).translate([0, 0])
            path       = d3.geoPath().projection(projection)
          
            b = path.bounds(state)
            s = .95 / Math.max((b[1][0] - b[0][0]) / (width_vis_main*0.85-20), (b[1][1] - b[0][1]) / (height_vis_main*0.62))
            t = [(width_vis_main*0.85 - s * (b[1][0] + b[0][0])) / 2, (height_vis_main - s * (b[1][1] + b[0][1])) / 2]
  
            projection.scale(s).translate(t)
          
            // Generate header
            svg_map.append("g")
                   .append("text")
                   .attr("transform", "translate(" + (0)+ ","+(padding_top_main/4)+")")
                   .text("Overview of infections and transmission rates in " + state_conv(county_data_crosswalk_current, state_choice,"name"))
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")

            svg_map.append("g")
                   .append("text")
                   .attr("id", "svg_map_small")
                   .attr("transform", "translate(" + (0)+ ","+(padding_top_main/4)*2+")")
                   .text("Effective reproduction number (R_t) as of " + timeForm(timeConv(today)))
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")

            // Generate button
            svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (0)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .attr("height",button_height_main*1.5)
                  .style("text-align","left")
                  .on("click", function() {
                    DrawBar()
                  })
                  .append("g")
                  .append("xhtml:button")
                  .attr("id","button_mode_"+(2))
                  .attr("class","btn btn-outline-dark btn-xs modebutton")
                  .html("Bar chart view")



          if (windowWidth>mobileWidth) {
            menu_main = svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_main*1.5)
                  .attr("class", "input-group")
                  .style("flex",null)
                  .append("g")
                
            menu_main
                  .append("xhtml:input")
                  .attr("id","button_mode_"+(2))
                  .attr("class","form-control")
                  .attr("type","text")
                  .attr("id","myInput")
                  .attr("autocomplete","new-password")
                  .attr("placeholder","Search for a county")
                  .style("display","inline-block")
                  .attr("class", "btn btn-xs")
                  .style("line-height", "0.7")
                  .style("font-size", "16px")
                  .style("padding", "5px 15px")
                  .style("padding-top","8px")
                  .attr("width",button_width_main*1.9)
                  .on("mouseover", function() {
                     d3.select(this).style("cursor", "")
                  })

            $("#myInput").on("keyup", function() {
                value       = $(this).val().toLowerCase();             
                county_list = []
                counties(county_data_crosswalk_current, state_choice,lowercase=false).forEach(function(a){
                    if (typeof(a) == 'string' && a.toLowerCase().indexOf(value)>-1) {
                        county_list.push(a)
                    }        
                 });
                    
                if (county_list.length>0 & county_list.length<(county_data_current.length-1)) {
              
                    d3.selectAll(".baractive").attr("class", "bar")
                    d3.selectAll(".baractive_mini").attr("class", "bar_mini")
                    d3.selectAll(".county-hover").attr("class", "county")
                    d3.selectAll(".county-hover-fixed").attr("class", "county")
                    
                    d3.selectAll(".bar_mini" ).attr("class", function(e)  {
                       if (county_list.indexOf(e.key)>-1) {
                            return ("baractive_mini bar_mini")
                        } else {
                            return ("bar_mini")
                        }
                    })
            
                    d3.selectAll(".bar" ).attr("class", function(e)  {
                       if (county_list.indexOf(e.key)>-1) {
                            return ("baractive bar")
                        } else {
                            return ("bar")
                        }
                    })
            
                    if (county_list.length==1) {
                      county_index = counties(county_data_crosswalk_current, 
                        state_choice,lowercase=false).indexOf(county_list[0])
                      
                      rt = 0
                      county = county_list[0]
                      county_data_current.forEach(function(e) {
                        if(e.key==county_list[0]) {
                          rt=(e.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t)
                          rt_future=(e.values[bisect(county_data_current[county_data_current.length-1].values, graphsetup.maxDate, 1)].r_t)
                       

                        }
                      })

                      rt = rt.toFixed(2)
                                            

                      d3.selectAll("#tooltip").html("<b>"+county_list[0]+"</b>" + " - R_t: " + 
                        rt)
            
                    }

                    county_data_current.forEach(function(e) {
                          if(county_list.indexOf(e.key)>-1) {
                            d3.selectAll(".county").attr("class", function(f) {
                              if (f.id==e.values[0].county_id) {
                                return("county-hover")
                              } else {
                                return("county")
            
                              }
                            })
                          }
                    }) 

                } else {
                    d3.selectAll(".bar_label").attr("class", "bar_labelactive bar_label")
                    d3.selectAll(".bar").attr("class", "baractive bar")
                    d3.selectAll(".bar_mini").attr("class", "baractive_mini bar_mini")
                    d3.selectAll(".county").attr("class", "county-hover")

                    rt = county_data_current[county_data_current.length-1].values[bisect(county_data_current[county_data_current.length-1].values, 
                      timeConv(today), 1)].r_t
                    rt = rt.toFixed(2)

                    rt_future = county_data_current[county_data_current.length-1].values[bisect(county_data_current[county_data_current.length-1].values, 
                      graphsetup.maxDate, 1)].r_t
                    rt_future = rt_future.toFixed(2)

                    if (windowWidth> mobileWidth) {
                      d3.selectAll("#tooltip").html("Hover over or click on a county to see details")
                    } else {
                      d3.selectAll("#tooltip").html("Click on or select a county to see details")
                    } 
                }
            });

          } else {

            menu_main = svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_main*1.5)
                  .attr("class", "input-group")
                  .style("flex",null)
                  .append("g")
                  .style("width","100%")

          menu_main.append("xhtml:select")
                    .attr("id","button_mode_"+(105))
                    .attr("class","dropdown dropdown-menu-4  modebutton")
                    .html("Select a county")
              
            
          menu              = document.querySelector('.dropdown-menu-4');
          county_list       = counties( county_data_crosswalk_current, state_choice,lowercase=false)
                
          menu.innerHTML += "<option selected disabled>Select a county</option>"
          for (var i = 0; i < county_list.length; i++) {
            menu.innerHTML += "<option id=countyoption'" + i + "' class='dropdown-item dropdown-item-1' href='#'>"+county_list[i]+"</option>";
          }

          $('#button_mode_105').on('change', function(e) {
           
              county_chosen   =  this.value
              
              d3.selectAll(".bar_labelactive").attr("class", "bar_label")
              d3.selectAll(".baractive").attr("class", "bar")
              d3.selectAll(".baractive_mini").attr("class", "bar_mini")
                
              d3.selectAll(".bar_mini" ).attr("class", function(e)  {
                   if (e.key == county_chosen) {
                        return ("baractive_mini bar_mini")
                    } else {
                        return ("bar_mini")
                    }
              })

                d3.selectAll(".bar" ).attr("class", function(e)  {
                   if (e.key == county_chosen) {
                        return ("baractive bar")
                    } else {
                        return ("bar")
                    }
                })

  
                county_index = counties( county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county_chosen)
                d3.selectAll("#tooltip").html("<b>"+county_chosen+"</b>" + " - Actual cumulative cases: " + 
                    formatNumber(county_data_current[county_index].values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative)
                    + " | Predicted cumulative cases: " 
                    + formatNumber(county_data_current[county_index].values[county_data_current[county_index].values.length-1].baseline_cumulative))


                county_data_current.forEach(function(e) {
                       if(e.key==county_chosen) {
                         d3.selectAll(".county").attr("class", function(f) {
                           if (f.id==e.values[0].county_id) {
                             return("county-hover")
                           } else {
                             return("county")
                 
                           }
                         })
                       }
                }) 


                d3.selectAll(".bar_label").attr("class", function(f) {
                    if (f==county_chosen) {
                      return("bar_labelactive bar_label")
                    } else {
                      return("bar_label")
          
                    }
                }) 
          })


          }


            rt = county_data_current[county_data_current.length-1].values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t
            rt = rt.toFixed(2)

            rt_future = county_data_current[county_data_current.length-1].values[bisect(county_data_current[county_data_current.length-1].values, graphsetup.maxDate, 1)].r_t
            rt_future = rt_future.toFixed(2)

            svg_map.append("g")
                   .style("overflow", "visible")
                   .attr("transform", "translate(" + (0)+ "," + (padding_top_main/4)*3.7 +")")
                   .append("foreignObject")
                   .style("overflow", "visible")
                   .attr("width",button_width_main*7)
                   .attr("height",button_height_main*1.5)
                   .append("xhtml:div")
                   .style("text-align","left")
                   .attr("height",button_height_main*1.5)
                   .append("g")
                   .append("xhtml:button")
                   .attr("id","tooltip")
                   .attr("class","btn btn-outline-dark btn-xss-tooltip modebutton")
                   .html(function() {
                    if (windowWidth>mobileWidth) {
                    return("Hover over or click on a county to see details")
                  } else {
                    return("Click on or select a county to see details")
                  }
                  })
                   .style("cursor","default")

            // Draw Graph
            svg_map.selectAll("path")
                    .data(topojson.feature(map_data_current, map_data_current.objects.counties).features)
                    .enter()
                    .append("path")
                    .attr("d", path)
                    .attr("class", function(d) {
                      if (state_choice == state_stored) {
                      if(d.id==county_stored_id) {
                        return("county county-hover")
                      } else {
                        return("county")
                      }
                    } else {
                       return("county county-hover")
                    }
                    })
                    .attr("county-name", function(d) {
                      county_name = "undefined"
                      county_data_current.forEach(function(e) {
                        if(e.values[0].county_id==d.id) {
                          county_name = e.key
                        }
                      })
                      return(county_name)
                    })
                    .attr("county-id", function(d) {
                      return d.id
                    })
                    .on("mouseover", function(d) {
                      d3.select(this).style("cursor", "pointer")
                      if (windowWidth>mobileWidth) { 
                        $("#myInput").val("") 
                      } else {
                        $("#button_mode_105").val("Select a county") 
                      }
                      d3.selectAll(".county-hover").attr("class", "county")
                      d3.select(this).attr("class", "county county-hover")
               
                      county_data_current.forEach(function(e) {
                        
                        if(e.values[0].county_id==d.id) {
                          d3.selectAll(".bar").attr("class", function(f) {
                            if (f.key==e.values[0].county) {
                              return("baractive bar")
                            } else {
                              return("bar")
          
                            }
                          })
                          d3.selectAll(".bar_mini").attr("class", function(f) {
                            if (f.key==e.values[0].county) {
                              return("baractive_mini bar_mini")
                            } else {
                              return("bar_mini")
          
                            }
                          })
                          d3.selectAll(".serie_label").attr("class", function(f) {

                            if (f.id==e.values[0].county) {
                              return("serie_label serie_labelactive")
                            } else {
                              return("serie_label")
          
                            }
                          })
                        }
                      })

                      rt = 0
                      county = ""
                      county_data_current.forEach(function(e) {
                        if(e.values[0].county_id==d.id) {
                          county = e.values[0].county
                          rt=(e.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t)
                          rt_future=(e.values[bisect(county_data_current[county_data_current.length-1].values, graphsetup.maxDate, 1)].r_t)
                       

                        }
                      })
                      if (county!="") {
                        rt = rt.toFixed(2)
                        rt_future = rt_future.toFixed(2)
                        d3.selectAll("#tooltip").html("<b>"+county+"</b>" + " - R_t: " + rt )
                      } else {

                        d3.selectAll("#tooltip").html("No data")

                      }
                    })
                 
                  .on("mouseout", function() {
                    d3.select(this).style("cursor", "")
                    d3.selectAll(".county").attr("class", "county-hover")

                    rt = county_data_current[county_data_current.length-1].values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t
                    rt = rt.toFixed(2)

                    rt_future = county_data_current[county_data_current.length-1].values[bisect(county_data_current[county_data_current.length-1].values, graphsetup.maxDate, 1)].r_t
                    rt_future = rt_future.toFixed(2)

                    if (windowWidth> mobileWidth) {
                      d3.selectAll("#tooltip").html("Hover over or click on a county to see details")
                    } else {
                      d3.selectAll("#tooltip").html("Click on or select a county to see details")
                    } 
                  })
                  .on("click", function(d,i) {
 
                    d3.selectAll().transition().duration(0)
                      county = ""
                      county_data_current.forEach(function(e) {
                        if(e.values[0].county_id==d.id) {
                          county = e.values[0].county
                        }
                      })

                      if (county!='') {
                      county_index = counties(county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county)
                      target       = "#svg_graph_"+(county_index+1);
                      //$('html,body').animate({scrollTop: $(target).offset().top},'slow');           
                      UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=relax_speed_current, agg_county=county_index)

                      if (windowWidth<=mobileWidth) {
                         target="#svg_graph_0"
                         $('html,body').animate({scrollTop: $(target).offset().top},'slow');
                      }

                    }

                  //  county_data_current.forEach(function(d,f) {

                  //     if ((f!=(county_index+1))) {
                  //       d3.selectAll("#svg_graph_"+f).attr("opacity","0.3")
                  //     } else {
                  //       d3.selectAll("#svg_graph_"+f).attr("opacity","1")
                   //    }

                   //  })

                   //  county_data_current.forEach(function(d,g) {

                   //    if (g!=(county_index+1)) {
                   //      d3.selectAll("#svg_graph_"+g)
                    //       .transition()
                    //      .delay(200)
                    //       .duration(2000)
                     //      .attr("opacity","1")
                    //   }
                   //  })

                   })
                
                svg_map.append("path")
                   .datum(states_data)
                   .attr("class", "outline")
                   .attr("d", path)
                   .attr("id", "land")
      
            // Link counties to rt & colour based on r_t
            d3.selectAll(".county").each(function(d,i) {
              county = d3.select(this).attr("county-name")
              color_choice = "white"
              d3.select(this).attr("fill", function() {
                county_data_current.forEach(function(e) {
                  if(e.key==county) {
                    if (e.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t>1) {
                    color_choice=color_bad((e.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t))
                  } else {
                    color_choice=color_good((e.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t))

                  }
                  }
                })
                return(color_choice)
              })
            })
          
            d3.selectAll(".county").each(function(d,i) {
              county = d3.select(this).attr("county-name")
              rt     = "undefined"
              d3.select(this).attr("rt", function() {
                county_data_current.forEach(function(e) {
                  if(e.key==county) {
                    rt=e.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t 
                  }
                })
                return(rt)
              })
            })
          
          }

          /// DrawBar
          // ----------------------------------------
          function DrawBar() {

            // Clear
            svg_map_temp = d3.selectAll("#svg_map")
            svg_map_temp.selectAll("*").remove();
            
            // Initialize variables
            graphsetup = InitializeGraph(data_column_selected="cumulative")

            // Initialize
            svg_map.append("g")
                   .append("text")
                   .attr("transform", "translate(" + (0)+ ","+(padding_top_main/4)+")")
                   .text("Overview of infections and transmission rates in " + state_conv(county_data_crosswalk_current, state_choice,"name"))
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")

            svg_map.append("g")
                   .append("text")
                   .attr("id","svg_graph_small")
                   .attr("transform", "translate(" + (0)+ ","+(padding_top_main/4)*2+")")
                   .text("Actual cumulative cases as of " +timeForm(county_data_current[0].values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].date) + " and predicted cumulative cases by " + timeForm(graphsetup.maxDate))
                   .style("text-anchor", "start")
                   .style("dominant-baseline", "central")

            // Data
            county_data_bar = jQuery.extend(true, [], county_data_current.slice(0,-1))
            county_data_bar.sort(function(a, b) {
              return d3.descending(a.values[a.values.length-1].baseline_cumulative, b.values[b.values.length-1].baseline_cumulative)
            })

            // Axes 
            svg_graph_layer_2 =  svg_map.append("g")
                                        .attr("transform", "translate(" + 0 + ","+(height_vis_main*0.9-legend_circle_radius*3) +")")

            svg_graph_layer_2.append("rect")
                             .attr("class",function() {
                                if(svg_id==-1) {
                                  return("leftrect leftrect_main")
                                } else {
                                  return("leftrect leftrect_mini")
                                }
                              })
                             .attr("id","leftrect_bar")
                             .attr("height", ((yScale_main.range()[0]-yScale_main.range()[1])))
                             .attr("transform", "translate(" +  0+ ","+((yScale_main.range()[0]-yScale_main.range()[1])*-1)+")")
                             .attr("width", xScale_main.range()[1])
                             .on("mouseover", function() {
                              d3.selectAll("#scroll_zoom").style("background", "black")
                              d3.selectAll("#scroll_zoom_up").style("background", "black")
                             })
                             .on("mouseout", function(){
                              d3.selectAll("#scroll_zoom").style("background", "white")
                              d3.selectAll("#scroll_zoom_up").style("background", "white")
                             })

            svg_map_map = svg_map.append("g").attr("id","zoom_group")

            x = d3.scaleLinear()
                  .domain([0, graphsetup.maxValue_mini_agg])
                  .range([ 0, width_vis_main - padding_hor_main*2])
            svg_map.append("g")
                   .attr("transform", "translate("+0+"," + (height_vis_main*0.9-legend_circle_radius*3) + ")")
                   .attr("class", "x axis")
                   .call(d3.axisBottom(x))
                   .selectAll("text")
                   .attr("transform", "translate(-10,0)rotate(0)")
                   .style("text-anchor", "start")
          
            height_available   = (height_vis_main*0.9-legend_circle_radius*3) - (0.05*height_vis_main+legend_circle_radius*3+padding_top_main)
            bar_count          = county_data_bar.length
            if (bar_count>=12) {
              bar_per_view       = 12
              height_per_bar     = height_available/bar_per_view
            } else {
              bar_per_view       = Math.max(6,bar_count)
              height_per_bar     = height_available/bar_per_view
            }
            
            height_total       = bar_count * height_per_bar
            shift              = 0 
            invisible          = 0
            original_transform = 0
            d3.selectAll("#zoom_group").attr('transform', "translate(" + 0 + ","+(0)+")")

            y = d3.scaleBand()
                  .range([ (0.05*height_vis_main+legend_circle_radius*3+padding_top_main), (height_total+(0.05*height_vis_main+legend_circle_radius*3+padding_top_main))])
                  .domain(county_data_bar.map(function(d) { return d.key }))
                  .padding(.1)
            
            yaxis = svg_map_map.append("g")
                   .attr("transform", "translate("+0+"," + 0 + ")")
                   .attr("class", "y axis")
                   .attr("id", "baryaxis")
                   .call(d3.axisLeft(y))

                    yaxis
                    .selectAll('text')
                    .attr("text-anchor","start")
                    .attr('dx', function(d,i) {
                      return (Math.max(x(county_data_bar[i].values[county_data_bar[i].values.length-1].baseline_cumulative), 
                        x(county_data_bar[i].values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative))+15)
                    })
                    .attr("opacity", function(d,i) {
                        if ((i<invisible) |(i>bar_per_view+invisible-1)) {
                          return(0)
                        } else {
                          return(1)
                        }
                    })
                    .attr("class", "bar_labelactive bar_label")

                   yaxis
                    .selectAll('path')
                    .style("opacity",0)

                    yaxis
                    .selectAll('line')
                    .attr("x1",function(d,i) {
                      return x(county_data_bar[i].values[county_data_bar[i].values.length-1].baseline_cumulative)
                    })
                    .attr("x2",function(d,i) {
                      return x(county_data_bar[i].values[county_data_bar[i].values.length-1].baseline_cumulative)+2
                    })
                    .attr("opacity", function(d,i) {
                        if ((i<invisible) |(i>bar_per_view+invisible-1)) {
                          return(0)
                        } else {
                          return(1)
                        }
                    })

            // Generate graph
            svg_map_map.selectAll("myRect")
                   .data(county_data_bar)
                   .enter()
                   .append("rect")
                   .attr("class",function(d) {
                    if (state_choice==state_stored) {
                        if(d.key==county_stored) {
                          return("baractive bar")
                        } else {
                          return("bar")
                        }
                      } else {
                        return("baractive bar")
                      }
                   })
                   .attr("x",  0)
                   .attr("y", function(d) { return y(d.key) })
                   .attr("width", function(d) { 
                      return (x(d.values[d.values.length-1].baseline_cumulative)) 
                    })

                   .attr("height", y.bandwidth() )
                   .attr("opacity", function(d,i) {
                        if ((i<invisible) |(i>bar_per_view+invisible-1)) {
                          return(0)
                        } else {
                          return(1)
                        }
                    })
                   .on("mouseover", function(d) {

                    d3.select(this).style("cursor", "pointer")
                      if (windowWidth>mobileWidth) { 
                        $("#myInput").val("") 
                      } else {
                        $("#button_mode_105").val("Select a county") 
                      }
                      d3.selectAll(".baractive").attr("class", "bar")
                      d3.selectAll(".baractive_mini").attr("class", "bar_mini")
                      d3.select(this).attr("class", "baractive bar")
                      d3.selectAll(".bar_mini" ).attr("class", function(e)  {
                        if (e.key==d.key) {
                          return ("baractive_mini bar_mini")
                        } else {
                          return ("bar_mini")
                        }
                      })

                      d3.selectAll("#tooltip").html("<b>"+d.key+"</b>" + " - Actual cumulative cases: " + 
                        formatNumber(d.values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative)
                        + " | Predicted cumulative cases: " 
                        + formatNumber(d.values[d.values.length-1].baseline_cumulative))

                      county_data_current.forEach(function(e) {
                        if(e.key==d.key) {
                          d3.selectAll(".county").attr("class", function(f) {
                            if (f.id==e.values[0].county_id) {
                              return("county-hover")
                            } else {
                              return("county")
          
                            }
                          })
                        }
                      })

                      d3.selectAll(".bar_label").attr("class", function(f) {
                            if (f==d.key) {
                              return("bar_labelactive bar_label")
                            } else {
                              return("bar_label")
          
                            }
                      }) 
                    })
                   .on("mouseout", function(d) {
                      d3.select(this).style("cursor", "")
                      d3.selectAll(".bar_label").attr("class", "bar_labelactive bar_label")
                      d3.selectAll(".bar").attr("class", "baractive bar")
                      d3.selectAll(".bar_mini").attr("class", "baractive_mini bar_mini")
                      d3.selectAll(".county-hover").attr("class", "county")
                      
                      if (windowWidth> mobileWidth) {
                        d3.selectAll("#tooltip").html("Hover over or click on a county to see details")
                      } else {
                        d3.selectAll("#tooltip").html("Click on or select a county to see details")
                      } 
                    })
                   .on("click", function(d,i) {

                      county_index = counties(county_data_crosswalk_current, state_choice,lowercase=false).indexOf(d.key)
                      target       = "#svg_graph_"+(county_index+1);
                      //$('html,body').animate({scrollTop: $(target).offset().top},'slow');           
                      UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=relax_speed_current, agg_county=county_index)
                      
                                            if (windowWidth<=mobileWidth) {
                         target="#svg_graph_0"
                         $('html,body').animate({scrollTop: $(target).offset().top},'slow');
                      }

                      //county_data_current.forEach(function(d,i) {
                      //  d3.selectAll("#svg_graph_"+i)
                      //    .attr("opacity","0.3")
                      //    .transition()
                      //    .delay(200)
                      //    .duration(2000)
                      //    .attr("opacity","1")
                      //})

                      //d3.selectAll("#svg_graph_"+(county_index+1))
                      //.attr("opacity","1")

                   })

            svg_map_map.selectAll("myRect2")
                   .data(county_data_bar)
                   .enter()
                   .append("rect")
                   .attr("class",function(d) {
                      if (state_choice==state_stored) {
                        if(d.key==county_stored) {
                          return("baractive_mini bar_mini")
                        } else {
                          return("bar_mini")
                        }
                      } else {
                        return("baractive_mini bar_mini")
                      }
                   })
                   .attr("x", 0)
                   .attr("y", function(d) { return y(d.key) })
                   .attr("width", function(d) {return x(d.values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative)})
                   .attr("height", y.bandwidth() )
                   .attr("opacity", function(d,i) {
                        if ((i<invisible) |(i>bar_per_view+invisible-1)) {
                          return(0)
                        } else {
                          return(1)
                        }
                    })
                   .on("mouseover", function(d) {
                    d3.select(this).style("cursor", "pointer")
                      if (windowWidth>mobileWidth) { 
                        $("#myInput").val("") 
                      } else {
                        $("#button_mode_105").val("Select a county") 
                      }
                      d3.selectAll(".baractive").attr("class", "bar")
                      d3.selectAll(".baractive_mini").attr("class", "bar_mini")
                      d3.select(this).attr("class", "baractive_mini bar_mini")
                      d3.selectAll(".bar" ).attr("class", function(e)  {
                        if (e.key==d.key) {
                          return ("baractive bar")
                        } else {
                          return ("bar")
                        }
                      })
                      county_data_current.forEach(function(e) {
                        if(e.key==d.key) {
                          d3.selectAll(".county").attr("class", function(f) {
                            if (f.id==e.values[0].county_id) {
                              return("county-hover")
                            } else {
                              return("county")
          
                            }
                          })
                        }
                      }) 

                    
                      d3.selectAll("#tooltip").html("<b>"+d.key+"</b>" + " - Actual cumulative cases: " + 
                        formatNumber(d.values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative)
                        + " | Predicted cumulative cases: " 
                        + formatNumber(d.values[d.values.length-1].baseline_cumulative))


                      d3.selectAll(".bar_label").attr("class", function(f) {
                            if (f==d.key) {
                              return("bar_labelactive bar_label")
                            } else {
                              return("bar_label")
          
                            }
                      }) 
                    })
                   .on("mouseout", function(d) {
                      d3.select(this).style("cursor", "")
                      d3.selectAll(".bar_label").attr("class", "bar_labelactive bar_label")
                      d3.selectAll(".bar").attr("class", "baractive bar")
                      d3.selectAll(".bar_mini").attr("class", "baractive_mini bar_mini")
                      d3.selectAll(".county-hover").attr("class", "county")
                      
                      if (windowWidth> mobileWidth) {
                        d3.selectAll("#tooltip").html("Hover over or click on a county to see details")
                      } else {
                        d3.selectAll("#tooltip").html("Click on or select a county to see details")
                      } 
                   })
                   .on("click", function(d,i) {

                      county_index = counties(county_data_crosswalk_current, state_choice,lowercase=false).indexOf(d.key)
                      target       = "#svg_graph_"+(county_index+1);
                      //$('html,body').animate({scrollTop: $(target).offset().top},'slow');           
                      UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=relax_speed_current, agg_county=county_index)
                      
                                            if (windowWidth<=mobileWidth) {
                         target="#svg_graph_0"
                         $('html,body').animate({scrollTop: $(target).offset().top},'slow');
                      }

                       //county_data_current.forEach(function(d,i) {
                      //   d3.selectAll("#svg_graph_"+i)
                       //    .attr("opacity","0.3")
                       //    .transition()
                       //    .delay(200)
                       //   .duration(2000)
                        //   .attr("opacity","1")
                       //})

                       //d3.selectAll("#svg_graph_"+(county_index+1))
                       //.attr("opacity","1")

                    })


            function zoom(svg) {
              zoombehaviour = d3.zoom()                   
              zoombehaviour.on("zoom", zoomed)
              svg.call(zoombehaviour);


              function zoomed() {

  
               if (county_data_bar.length>bar_per_view) {

                  if ((d3.mouse(this)[0]>x.range()[0]) & (d3.mouse(this)[0]<x.range()[1]) & 
                    (d3.mouse(this)[1]>y.range()[0]) & (d3.mouse(this)[1]<y.range()[1])) {
                
                  y_shift          = d3.event.transform.y*-1

                  if (y_shift < original_transform) {
                    y_shift = (+d3.selectAll("#zoom_group")._groups[0][0].getAttribute("transform").split(",")[1].split(")")[0])-10
                  } else {

                    y_shift = (+d3.selectAll("#zoom_group")._groups[0][0].getAttribute("transform").split(",")[1].split(")")[0])+10
                  }
                  original_transform = d3.event.transform.y*-1
                  
                  y_shift        = Math.min(0,Math.max(y_shift, (-1*((bar_count-bar_per_view+1)*height_per_bar))))
                  zoom_helper(y_shift, "zoom")
                }
              }

              }
            }
            
            function zoom_helper(y_shift, mode) {
            
         
                d3.selectAll("#zoom_group").attr('transform', "translate(" + 0 + ","+(y_shift)+")")
                shift              = (+d3.selectAll("#zoom_group")._groups[0][0].getAttribute("transform").split(",")[1].split(")")[0])*-1
              
                invisible          = Math.ceil((shift/height_per_bar ))
                
                d3.selectAll(".bar_mini")
                  .transition().delay("0")
                  .duration("0")
                  .attr("opacity",function(d,i) {
                    if ((i<invisible) |(i>bar_per_view+invisible-2)) {
                      return(0)
                    } else {
                      return(1)
                    }
                  })
                
                d3.selectAll(".bar")
                  .transition()
                  .delay("0")
                  .duration("0")
                  .attr("opacity",function(d,i) {
                    if ((i<invisible) |(i>bar_per_view+invisible-2)) {
                      return(0)
                    } else {
                      return(1)
                    }
                  })

                d3.selectAll("#baryaxis")
                  .selectAll("text")
                  .transition()
                  .delay("0")
                  .duration("0")
                  .attr("opacity",function(d,i) {
                        if ((i<invisible) |(i>bar_per_view+invisible-2)) {
                          return(0)
                        } else {
                          return(1)
                        }
                  })

                d3.selectAll("#baryaxis")
                  .selectAll("line")
                  .transition()
                  .delay("0")
                  .duration("0")
                  .attr("opacity",function(d,i) {
                        if ((i<invisible) |(i>bar_per_view+invisible-2)) {
                          return(0)
                        } else {
                          return(1)
                        }
                  })

                d3.selectAll("#scroll_zoom")
                .style("opacity", function() {
                  if(bar_count>(bar_per_view+invisible)) {
                    return("1")
                  } else {
                    return("0")
                  }
                })
                .style("cursor", function() {
                  if(bar_count>(bar_per_view+invisible)) {
                    return("default")
                  } else {
                    return("default")
                  }
                })

                d3.selectAll("#scroll_zoom_up")
                  .style("opacity", function() {
                   if(invisible>(0)) {
                    return("1")
                   } else {
                    return("0")
                   }
                })
                .style("cursor", function() {
                   if(invisible>(0)) {
                    return("default")
                  } else {
                    return("default")
                  }
                })
              
            }

            // Zoom
            svg_map.call(zoom)

            // Generate button
            svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (0)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_main*1.5)
                  .on("click", function() {
                    DrawMap()
                  })
                  .append("g")
                  .append("xhtml:button")
                  .attr("id","button_mode_"+(2))
                  .attr("class","btn btn-outline-dark btn-xs modebutton")
                  .html("Map view")

            svg_map.append("g")
                   .style("overflow", "visible")
                   .attr("transform", "translate(" + (0)+ "," + (padding_top_main/4)*3.7 +")")
                   .append("foreignObject")
                   .style("overflow", "visible")
                   .attr("width",button_width_main*7)
                   .attr("height",button_height_main*1.5)
                   .append("xhtml:div")
                   .style("text-align","left")
                   .attr("height",button_height_main*1.5)
                   .append("g")
                   .append("xhtml:button")
                   .attr("id","tooltip")
                   .attr("class","btn btn-outline-dark btn-xss-tooltip modebutton")

            if (windowWidth> mobileWidth) {
              d3.selectAll("#tooltip").html("Hover over or click on a county to see details")
            } else {
              d3.selectAll("#tooltip").html("Click on or select a county to see details")
            }           

          if (windowWidth>mobileWidth) {
            menu_main = svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_main*1.5)
                  .attr("class", "input-group")
                  .style("flex",null)
                  .append("g")
                

            menu_main
                  .append("xhtml:input")
                  .attr("id","button_mode_"+(2))
                  .attr("class","form-control SafariFix")
                  .attr("type","text")
                  .attr("id","myInput")
                  .attr("autocomplete","new-password")
                  .attr("placeholder","Search for a county")
                  .style("display","inline-block")
                  .attr("class", "btn btn-xs SafariFix")
                  .style("line-height", "0.7")
                  .style("font-size", "16px")
                  .style("padding", "5px 15px")
                  .style("padding-top","8px")
                  .attr("width",button_width_main*1.9)

            $("#myInput").on("keyup", function() {
                      
              value       = $(this).val().toLowerCase();               
              county_list = []
              
              counties( county_data_crosswalk_current, state_choice,lowercase=false).forEach(function(a){
                if (typeof(a) == 'string' && a.toLowerCase().indexOf(value)>-1) {
                      county_list.push(a)
                }        
              });
                   
              if (county_list.length>0 & county_list.length<(county_data_current.length-1)) {
  
                d3.selectAll(".bar_labelactive").attr("class", "bar_label")
                d3.selectAll(".baractive").attr("class", "bar")
                d3.selectAll(".baractive_mini").attr("class", "bar_mini")
                
                d3.selectAll(".bar_mini" ).attr("class", function(e)  {
                   if (county_list.indexOf(e.key)>-1) {
                        return ("baractive_mini bar_mini")
                    } else {
                        return ("bar_mini")
                    }
                })

                d3.selectAll(".bar" ).attr("class", function(e)  {
                   if (county_list.indexOf(e.key)>-1) {
                        return ("baractive bar")
                    } else {
                        return ("bar")
                    }
                })

                if (county_list.length==1) {
                  county_index = counties( county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county_list[0])
                  d3.selectAll("#tooltip").html("<b>"+county_list[0]+"</b>" + " - Actual cumulative cases: " + 
                    formatNumber(county_data_current[county_index].values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative)
                    + " | Predicted cumulative cases: " 
                    + formatNumber(county_data_current[county_index].values[county_data_current[county_index].values.length-1].baseline_cumulative))

                }

                county_data_current.forEach(function(e) {
                       if(county_list.indexOf(e.key)>-1) {
                         d3.selectAll(".county").attr("class", function(f) {
                           if (f.id==e.values[0].county_id) {
                             return("county-hover")
                           } else {
                             return("county")
                 
                           }
                         })
                       }
                }) 


                d3.selectAll(".bar_label").attr("class", function(f) {
                    if (county_list.indexOf(f)>-1) {
                      return("bar_labelactive bar_label")
                    } else {
                      return("bar_label")
          
                    }
                }) 
              
              } else {
                d3.selectAll(".bar_label").attr("class", "bar_labelactive bar_label")
                d3.selectAll(".bar").attr("class", "baractive bar")
                d3.selectAll(".bar_mini").attr("class", "baractive_mini bar_mini")
                d3.selectAll(".county").attr("class", ".county-hover")

                if (windowWidth> mobileWidth) {
                  d3.selectAll("#tooltip").html("Hover over or click on a county to see details")
                } else {
                  d3.selectAll("#tooltip").html("Click on or select a county to see details")
                }   
              }
               
            })
          } else {

            menu_main = svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_main*1.5)
                  .attr("class", "input-group")
                  .style("flex",null)
                  .append("g")
                  .style("width","100%")

          menu_main.append("xhtml:select")
                    .attr("id","button_mode_"+(105))
                    .attr("class","dropdown dropdown-menu-4  modebutton")
                    .html("Select a county")
              
            
          menu              = document.querySelector('.dropdown-menu-4');
          county_list       = counties( county_data_crosswalk_current, state_choice,lowercase=false)
                
          menu.innerHTML += "<option selected disabled>Select a county</option>"
          for (var i = 0; i < county_list.length; i++) {
            menu.innerHTML += "<option id=countyoption'" + i + "' class='dropdown-item dropdown-item-1' href='#'>"+county_list[i]+"</option>";
          }

          $('#button_mode_105').on('change', function(e) {
           
              county_chosen   =  this.value
              
              d3.selectAll(".bar_labelactive").attr("class", "bar_label")
              d3.selectAll(".baractive").attr("class", "bar")
              d3.selectAll(".baractive_mini").attr("class", "bar_mini")
                
              d3.selectAll(".bar_mini" ).attr("class", function(e)  {
                   if (e.key == county_chosen) {
                        return ("baractive_mini bar_mini")
                    } else {
                        return ("bar_mini")
                    }
              })

                d3.selectAll(".bar" ).attr("class", function(e)  {
                   if (e.key == county_chosen) {
                        return ("baractive bar")
                    } else {
                        return ("bar")
                    }
                })

  
                county_index = counties( county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county_chosen)
                d3.selectAll("#tooltip").html("<b>"+county_chosen+"</b>" + " - Actual cumulative cases: " + 
                    formatNumber(county_data_current[county_index].values.filter(function(e) {return (e.actual_daily!=-1)}).slice(-1)[0].actual_cumulative)
                    + " | Predicted cumulative cases: " 
                    + formatNumber(county_data_current[county_index].values[county_data_current[county_index].values.length-1].baseline_cumulative))


                county_data_current.forEach(function(e) {
                       if(e.key==county_chosen) {
                         d3.selectAll(".county").attr("class", function(f) {
                           if (f.id==e.values[0].county_id) {
                             return("county-hover")
                           } else {
                             return("county")
                 
                           }
                         })
                       }
                }) 


                d3.selectAll(".bar_label").attr("class", function(f) {
                    if (f==county_chosen) {
                      return("bar_labelactive bar_label")
                    } else {
                      return("bar_label")
          
                    }
                }) 
          })


       }


            svg_map.append("g")
                   .style("overflow", "visible")
                   .attr("transform", "translate(" + (x.range()[1])*0.95+ "," + (0.05*height_vis_main+legend_circle_radius*3+padding_top_main+height_available)*0.93 +")")
                   .append("foreignObject")
                   .style("overflow", "visible")
                   .attr("width",button_width_main*0.25)
                   .attr("height",button_height_main*1.5)
                   .append("xhtml:div")
                   .style("text-align","left")
                   .attr("height",button_height_main*1.5)
                   .append("g")
                   .append("xhtml:button")
                   .attr("id","scroll_zoom")
                   .attr("class","btn btn-outline-dark btn-xsss modebutton")
                   .html("<i class='fas fa-arrow-down' id='arrow'></i>")
                   .style("opacity", function() {
                    if(bar_count>(bar_per_view)) {
                      return("1")
                    } else {
                      return("0")
                    }
                  })
                  .style("cursor", function() {
                    if(bar_count>(bar_per_view)) {
                      return("default")
                    } else {
                      return("default")
                    }
                  })

                svg_map.append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (x.range()[1])*0.9+ "," + (0.05*height_vis_main+legend_circle_radius*3+padding_top_main+height_available)*0.93 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*0.25)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_main*1.5)
                  .append("g")
                  .append("xhtml:button")
                  .attr("id","scroll_zoom_up")
                  .attr("class","btn btn-outline-dark btn-xsss modebutton")
                  .html("<i class='fas fa-arrow-up' id='arrow'></i>")
                  .style("opacity", function() {
                    if(invisible>0) {
                      return("1")
                    } else {
                      return("0")
                    }
                  })
                  .style("cursor", function() {
                    if(invisible>0) {
                      return("default")
                    } else {
                      return("default")
                    }
                  })

         }

         
          /// DrawGraphHelper
          // ----------------------------------------
          function DrawGraphHelper(svg_id) {
          
            if (svg_id==-1) {

              graphsetup = InitializeGraph()
              
              // Generate header
              d3.selectAll("#svg_graph_"+(svg_id+1))
                .append("g")
                .append("text")
                .attr("id", "RHStitle")
                .attr("transform", "translate(" + (0)+ ","+(padding_top_main/4)+")")
                .html("Infection projections for " + state_conv(county_data_crosswalk_current, state_choice,"name")+"<tspan id='download_icon' class='fa fa-sm'>&nbsp;&nbsp;&nbsp;\uf381</tspan>")
                .style("text-anchor", "start")
                .style("dominant-baseline", "central")

              d3.selectAll("#download_icon")
              .style("dominant-baseline", "central")
                .on("mouseover", function() {

                 d3.selectAll("#div_stat_0").html("Click to download the infection projection data for " + state_conv(county_data_crosswalk_current, state_choice,"name"))           

                })
                .on("mouseout", function() {

                if (windowWidth>mobileWidth) {
                  d3.selectAll("#div_stat_0").html("Move your cursor over the graph to see details or hover over the info icon to learn more")           
                } else {
                  d3.selectAll("#div_stat_0").html("Tap on and move over the graph to see details or click on the info icon to learn more")           
                } 

                })


              d3.selectAll("#svg_graph_"+(svg_id+1))
                .append("g")
                .append("text")
                .attr("id", "svg_graph_small")
                .attr("transform", "translate(" + (0)+ ","+(padding_top_main/4)*2+")")
                .text("Cumulative predicted cases through to " + timeForm(graphsetup.maxDate) +" under different social mobility scenarios")                
                .attr("class", "svg_graph_main_label")
                .style("text-anchor", "start")
                .style("dominant-baseline", "central")            

              // Generate button
              d3.selectAll("#svg_graph_"+(svg_id+1))
                .append("g")
                .style("overflow", "visible")
                .attr("transform", "translate(" + (0)+ "," + (padding_top_main/4)*2.5 +")")
                .append("foreignObject")
                .style("overflow", "visible")
                .attr("width",button_width_main*1.9)
                .attr("height",button_height_main*1.5)
                .append("xhtml:div")
                .style("text-align","left")
                .attr("height",button_height_main*1.5)
                .on("click", function() {
                  if (data_column_current=="cumulative") {
                    UpdateVisualization(data_column="daily", relax_date=relax_date_current, relax_speed=relax_speed_current, agg_county=-1)
                    d3.selectAll("#selector_2").html("View cumulative cases")
                    d3.selectAll(".svg_graph_main_label").text("Daily predicted cases through to " + timeForm(graphsetup.maxDate) +" under different social mobility scenarios")

                  } else {
                    UpdateVisualization(data_column="cumulative", relax_date=relax_date_current, relax_speed=relax_speed_current, agg_county=-1)
                    d3.selectAll("#selector_2").html("View daily cases")
                    d3.selectAll(".svg_graph_main_label").text("Cumulative predicted cases through to " + timeForm(graphsetup.maxDate) +" under different social mobility scenarios")

                  }
                })
                .append("g")
                .append("xhtml:button")
                .attr("id","selector_"+(2))
                .attr("class","btn btn-outline-dark btn-xs modebutton")
                .html("View daily cases")

              // Generate button
              //d3.selectAll("#svg_graph_"+(svg_id+1))
               // .append("g")
               // .style("overflow", "visible")
               // .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
               // .append("foreignObject")
               // .style("overflow", "visible")
               // .attr("width",button_width_main*1.9)
               // .attr("height",button_height_main*1.5)
               // .append("xhtml:div")
               // .style("text-align","left")
               // .attr("height",button_height_main*1.5)
               // .on("click", function() {
               //   if (actual_hide==true) {
               //     d3.selectAll(".ghost-line-actual").attr("opacity",1)
               //     d3.selectAll("#selector_51").html("Hide actual cases")
               //     actual_hide=false
               //   } else {
                //    d3.selectAll(".ghost-line-actual").attr("opacity",0)
                //    d3.selectAll("#selector_51").html("View actual cases")
                //    actual_hide=true
                //  }
                //})
                //.append("g")
               // .append("xhtml:button")
               // .attr("id","selector_"+(51))
               // .attr("class","btn btn-outline-dark btn-xs modebutton")
               // .html("Hide actual cases")

            // Generate button
              d3.selectAll("#svg_graph_"+(svg_id+1))
                .append("g")
                .style("overflow", "visible")
                .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
                .append("foreignObject")
                .attr("id","button_mode_compare_div")
                .style("opacity",0.3)
                .style("overflow", "visible")
                .attr("width",button_width_main*1.9)
                .attr("height",button_height_main*1.5)
                .append("xhtml:div")
                .style("text-align","left")
                .attr("height",button_height_main*1.5)
                .on("click", function() {

                })
                .append("g")
                .style("cursor","default")
                .append("xhtml:button")
                .attr("id","button_mode_compare")
                .attr("class","btn btn-outline-dark btn-xs modebutton")
                .html("Compare across counties")
                .style("cursor","default")
                

                /*d3.selectAll("#svg_graph_"+(svg_id+1))
                  .append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (button_width_main*2)+ "," + (padding_top_main/4)*2.5 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*1.9)
                  .attr("height",button_height_main*1.5)
                  .append("xhtml:div")
                  .attr("height",button_height_main*1.5)
                  .style("text-align","left")
                  .on("click", function() {
                      
  
                  })
                  .append("g")
                  .append("xhtml:button")
                  .attr("id","button_mode_compare")
                  .attr("class","btn btn-outline-dark btn-xs modebutton")
                  .html("Compare across counties")
                  .style("opacity",0.3)
                  .style("position","initial")
                  .style("cursor","default")*/



              // Generate statistics box
              d3.selectAll("#svg_graph_"+(svg_id+1))
                  .append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (0)+ "," + (padding_top_main/4)*3.7 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_main*6)
                  .attr("height",(button_height_main*1.5)/3)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",(button_height_main*1.5)/3)
                  .append("g")
                  .append("xhtml:button")
                  .attr("id","div_stat_"+(svg_id+1))
                  .attr("class","btn btn-outline-dark btn-xss modebutton")
                  .html("")
                  .style("cursor","default")

            // Generate links
            
            // Link 1
            text_content = "<div id='scenario_span'>Scenario</div>"
            text_node    = d3.selectAll(".leftrect_main_-1")
              .append("g")
              .on("mouseover", function() {
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0.1")
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "0.1")
                d3.select(this).style("cursor", "pointer")
                d3.selectAll("#textrect0").selectAll("#scenario_span").text("| Scenario")
              })
              .on("mouseout", function() {

                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {
                  if (actual_hide==true) {
                    return(0)
                  } else {
                    return(1)
                  }})
                
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")
                d3.select(this).style("cursor", "")
                d3.selectAll("#textrect0").selectAll("#scenario_span").text("Scenario")
              })       
              .attr("id","textrect0")
              .attr("class","textwrapdivlegend sliding-link-scenario_"+svg_id)
              .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.9) +")")
              .attr("opacity", "0")
              .attr("color", '#0059bb')
              .append('text')

            text_node.text(text_content)
            bounds  = d3.selectAll(".leftrect_main_-1").select('#leftrect_-1').node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            // Link 2
            text_content = "<div id='baseline_span'>Baseline</div>"
            text_node    = d3.selectAll(".leftrect_main_-1")
              .append("g")
              .on("mouseover", function() {
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0.1")
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "0.1")
                d3.select(this).style("cursor", "pointer")
                d3.selectAll("#textrect1").selectAll("#baseline_span").text("| Baseline")
               
              })
              .on("mouseout", function() {
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {
                  if (actual_hide==true) {
                    return(0)
                  } else {
                    return(1)
                  }})                        
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")
                d3.select(this).style("cursor", "")
                d3.selectAll("#textrect1").selectAll("#baseline_span").text("Baseline")
              })                      

              .attr("id","textrect1")
              .attr("class","textwrapdivlegend sliding-link-predicted_"+svg_id)
              .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.85)+")")
              .attr("opacity", "0")
              .attr("color", '#bdbdbd')
              .append('text')

            text_node.text(text_content)
            bounds  = d3.selectAll(".leftrect_main_-1").select('#leftrect_-1').node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            // Link 3
            text_content = "<div id='actual_span'>Actual</div>"
            text_node    = d3.selectAll(".leftrect_main_-1")
              .append("g")
              .on("mouseover", function() {
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "1")
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "0.1")
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "0.1")
                d3.select(this).style("cursor", "pointer")
                d3.selectAll("#textrect2").selectAll("#actual_span").text("| Actual")
              })
              .on("mouseout", function() {
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")
                d3.select(this).style("cursor", "")
                d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {
                  if (actual_hide==true) {
                    return(0)
                  } else {
                    return(1)
                  }})
                d3.selectAll("#textrect2").selectAll("#actual_span").text("Actual")
              }) 
              .attr("id","textrect2")
              .attr("class","textwrapdivlegend sliding-link-actual_"+svg_id)
              .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.8) +")")
              .attr("opacity", "0")
              .attr("color", '#525252')
              .append('text')

            
              text_node.text(text_content)
              bounds  = d3.selectAll(".leftrect_main_-1").select('#leftrect_-1').node()
              padding = 10
              wrap    = d3.textwrap()
              wrap.bounds(bounds).padding(padding)
              text_node.call(wrap) 

              d3.selectAll(".textwrapdivlegend").select("#textwrapdiv")
                .attr("width", xScale_main(timeConv(today))*0.3)
                .attr("height", button_height_main)

              // Info box
              d3.selectAll("#svg_graph_"+(svg_id+1))
                .append("g")
                .style("overflow", "visible")
                .attr("transform", "translate(" + (((xScale_main(graphsetup.minDate))+2*(xScale_main(timeConv(relax_date_default_plus))-
                  xScale_main(timeConv(relax_date_default_minus)))))+ "," + ((yScale_main(graphsetup.maxValue_main))*1.05)+")")
                .append("foreignObject")
                .style("overflow", "visible")
                .attr("width",button_width_main*0.5)
                .attr("height",button_height_main)
                .append("xhtml:div")
                .style("text-align","left")
                .attr("height",button_height_main)
                .append("g")
                .append("xhtml:button")
                .attr("id","selector_"+(4))
                .attr("class","btn btn-xs-info modebutton")
                .html("<i class='fas fa-info-circle fa-lg'></i>")
                .on("mouseover", function() {

                  info_close=false

                  if (d3.selectAll("#textrect").size()==0) {

                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")

                    d3.selectAll("#textrect0").attr("opacity",0.7)
                    d3.selectAll("#textrect1").attr("opacity",0.7)
                    d3.selectAll("#textrect2").attr("opacity",0.7)
 
                        if (windowWidth>mobileWidth) {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Hover over the description of the three lines to learn more")           
                        } else {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Click on the description of the three lines to learn more")           
                        }
                    current_stay_at_home_current     = stay_home_calc("current","ZZAggregate",graphsetup.maxDate)
                    one_month_stay_at_home_current   = stay_home_calc("one_month","ZZAggregate",graphsetup.maxDate)
                    latest_date_stay_at_home_current = stay_home_calc("latest_date","ZZAggregate", graphsetup.maxDate)
                    
                    if (relax_speed_current==1) {
                      one_month_rate = "100"
                    } else if (relax_speed_current==15) {
                      one_month_rate = "75"
                    } else if (relax_speed_current==30) {
                      one_month_rate = "50"
                    } else if (relax_speed_current==60) {
                      one_month_rate = "25"
                    } 
                    //else if (relax_speed_current==90) {
                     // one_month_rate = "17"
                   // } else if (relax_speed_current==120) {
                   //   one_month_rate = "13"
                   // }

                  if(county_mode==false) {
                    location_selected=state_conv(county_data_crosswalk_current, state_choice,"name")
                  } else {
                    location_selected=county_selected + ", " + state_choice       
                  }


                    text_content = "<div id='scenario_span'><u><b><font color='#0059bb' style='opacity:0'>'Scenario' line:</font></b></u></div>Infection projections for " + location_selected + 
                                   " if social mobility levels start to increase towards normal (pre-Covid) levels on " + timeForm(relax_date_current)+", and the speed of increase is such that mobility levels are " + 
                                   one_month_rate+"% of the way from today's levels to normal levels by " + timeForm(one_month_stay_at_home_current[0]) + "."

                    text_node    = d3.selectAll(".leftrect_main_-1")
                      .append("g")
                      .on("mouseover", function() {
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0.1")
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "0.1")
                        d3.select(this).style("cursor", "pointer")
                        d3.selectAll(".btn-xss-mini").style("color", "#02124c")
 
                        if (windowWidth>mobileWidth) {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Hover over the description of the three lines to learn more")           
                        } else {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Click on the description of the three lines to learn more")           
                        }

                      })
                      .on("mouseout", function() {


                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {
                          if (actual_hide==true) {
                            return(0)
                          } else {
                            return(1)
                          }})
                        
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")
                        d3.select(this).style("cursor", "")
                      })       
                      .attr("id","textrect")
                      .attr("class", "textwrapdivinfo")
                      .attr("transform", "translate(" + (0)+ "," + -387 +")")
                      .attr("opacity", "0")
                      .append('text')

                    text_node.text(text_content)
                    bounds  = d3.selectAll(".leftrect_main_-1").select('#leftrect_-1').node()
                    padding = 10
                    wrap    = d3.textwrap()
                    wrap.bounds(bounds).padding(padding)
                    text_node.call(wrap) 

                    text_content = "<div id='baseline_span'><u><b><font color='#bdbdbd' style='opacity:0'>'Baseline' line:</font></b></u></div>Infection projections for " + location_selected + " if social mobility remains at current levels."

                    text_node    = d3.selectAll(".leftrect_main_-1")
                      .append("g")
                      .on("mouseover", function() {
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0.1")
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "0.1")
                        d3.select(this).style("cursor", "pointer")
                        d3.selectAll(".btn-xss-mini").style("color", "#02124c")
 
                        if (windowWidth>mobileWidth) {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Hover over the description of the three lines to learn more")           
                        } else {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Click on the description of the three lines to learn more")           
                        }
                      })
                      .on("mouseout", function() {
                          d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {
                          if (actual_hide==true) {
                            return(0)
                          } else {
                            return(1)
                          }})                        
                          d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")
                        d3.select(this).style("cursor", "")
                      })                      

                      .attr("id","textrect")
                      .attr("class", "textwrapdivinfo")
                      .attr("transform", "translate(" + (0)+ "," + -180+")")
                      .attr("opacity", "0")
                      .append('text')
                       
                    text_node.text(text_content)
                    bounds  = d3.selectAll(".leftrect_main_-1").select('#leftrect_-1').node()
                    padding = 10
                    wrap    = d3.textwrap()
                    wrap.bounds(bounds).padding(padding)
                    text_node.call(wrap) 

                    text_content = "<div id='actual_span'><u><b><font color='#525252' style='opacity:0'>'Actual' line:</font></b></u></div> Actual cases."

                    text_node    = d3.selectAll(".leftrect_main_-1")
                      .append("g")
                           .on("mouseover", function() {
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "1")
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "0.1")
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "0.1")
                        d3.select(this).style("cursor", "pointer")
                        d3.selectAll(".btn-xss-mini").style("color", "#02124c")
 
                        if (windowWidth>mobileWidth) {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Hover over the description of the three lines to learn more")           
                        } else {
                          d3.selectAll("#div_stat_"+(svg_id+1)).html("Click on the description of the three lines to learn more")           
                        }
                      })
                      .on("mouseout", function() {
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")
                        d3.select(this).style("cursor", "")
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {
                          if (actual_hide==true) {
                            return(0)
                          } else {
                            return(1)
                          }})
                      }) 
                      .attr("id","textrect")
                      .attr("class", "textwrapdivinfo")
                      .attr("transform", "translate(" + (0)+ "," + -90 +")")
                      .attr("opacity", "0")
                      .append('text')

                    

                    text_node.text(text_content)
                    bounds  = d3.selectAll(".leftrect_main_-1").select('#leftrect_-1').node()
                    padding = 10
                    wrap    = d3.textwrap()
                    wrap.bounds(bounds).padding(padding)
                    text_node.call(wrap) 


                    d3.selectAll(".textwrapdivinfo").select("#textwrapdiv")
                      .attr("width", xScale_main(timeConv(today))*0.95)
                      .attr("height", button_height_main*6)

                    d3.selectAll("#textrect0")
                      .transition()
                      .duration(1000)
                      .attr("transform", "translate(" + (0)+ "," + -387 +")")

                    d3.selectAll("#textrect1")
                      .transition()
                      .duration(1000)
                      .attr("transform", "translate(" + (0)+ "," + -180 +")")

                    d3.selectAll("#textrect2")
                      .transition()
                      .duration(1000)
                      .attr("transform", "translate(" + (0)+ "," + -90 +")")
                      .on("end", function() {
                    
                    d3.selectAll("#textrect")
                          .attr("opacity", ("1"))

                      })
                  }

                })
                .on("mouseout", function() {

                  d3.selectAll(".btn-xss-mini")
                  .transition()
                    .delay(7000)
                    .duration(50)
                    .style("color", "white")

                  d3.selectAll("#textrect")
                    .transition()
                    .delay(7000)
                    .duration(2000)
                    .attr("opacity", "0")
                    .on("end", function() {
                      d3.selectAll("#textrect").remove()
                     
                      d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")
                      d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")
                       if(actual_hide==true) {
                        d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0")
                      } else {
                         d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "1")
                       
                      }
                      d3.select(this).style("cursor", "")
                      d3.selectAll("#textrect0")
                      .transition()
                      .duration(1000)
                      .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.9) +")")

                    d3.selectAll("#textrect1")
                      .transition()
                      .duration(1000)
                      .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.85)+")")

                    d3.selectAll("#textrect2")
                      .transition()
                      .duration(1000)
                      .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.8)+")")

                    info_close=true
                    });
                      
                })
                .on("click", function(e) {
                 // d3.selectAll("#textrect").remove()
                })

                // Initialize statistics box
                if (windowWidth>mobileWidth) {
                  d3.selectAll("#div_stat_"+(svg_id+1)).html("Move your cursor over the graph to see details or hover over the info icon to learn more")           
                } else {
                  d3.selectAll("#div_stat_"+(svg_id+1)).html("Tap on and move over the graph to see details or click on the info icon to learn more")           
                } 
            } else {

              // Generate header
              d3.selectAll("#svg_graph_"+(svg_id+1))
                .append("g")
                .append("text")
                .attr("transform", "translate(" + (0)+ ","+(padding_top_mini/4)+")")
                .html("<tspan>"+county_data_current[svg_id].key + ", " + state_choice+"</tspan>  <tspan  id='zoomlink' class='zoomlink_"+(svg_id)+"'>"+"Enlarge" +"</tspan>")
                .style("text-anchor", "start")
                .style("dominant-baseline", "central")
                .attr("class", function(d) {
                  if (d.key=="") {
                    return("serie_labelactive serie_label")
                  } else {
                    return("serie_label")
                  }
                })
                .attr("id", "div_name_"+(svg_id+1))

              d3.selectAll(".zoomlink_"+svg_id)
                .attr("opacity",0)
                .style("cursor","default")
                .on("click", function() {


                })

               d3.selectAll("#svg_graph_"+(svg_id+1))
                  .on("touchmove", function() {
                    d3.selectAll("#zoomlink").attr("opacity","0")
                    d3.selectAll(".zoomlink_"+svg_id).attr("opacity","0.7")
                  })
                  .on("mouseover", function() {
                    d3.selectAll(".zoomlink_"+svg_id).attr("opacity","0.7")
                    d3.selectAll(".zoomlink_"+svg_id).style("cursor","pointer")
                    d3.selectAll(".zoomlink_"+svg_id)
                      .on("click", function() {

                        county_index = counties( county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county_data_current[svg_id].key)
                        UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=relax_speed_current, agg_county=county_index)

                        if (windowWidth>mobileWidth) {

                          $('html,body').animate({scrollTop: 0},'slow'); 

                        } else {
                          target="#svg_graph_0"
                          $('html,body').animate({scrollTop: $(target).offset().top},'slow');
                        }
                      })

                  })
                  .on("mouseout", function() {
                    d3.selectAll(".zoomlink_"+svg_id).attr("opacity","0")
                    d3.selectAll(".zoomlink_"+svg_id).style("cursor","default")
                    d3.selectAll(".zoomlink_"+svg_id)
                      .on("click", function() {
                      })
                  })
              
  

              d3.selectAll("#svg_graph_"+(svg_id+1))
                  .append("g")
                  .style("overflow", "visible")
                  .attr("transform", "translate(" + (0)+ "," + (padding_top_mini/4)*2 +")")
                  .append("foreignObject")
                  .style("overflow", "visible")
                  .attr("width",button_width_mini*6)
                  .attr("height",button_height_mini)
                  .append("xhtml:div")
                  .style("text-align","left")
                  .attr("height",button_height_mini)
                  .on("click", function() {
                  })
                  .append("g")
                  .append("xhtml:button")
                  .attr("id","div_stat_"+(svg_id+1))
                  .attr("class","btn btn-outline-dark btn-xss-mini modebutton")
                  .html("")
                  .style("cursor","default")


            
              // Initialize statistics box
              if (windowWidth>mobileWidth) {
                 d3.selectAll("#div_stat_"+(svg_id+1)).html("Move over the graph to see details")           
              } else {
                 d3.selectAll("#div_stat_"+(svg_id+1)).html("Tap on and move over the graph to see details")           
              } 

 

            }

          }
          
          /// DrawGraph
          // ----------------------------------------
          function DrawGraph(svg_id) {
          
            /// Initialize
            // ----------------------------------------
  
            // Initialize axes   
            graphsetup = InitializeGraph()

            // Helpers
            if (svg_id==-1) {
          
              // Misc var
              height_focus_temp = padding_top_main*1+ padding_bottom_main
              height_temp       = height_vis_main
              legend_circle_radius_temp = legend_circle_radius

              // Scales
              xScale_main.domain([graphsetup.minDate,graphsetup.maxDate_plus_one])
              yScale_main.domain([graphsetup.minValue,graphsetup.maxValue_main])
              
              xScale_temp = xScale_main
              yScale_temp = yScale_main
             
              // Axes
              xaxis = d3.axisBottom()
                        .ticks(d3.timeMonth.every(2))
                        .tickFormat(d3.timeFormat("%b %d"))
                        .scale(xScale_main)
              
              yaxis = d3.axisLeft()
                        .scale(yScale_main)
                        .ticks(10)

              // Brush label
              brush_label = d3.selectAll("#svg_graph_"+(svg_id+1))
                              .append("g")
                              .append("text")
                              .attr("id", "brush_label_"+svg_id)
                              .attr("transform", "rotate(270)")
                              .attr("x", function() {
                                if (windowWidth>mobileWidth) {
                                  return((yScale_temp(0)* -0.97))
                                } else{
                                  return((yScale_temp(graphsetup.maxValue_main)* -1.6))
                                }
                              })

                              .attr("y", (xScale_temp(timeConv(relax_date_default)))-1.5*(xScale_temp(timeConv(relax_date_default_plus))-
                                xScale_temp(timeConv(relax_date_default_minus))))
                              .text("Relax date")
                              .style("text-anchor", "start")

            } else {
          
              // Misc var
              height_focus_temp = padding_top_mini*1  + padding_bottom_mini
              height_temp       = height_vis_mini
              legend_circle_radius_temp = legend_circle_radius_mini

              // Scales
              xScale_mini.domain([graphsetup.minDate,graphsetup.maxDate_plus_one])
              yScale_mini_array[svg_id].domain([graphsetup.minValue,graphsetup.maxValue_mini[svg_id]])

              xScale_temp = xScale_mini
              yScale_temp = yScale_mini_array[svg_id]

              // Axes
              xaxis = d3.axisBottom()
                        .ticks(d3.timeMonth.every(2))
                        .tickFormat(d3.timeFormat("%b %d"))
                        .scale(xScale_mini)
              
              yaxis = d3.axisLeft()
                        .scale(yScale_temp)
                        .ticks(5)

            }
           

            /// Draw
            // ----------------------------------------
  
            // Initialize lines   
            line = d3.line()
                     .x(function(d) { return xScale_temp(d.date) })
                     .y(function(d) { return yScale_temp(d[data_column_current])})

            line_baseline = d3.line()
                            .x(function(d) { return xScale_temp(d.date) })
                            .y(function(d) { return yScale_temp(d["baseline_"+data_column_current])})

            line_actual = d3.line()
                            .x(function(d) { return xScale_temp(d.date)})
                            .y(function(d) {return yScale_temp(d["actual_"+data_column_current])})
           
          
            // Draw axes    
            d3.selectAll("#svg_graph_"+(svg_id+1))
              .append("g")
              .attr("class", "x axis")
              .attr("transform", function() {
                  if (svg_id==-1) {
                    return("translate(" + 0 + "," + (height_temp*0.9-legend_circle_radius_temp*3)  + ")")
                  } else {
                    return("translate(" + 0 + "," + (height_temp*0.9-legend_circle_radius_temp*3)  + ")")
                  }
                })
              .call(xaxis)
                
            yaxis_graph = d3.selectAll("#svg_graph_"+(svg_id+1))
              .append("g")
              .attr("class", "y axis")
              .attr("transform", function() {
                  if (svg_id==-1) {
                    return("translate(" + xScale_main.range()[1]*1.03 + ",0)")
                  } else {
                    return("translate(" + xScale_mini.range()[1]*1.06 + ",0)")
                  }
              })
              .call(yaxis)


              yaxis_graph
              .selectAll('text')
              .attr("text-anchor","start")

              yaxis_graph
              .selectAll('path')
              .style("opacity",0)

              yaxis_graph
              .selectAll('line')
              .style("opacity",0)

            // Draw main objects - layer: svg_graph_layer_2
            svg_graph_layer_1 =  d3.selectAll("#svg_graph_"+(svg_id+1))
                                   .append("g")
                                   .attr("transform", function() {
                                    if (svg_id==-1) {
                                        return("translate(" + 0 + ",0)")
                                      } else {
                                        return("translate(" + 0 + ",0)")
                                      }
                                  })

            svg_graph_layer_2 =  d3.selectAll("#svg_graph_"+(svg_id+1))
                                   .append("g")
                                   .attr("class","leftrect_main_"+svg_id)
                                   .attr("transform", function() {
                                   if (svg_id==-1) {
                                       return("translate(" + 0 + ","+(height_temp*0.9-legend_circle_radius_temp*3) +")")
                                     } else {
                                       return("translate(" + 0 + ","+(height_temp*0.9-legend_circle_radius_temp*3) +")")
                                     }
                                   })


            svg_graph_layer_2.append("rect")
                             .style("opacity","0.1")
                             .attr("id","day_rectangle_"+svg_id)
                             .attr("height", (yScale_temp.range()[0]-yScale_temp.range()[1]))
                             .attr("transform", "translate(" +  0+ ","+((yScale_temp.range()[0]-yScale_temp.range()[1])*-1)+")")
                             .attr("width", xScale_temp(timeConv(today)))
                             .attr("fill", "lightgrey")



            svg_graph_layer_2.append("rect")
                             .style("opacity","0.1")
                             .attr("id","day_2_rectangle_"+svg_id)
                             .attr("height", (yScale_temp.range()[0]-yScale_temp.range()[1]))
                             .attr("transform", "translate(" + (xScale_temp(timeConv(graphsetup.latest_prediction_date)))+ ","+((yScale_temp.range()[0]-yScale_temp.range()[1])*-1)+")")
                             .attr("width", xScale_temp.range()[1]-xScale_temp(timeConv(graphsetup.latest_prediction_date)))
                             .attr("fill", "lightgrey")

            if (svg_id==-1) {


               
              if (windowWidth > mobileWidth) {


              menu_main = d3.selectAll("#svg_graph_"+(svg_id+1))
                    .append("g")
                    .style("overflow", "visible")
                    .attr("transform", "translate(" + (xScale_main(graphsetup.maxDate)*0.98- button_width_main*1.9)+ "," + ((yScale_main(graphsetup.maxValue_main))*1.05) +")")
                    .append("foreignObject")
                    .style("overflow", "visible")
                    .attr("width",button_width_main*1.9)
                    .attr("height",button_height_main*0.5)
                    .append("xhtml:div")
                    .style("text-align","left")
                    .attr("height",button_height_main*0.5)
                    .attr("class", "dropdown")

               menu_main.append("xhtml:button")
                    .attr("id","button_mode_"+(10))
                    .attr("class","btn btn-outline-dark btn-xs modebutton dropdown-toggle")
                    .attr("data-toggle", "dropdown")
                    .html("Speed of mobility change")
                    .on("mouseover", function() {

                      d3.selectAll("#div_stat_"+(svg_id+1)).html("Select the speed at which social mobility increases")           
                      d3.selectAll(".btn-xss-mini").style("color", "#02124c")           


                    })
                    .on("mouseout", function() {
                      d3.selectAll(".btn-xss-mini").style("color", "white")           
                     
                    })
              

                menu_main.append("xhtml:div")
                .append("xhtml:ul")
                    .attr("class","dropdown-menu dropdown-menu-101")
                    .attr("aria-labelledby","dropdownMenuLink")
                    .on("mouseover", function() {

                      d3.selectAll("#div_stat_"+(svg_id+1)).html("Select the speed at which social mobility increases")           
                      d3.selectAll(".btn-xss-mini").style("color", "#02124c")           


                    })
                    .on("mouseout", function() {
                      d3.selectAll(".btn-xss-mini").style("color", "white")           
                     
                    })
                menu              = document.querySelector('.dropdown-menu-101');
                //menu.innerHTML += "<h4 class='dropdown-header'>"+"% of March mobility (relax date +30 days)"+"</h4>";
                
                speed_description = ['Immediate',
                   // 'Rapid', 
                    'Fast', 
                    'Moderate',
                    //'Gradual',
                    'Slow'];
                    speeds = [
                    'Immediate', 
                    //'Rapid', 
                    'Fast',
                    'Moderate',
                   // 'Gradual',
                    'Slow']
                    
                for (var i = 0; i < speeds.length; i++) {
                  menu.innerHTML += "<a id='" + speeds[i] + "' class='dropdown-item dropdown-item-1' href='#'>"+speed_description[i]+"</a>";
                }

                $('#Immediate').click(function(e) {
                   //d3.selectAll("#Fast").attr("class", "dropdown-item")
                  UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=1, agg_county=-1)
                  d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                  d3.selectAll("#Immediate").attr("class", "dropdown-item active dropdown-item-1")
                  e.preventDefault();
                });   
               //$('#Rapid').click(function(e) {
                //   d3.selectAll("#Fast").attr("class", "dropdown-item")
                //  UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=15, agg_county=-1)
                //  d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                //  d3.selectAll("#Rapid").attr("class", "dropdown-item active dropdown-item-1")
                //  e.preventDefault();
               // });  
                $('#Fast').click(function(e) {
                   // d3.selectAll("#Fast").attr("class", "dropdown-item")
                  UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=15, agg_county=-1)
                  d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                  d3.selectAll("#Fast").attr("class", "dropdown-item active dropdown-item-1")
                  e.preventDefault();
                });
                $('#Moderate').click(function(e) {
                   // d3.selectAll("#Fast").attr("class", "dropdown-item")
                  UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=30, agg_county=-1)
                  d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                  d3.selectAll("#Moderate").attr("class", "dropdown-item active dropdown-item-1")
                  e.preventDefault();
                });
             //   $('#Gradual').click(function(e) {
              //    d3.selectAll("#Fast").attr("class", "dropdown-item")
              //    UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=90, agg_county=-1)
              //    d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
              //    d3.selectAll("#Gradual").attr("class", "dropdown-item active dropdown-item-1")
              //    e.preventDefault();
              //  });
                $('#Slow').click(function(e) {
                  //  d3.selectAll("#Fast").attr("class", "dropdown-item")
                  UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=60, agg_county=-1)
                  d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item  dropdown-item-1")
                  d3.selectAll("#Slow").attr("class", "dropdown-item active  dropdown-item-1")
                  e.preventDefault();
                });

               // Select default
                if (relax_speed_current==1) {
                    d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                    d3.selectAll("#Immediate").attr("class", "dropdown-item active dropdown-item-1")
                } else if (relax_speed_current==15) {
                    d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                    d3.selectAll("#Fast").attr("class", "dropdown-item active dropdown-item-1")
                } else if (relax_speed_current==30) {
                    d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                    d3.selectAll("#Moderate").attr("class", "dropdown-item active dropdown-item-1")
                } else if (relax_speed_current==60) {
                    d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                    d3.selectAll("#Slow").attr("class", "dropdown-item active dropdown-item-1")
                } 
                //else if (relax_speed_current==90) {
                 //   d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item")
                //    d3.selectAll("#Gradual").attr("class", "dropdown-item active")
               //} else if (relax_speed_current==120) {
                //    d3.selectAll(".dropdown-item").attr("class", "dropdown-item")
                //    d3.selectAll("#Slow").attr("class", "dropdown-item active")
                //}


            } else {

              menu_main = d3.selectAll("#svg_graph_"+(svg_id+1))
                    .append("g")
                    .style("overflow", "visible")
                    .attr("transform", "translate(" + (xScale_main(graphsetup.maxDate)*0.98- button_width_main*1.8)+ "," + ((yScale_main(graphsetup.maxValue_main))*3.85) +")")
                    .append("foreignObject")
                    .style("overflow", "visible")
                    .attr("width",button_width_main*1.9)
                    .attr("height",button_height_main*0.5)
                    .append("xhtml:div")
                    .style("text-align","left")
                    .attr("height",button_height_main*0.5)
                    .attr("class", "dropdown")

                menu_main.append("xhtml:select")
                    .attr("id","button_mode_"+(10))
                    .attr("class","dropdown dropdown-menu-1 btn btn-outline-dark btn-xs-mobile modebutton")
                    .html("Speed of mobility change")
                    .on("mouseover", function() {

                      d3.selectAll("#div_stat_"+(svg_id+1)).html("Select the speed at which social mobility increases")           
                      d3.selectAll(".btn-xss-mini").style("color", "#02124c")           

                    })
                    .on("mouseout", function() {
                      d3.selectAll(".btn-xss-mini").style("color", "white")           
                     
                    })
              
            
                menu              = document.querySelector('.dropdown-menu-1');
                
                speed_description = ['Immediate',
                   // 'Rapid', 
                    'Fast', 
                    'Moderate',
                   // 'Gradual',
                    'Slow'];
                speeds = ['Immediate', 
               // 'Rapid', 
                'Fast',
                'Moderate',
               // 'Gradual',
                'Slow']
                half_life = [1,
                15,
                30,
                60]
                //90,
                //120]
                
                menu.innerHTML += "<option selected disabled>Speed of mobility change</option>"
                for (var i = 0; i < speeds.length; i++) {
                  menu.innerHTML += "<option id='" + speeds[i] + "' class='dropdown-item dropdown-item-1' href='#'>"+speed_description[i]+"</option>";
                }

                $('#button_mode_10').on('change', function(e) {
                    
                    selector = "#"+speeds[speed_description.indexOf(this.value)]
                   // d3.selectAll("#Fast").attr("class", "dropdown-item")
                    UpdateVisualization(data_column=data_column_current, relax_date=relax_date_current, relax_speed=half_life[speed_description.indexOf(this.value)], agg_county=-1)
                    d3.selectAll(".dropdown-item-1").attr("class", "dropdown-item dropdown-item-1")
                    d3.selectAll(selector).attr("class", "dropdown-item active dropdown-item-1")
                    e.preventDefault();
               
                })

                // Select default
                if (init!=0) {
                if (relax_speed_current==1) {
                  $('#button_mode_10').val('Immediate')
                } else if (relax_speed_current==15) {
                  $('#button_mode_10').val('Fast')
                } else if (relax_speed_current==30) {
                  $('#button_mode_10').val('Moderate')
                } else if (relax_speed_current==60) {
                  $('#button_mode_10').val('Slow')
                } 

                //else if (relax_speed_current==90) {
               //   $('#button_mode_10').val('Gradual')
               // } else if (relax_speed_current==120) {
               //   $('#button_mode_10').val('Slow')
               // }
             }

              menu_main = d3.selectAll("#svg_graph_"+(svg_id+1))
                    .append("g")
                    .style("overflow", "visible")
                    .attr("transform", "translate(" + (xScale_main(graphsetup.maxDate)*0.98- button_width_main*1.8)+ "," + ((yScale_main(graphsetup.maxValue_main))*3.6) +")")
                    .append("foreignObject")
                    .style("overflow", "visible")
                    .attr("width",button_width_main*1.9)
                    .attr("height",button_height_main*0.5)
                    .append("xhtml:div")
                    .style("text-align","left")
                    .attr("height",button_height_main*0.5)
                    .attr("class", "dropdown")

                menu_main.append("xhtml:select")
                    .attr("id","button_mode_"+(106))
                    .attr("class","dropdown dropdown-menu-10 btn btn-outline-dark btn-xs-mobile modebutton")
                    .html("Relax date")
                    .on("mouseover", function() {

                      d3.selectAll("#div_stat_"+(svg_id+1)).html("Select the date when restrictions being being lifted")           
                      d3.selectAll(".btn-xss-mini").style("color", "#02124c")           

                    })
                    .on("mouseout", function() {
                      d3.selectAll(".btn-xss-mini").style("color", "white")           
                     
                    })
              
            
                menu              = document.querySelector('.dropdown-menu-10');
                
                date_list=[]
                county_data_current[0].values.forEach(function(d) {
                  if (xScale_main(d.date)>=xScale_main(timeConv(today)) & xScale_main(d.date)<=xScale_main(timeConv(graphsetup.latest_prediction_date)))
                  date_list.push(timeForm(d.date))
                })

                menu.innerHTML += "<option selected disabled>Relax date</option>"
                for (var i = 0; i < date_list.length; i++) {
                  menu.innerHTML += "<option id=dateoption'" + i + "' class='dropdown-item dropdown-item-1' href='#'>"+date_list[i]+"</option>";
                }

                $('#button_mode_106').on('change', function(e) {
                    
                    date     = timeConv_org(this.value)
                    UpdateVisualization(data_column=data_column_current, relax_date=date, relax_speed=relax_speed_current, agg_county=-1)


               
                })

                // Select default
                if (init!=0) {
                $('#button_mode_106').val(timeForm(relax_date_current))
                }
            }

                            

            }

            focus = svg_graph_layer_2.append("g")
                                     .append("line")
                                     .attr("class", function() {
                                        if(svg_id==-1) {
                                          return("focus focus_main")
                                        } else {
                                          return("focus focus_mini")
                                        }
                                     })
                                     .attr("id","focus_"+svg_id)
                                     .attr("stroke", "black")
                                     .attr("y1", 0)
                                     .attr("y2", ((yScale_temp.range()[0]-yScale_temp.range()[1])*-1))
                                     .attr("x1", xScale_temp(timeConv(today)))
                                     .attr("x2", xScale_temp(timeConv(today)))
                                     .style("stroke-dasharray", "3,3")

            svg_graph_layer_2.append("rect")
                             .attr("class",function() {
                                if(svg_id==-1) {
                                  return("leftrect leftrect_main")
                                } else {
                                  return("leftrect leftrect_mini")
                                }
                              })
                             .attr("id","leftrect_"+svg_id)
                             .attr("height", ((yScale_temp.range()[0]-yScale_temp.range()[1])))
                             .attr("transform", "translate(" +  0+ ","+((yScale_temp.range()[0]-yScale_temp.range()[1])*-1)+")")
                             .attr("width", xScale_temp.range()[1])
                             .on("mouseover", function() {

                              d3.selectAll(".sliding-link-predicted_"+svg_id).attr("opacity","0.7")
                              d3.selectAll(".sliding-link-actual_"+svg_id).attr("opacity","0.7")
                              d3.selectAll(".sliding-link-scenario_"+svg_id).attr("opacity","0.7")
                              

                             })
                             .on("mouseout", function() {

                              if (svg_id==-1) {


                                if ((d3.mouse(this)[0]<xScale_main.range()[0]) | (d3.mouse(this)[0]>xScale_main.range()[1])  | 
                                 (d3.mouse(this)[1]<0) | (d3.mouse(this)[1]>(yScale_main.range()[0]-yScale_main.range()[1]))) {

                                if (info_close==true) {
                                  d3.selectAll(".sliding-link-predicted_"+svg_id).attr("opacity","0")
                                  d3.selectAll(".sliding-link-actual_"+svg_id).attr("opacity","0")
                                  d3.selectAll(".sliding-link-scenario_"+svg_id).attr("opacity","0")
                                  
                                }

                                if (drag==false) {
                                if (windowWidth>mobileWidth) {
                                  d3.selectAll(".btn-xss-mini").html("Move over the graph to see details")           
                                } else {
                                  d3.selectAll(".btn-xss-mini").html("Tap on and move over the graph to see details")           
                                } 
                                if (windowWidth>mobileWidth) {
                                  d3.selectAll(".btn-xss").html("Move your cursor over the graph to see details or hover over the info icon to learn more")           
                                } else {
                                  d3.selectAll(".btn-xss").html("Tap on and move over the graph to see details or click on the info icon to learn more")           
                                } 


                              }}} else {
                                if ((d3.mouse(this)[0]<xScale_temp.range()[0]) | (d3.mouse(this)[0]>xScale_temp.range()[1])  | 
                                 (d3.mouse(this)[1]<0) | (d3.mouse(this)[1]>(yScale_temp.range()[0]-yScale_temp.range()[1]))) {
               
                                if (info_close==true) {
                                  d3.selectAll(".sliding-link-predicted_"+svg_id).attr("opacity","0")
                                  d3.selectAll(".sliding-link-actual_"+svg_id).attr("opacity","0")
                                  d3.selectAll(".sliding-link-scenario_"+svg_id).attr("opacity","0")
                  
                                }

                                if (drag==false) {
                                if (windowWidth>mobileWidth) {
                                  d3.selectAll(".btn-xss-mini").html("Move over the graph to see details")           
                                } else {
                                  d3.selectAll(".btn-xss-mini").html("Tap on and move over the graph to see details")           
                                } 

                                if (windowWidth>mobileWidth) {
                                  d3.selectAll(".btn-xss").html("Move your cursor over the graph to see details or hover over the info icon to learn more")           
                                } else {
                                  d3.selectAll(".btn-xss").html("Tap on and move over the graph to see details or click on the info icon to learn more")           
                                }                               
                              }}


                              }




                             })
                             .on("mousemove", function () {

                                if (drag==false) {
                                  
                                  d3.selectAll(".btn-xss-mini").style("color", "white")
                                  
                                  if (svg_id==-1) {
                                    x0           = xScale_main.invert(d3.mouse(this)[0])
                                    index        = bisect(county_data_current[county_data_current.length-1].values, x0, 1)
                                    selectedData = county_data_current[county_data_current.length-1].values[index]
                                    deathtotal   = county_data_current[county_data_current.length-1].values[index][data_column_current]
                                    deathtotal_baseline = county_data_current[county_data_current.length-1].values[index]["baseline_" + data_column_current]

                             
                                    if ( data_column_current=="cumulative") {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(d3.mouse(this)[0]))+"</b> - " + "Predicted cumulative cases: " + formatNumber(Math.round(deathtotal)) + " | +"  + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                       d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(d3.mouse(this)[0]))+"</b> - " + "Predicted cumulative cases: " + formatNumber(Math.round(deathtotal)))
                                      }
                                    } else {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(d3.mouse(this)[0])) +"</b> - " +"Predicted daily cases: " + formatNumber(Math.round(deathtotal)) + " | +" + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(d3.mouse(this)[0])) +"</b> - " + "Predicted daily cases: " + formatNumber(Math.round(deathtotal)))
                                      }
                                    }



                                    county_data_current.slice(0,-1).forEach(function(d,i) {
                                      if (data_column_current=="cumulative") {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + " | +" +  
                                            formatNumber(Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_" + data_column_current]))) + "")
                                        } else {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])))
                                        }
                                      } else {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + " | +" +  
                                          formatNumber( Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_"+data_column_current]))) +"")
                                        } else {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])))
                                        }
                                      }
                                    })

                                  } else {
                                    x0                  = xScale_mini.invert(d3.mouse(this)[0])
                                    index               = bisect(county_data_current[county_data_current.length-1].values, x0, 1)
                                    selectedData        = county_data_current[county_data_current.length-1].values[index]
                                    deathtotal          = county_data_current[county_data_current.length-1].values[index][data_column_current]
                                    deathtotal_baseline = county_data_current[county_data_current.length-1].values[index]["baseline_" + data_column_current]

                                    if ( data_column_current=="cumulative") {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_mini.invert(d3.mouse(this)[0])) + "</b> - " + "Predicted cumulative cases: " + formatNumber(Math.round(deathtotal)) +  " | +" + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                       d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_mini.invert(d3.mouse(this)[0])) + "</b> - " + "Predicted cumulative cases: "+ formatNumber(Math.round(deathtotal)))
                                      }
                                    } else {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" +timeForm(xScale_mini.invert(d3.mouse(this)[0]))+ "</b> - " + "Predicted daily cases: " + formatNumber(Math.round(deathtotal)) +  " | +" + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_mini.invert(d3.mouse(this)[0])) + "</b> - " + "Predicted daily cases: " + formatNumber(Math.round(deathtotal)))
                                      }
                                    }
                                  }

                                  county_data_current.slice(0,-1).forEach(function(d,i) {
                                      if (data_column_current=="cumulative") {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + 
                                            " | +" + formatNumber(Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_" + data_column_current]))) +"")
                                        } else {
                                         d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + 
                                          formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) )
                                        }
                                      } else {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + 
                                            formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + " | +" + 
                                          formatNumber( Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_"+data_column_current]))) +"")
                                        } else {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + 
                                            formatNumber(Math.round(county_data_current[i].values[index][data_column_current])))
                                        }
                                      }
                                  })
                  
                                  d3.selectAll(".focus_main")
                                     .attr("x1", xScale_main(selectedData.date))
                                     .attr("x2", xScale_main(selectedData.date))

                                   d3.selectAll(".focus_mini")
                                     .attr("x1", xScale_mini(selectedData.date))
                                     .attr("x2", xScale_mini(selectedData.date))
                              }



                            
                            })
                            .on("touchmove", function(e) {
                                if (drag==false) {
                                  d3.selectAll(".btn-xss-mini").interrupt()
                                  d3.selectAll(".btn-xss-mini").style("color", "white")
                                  if (svg_id==-1) {
                                    x_cord_touch = +d3.touches(this)[0][0]
                                    x0           = xScale_main.invert(x_cord_touch)
                                    index        = bisect(county_data_current[county_data_current.length-1].values, x0, 1)
                                    selectedData = county_data_current[county_data_current.length-1].values[index]
                                    deathtotal   = county_data_current[county_data_current.length-1].values[index][data_column_current]
                                    deathtotal_baseline = county_data_current[county_data_current.length-1].values[index]["baseline_" + data_column_current]

                             
                                    if ( data_column_current=="cumulative") {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(x_cord_touch))+"</b> - " + "Predicted cumulative cases: " + formatNumber(Math.round(deathtotal)) + " | +"  + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                       d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(x_cord_touch))+"</b> - " + "Predicted cumulative cases: " + formatNumber(Math.round(deathtotal)))
                                      }
                                    } else {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(x_cord_touch)) +"</b> - " +"Predicted daily cases: " + formatNumber(Math.round(deathtotal)) + " | +" + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_main.invert(x_cord_touch)) +"</b> - " + "Predicted daily cases: " + formatNumber(Math.round(deathtotal)))
                                      }
                                    }



                                    county_data_current.slice(0,-1).forEach(function(d,i) {
                                      if (data_column_current=="cumulative") {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + " | +" +  
                                            formatNumber(Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_" + data_column_current]))) + "")
                                        } else {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])))
                                        }
                                      } else {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + " | +" +  
                                          formatNumber( Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_"+data_column_current]))) +"")
                                        } else {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])))
                                        }
                                      }
                                    })

                                  } else {

                                    x_cord_touch = +d3.touches(this)[0][0]                                    
                                    x0                  = xScale_mini.invert(x_cord_touch)
                                    index               = bisect(county_data_current[county_data_current.length-1].values, x0, 1)
                                    selectedData        = county_data_current[county_data_current.length-1].values[index]
                                    deathtotal          = county_data_current[county_data_current.length-1].values[index][data_column_current]
                                    deathtotal_baseline = county_data_current[county_data_current.length-1].values[index]["baseline_" + data_column_current]

                                    if ( data_column_current=="cumulative") {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_mini.invert(x_cord_touch)) + "</b> - " + "Predicted cumulative cases: " + formatNumber(Math.round(deathtotal)) +  " | +" + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                       d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_mini.invert(x_cord_touch)) + "</b> - " + "Predicted cumulative cases: "+ formatNumber(Math.round(deathtotal)))
                                      }
                                    } else {
                                      if (xScale_main(x0)>xScale_main(relax_date_current)) {
                                        d3.selectAll("#div_stat_0").html("<b>" +timeForm(xScale_mini.invert(x_cord_touch))+ "</b> - " + "Predicted daily cases: " + formatNumber(Math.round(deathtotal)) +  " | +" + formatNumber(Math.round(deathtotal-deathtotal_baseline))+" cases relative to baseline scenario")
                                      } else {
                                        d3.selectAll("#div_stat_0").html("<b>" + timeForm(xScale_mini.invert(x_cord_touch)) + "</b> - " + "Predicted daily cases: " + formatNumber(Math.round(deathtotal)))
                                      }
                                    }
                                  }

                                  county_data_current.slice(0,-1).forEach(function(d,i) {
                                      if (data_column_current=="cumulative") {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + 
                                            " | +" + formatNumber(Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_" + data_column_current]))) +"")
                                        } else {
                                         d3.selectAll("#div_stat_"+(i+1)).html("Predicted cumulative cases: " + 
                                          formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) )
                                        }
                                      } else {
                                        if (xScale_mini(x0)>xScale_mini(relax_date_current)) {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + 
                                            formatNumber(Math.round(county_data_current[i].values[index][data_column_current])) + " | +" + 
                                          formatNumber( Math.round(Math.round(county_data_current[i].values[index][data_column_current]) - Math.round(county_data_current[i].values[index]["baseline_"+data_column_current]))) +"")
                                        } else {
                                          d3.selectAll("#div_stat_"+(i+1)).html("Predicted daily cases: " + 
                                            formatNumber(Math.round(county_data_current[i].values[index][data_column_current])))
                                        }
                                      }
                                  })
                  
                                  d3.selectAll(".focus_main")
                                     .attr("x1", xScale_main(selectedData.date))
                                     .attr("x2", xScale_main(selectedData.date))

                                   d3.selectAll(".focus_mini")
                                     .attr("x1", xScale_mini(selectedData.date))
                                     .attr("x2", xScale_mini(selectedData.date))
                              }

                            })



            // Draw main objects - layer: svg_graph_layer_1

            lines = svg_graph_layer_1.selectAll("lines")
                                     .data(function() {
                                      if (svg_id==-1) {
                                        return([county_data_current[county_data_current.length-1]])
                                      } else {
                                        return([county_data_current[svg_id]])
                                      }
                                     })
                                     .enter()
                                     .append("g")
                                     .attr("transform", "translate(" + 0 + ",0)")
 
             ghost_lines_baseline = lines.append("path")
                                .attr("class", function(d) {
                                  if (d.key=="") {
                                    return("ghost-line-baseline")
                                  } else {
                                    return("ghost-line-baseline")
                                  }
                                })
                               .attr("d", function(d) { return line_baseline(d.values) }) 
                
            lines.append("path")
                 .attr("class", "line")
                 .attr("d", function(d) { 
                   return line(d.values.filter(function(e) {
                   return(xScale_main(e.date)>xScale_main(timeConv(today)))})) 
                 }) 

            


            ghost_lines_actual = lines.append("path")
                                .attr("class", function(d) {
                                  if (d.key=="") {
                                    return("ghost-line-actual")
                                  } else {
                                    return("ghost-line-actual")
                                  }
                                })
                               .attr("d", function(d) { return line_actual(d.values.filter(function(e) {return (e.actual_daily!=-1)})) }) 
                               .attr("opacity",1)


          
            brush = svg_graph_layer_2.append("g")
                                     .append("line")
                                     .attr("class", function() {
                                       if(svg_id==-1) {
                                          return("brush brush_main")
                                        } else {
                                          return("brush brush_mini")
                                        }
                                      })
                                      .attr("id","brush_"+svg_id)
                                      .attr("stroke", "black")
                                      .attr("y1", 0)
                                      .attr("y2", ((yScale_temp.range()[0]-yScale_temp.range()[1])*-1))
                                      .attr("x1", xScale_temp(timeConv(relax_date_default)))
                                      .attr("x2", xScale_temp(timeConv(relax_date_default)))
                                      .on("mouseover", function () {
                                          if (drag==false & windowWidth>mobileWidth) {
                                            if (svg_id==-1) {
                                              d3.selectAll("#div_stat_"+(svg_id+1)).html("Drag the line to the right or left to modify the date when restrictions begin being lifted")           
                                              d3.selectAll(".btn-xss-mini").style("color", "#02124c") 
                                            } else {
                                              d3.selectAll("#div_stat_"+(svg_id+1)).html("Drag the line to modify the relax date")           

                                            }

                                          }})

                                      .on("mouseout", function() {
                                        d3.selectAll(".btn-xss-mini").style("color", "white")           
                     
                                      })
            // Links
            if (svg_id!=-1) {

              svg_graph_layer_2.append("g")
                      .append("text")
                      .attr("id", "svg_top_small")
                      .attr("class", "sliding-link-scenario_"+svg_id)
                      .attr("opacity",0)
                      .attr("transform", function() {
                        if (svg_id==-1) {
                        return("translate(" + (10) + "," + ((yScale_temp.range()[0]-yScale_temp.range()[1])*-.95) +")")
                        } else {
                        return("translate(" + (10) + "," + ((yScale_temp.range()[0]-yScale_temp.range()[1])*-.93) +")")
                        }
                      })
                      .text("Scenario")
                      .attr("text-decoration", "none")
                      .style("text-anchor", "start")
                      .style("dominant-baseline", "central")
                      .style("font-weight","100")
                      .style("font-size", function()
                       {if (svg_id==-1) {return("14px")} else{return("14px")}})
                      .style("cursor","pointer")
                      .style("fill","#0059bb")

              svg_graph_layer_2.append("g")
                      .append("text")
                      .attr("id", "svg_top_small")
                      .attr("class", "sliding-link-predicted_"+svg_id)
                      .attr("transform", function() {
                        if (svg_id==-1) {
                        return("translate(" + (10) + "," + ((yScale_temp.range()[0]-yScale_temp.range()[1])*-.9) +")")
                        } else {
                        return("translate(" + (10) + "," + ((yScale_temp.range()[0]-yScale_temp.range()[1])*-.83) +")")
                        }
                      })                     
                      .text("Baseline")
                      .attr("text-decoration", "none")
                      .style("text-anchor", "start")
                      .style("dominant-baseline", "central")
                      .style("font-weight","100")
                      .attr("opacity",0)
                      .style("font-size", function(){
                        if (svg_id==-1) {
                          return("14px")
                        } else{
                          return("14px")
                        }
                      })
                      .style("cursor","pointer")
                      .style("fill","#bdbdbd")
    

              svg_graph_layer_2.append("g")
                      .append("text")
                      .attr("id", "svg_top_small")
                      .attr("class", "sliding-link-actual_"+svg_id)
                      .attr("transform", function() {
                        if (svg_id==-1) {
                        return("translate(" + (10) + "," + ((yScale_temp.range()[0]-yScale_temp.range()[1])*-.85) +")")
                        } else {
                        return("translate(" + (10) + "," + ((yScale_temp.range()[0]-yScale_temp.range()[1])*-.73) +")")
                        }
                      })                          
                      .text("Actual")
                      .attr("text-decoration", "none")
                      .attr("opacity",0)
                      .style("text-anchor", "start")
                      .style("dominant-baseline", "central")
                      .style("font-weight","100")
                      .style("font-size", function(){
                        if (svg_id==-1) {
                          return("14px")
                        } else{
                          return("14px")
                        }
                      })
                      .style("cursor","pointer")
                      .style("fill","#525252")
    
              d3.selectAll(".sliding-link-actual_"+svg_id)
                .on("mouseover", function() {

                  d3.selectAll(".sliding-link-actual_"+svg_id).text("| Actual").attr("text-decoration", "none")                
                  d3.selectAll(".sliding-link-actual_"+svg_id).attr("opacity","0.7")
                  d3.selectAll(".sliding-link-scenario_"+svg_id).attr("opacity","0.7")
                  d3.selectAll(".sliding-link-predicted_"+svg_id).attr("opacity","0.7")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "1")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "0.1")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "0.1")

                


                })
                .on("mouseout", function() {
                  d3.selectAll(".sliding-link-actual_"+svg_id).text("Actual").attr("text-decoration", "none")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {

                    if (actual_hide==false) {
                      return("1") 
                    } else {
                      return("0")
                    }

                })})
    
    
              d3.selectAll(".sliding-link-predicted_"+svg_id)
                .on("mouseover", function() {

                  d3.selectAll(".sliding-link-predicted_"+svg_id).text("| Baseline").attr("text-decoration", "none")                
                  d3.selectAll(".sliding-link-predicted_"+svg_id).attr("opacity","0.7")
                  d3.selectAll(".sliding-link-actual_"+svg_id).attr("opacity","0.7")
                  d3.selectAll(".sliding-link-scenario_"+svg_id).attr("opacity","0.7")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "0.1")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0.1")
                })
                .on("mouseout", function() {

                  d3.selectAll(".sliding-link-predicted_"+svg_id).text("Baseline").attr("text-decoration", "none")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".line").attr("opacity", "1")

                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {

                    if (actual_hide==false) {
                      return("1") 
                    } else {
                      return("0")
                    }})

              })

              d3.selectAll(".sliding-link-scenario_"+svg_id)
                .on("mouseover", function() {

                  d3.selectAll(".sliding-link-scenario_"+svg_id).text("| Scenario").attr("text-decoration", "none")
                  d3.selectAll(".sliding-link-predicted_"+svg_id).attr("opacity","0.7")
                  d3.selectAll(".sliding-link-actual_"+svg_id).attr("opacity","0.7")
                  d3.selectAll(".sliding-link-scenario_"+svg_id).attr("opacity","0.7")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "0.1")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", "0.1")
              })
              .on("mouseout", function() {

                  d3.selectAll(".sliding-link-scenario_"+svg_id).text("Scenario").attr("text-decoration", "none")
                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-baseline").attr("opacity", "1")

                  d3.selectAll("#svg_graph_"+(svg_id+1)).selectAll(".ghost-line-actual").attr("opacity", function() {

                    if (actual_hide==false) {
                      return("1") 
                    } else {
                      return("0")
                    }})
    
              })

            }

            if (windowWidth>mobileWidth) {
              if (svg_id==-1) {
                dragHandler_main(d3.selectAll("#brush_"+svg_id))
              } else {
                dragHandler_mini(d3.selectAll("#brush_"+svg_id))
              }
            }

            // Draw Helpers
            DrawGraphHelper(svg_id)
     
          }
      
          /// DrawMiddle
          // ----------------------------------------
          function DrawMiddle() {



          }
    
        
          /// DrawBottom
          // ----------------------------------------
          function DrawBottom() {
             
            // Section 1
            text_content = "Model benefits"
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom1")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.61) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.56)) +")")
                }
              })                   
              .append('text')

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom1")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_big")

            // Section 2
              if (windowWidth>mobileWidth) {
                text_content = "<br><br>Predictions are based on a machine learning model grounded in SIR dynamics. The model has two key features: (1) County-level granularity and (2) mobility-based scenarios."

                } else {
                text_content = "<br><br>Predictions are based on a machine learning model grounded in SIR dynamics. The model has two key features: (1) County-level granularity and (2) mobility-based scenarios."
                }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom2")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })              
              .append('text')

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom2")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")

            // Section 3
            if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br>1) <b>County-level granularity</b>: The model learns on all counties simultaneously by leveraging the asynchronous evolution of the disease in different regions; this enables accurate and stable predictions even for counties just entering the epidemic. Specifically, model parameters between counties are tied together, allowing data from counties whose cases are 'plateauing' to help predict in counties with similar features that are earlier on in the curve. The model incorporates both static and dynamic county-level features, including demographics, housing conditions, comorbidities, family interaction patterns, and social mobility."

                } else {
            text_content = "<br><br><br><br><br>1) <b>County-level granularity</b>: The model learns on all counties simultaneously by leveraging the asynchronous evolution of the disease in different regions; this enables accurate and stable predictions even for counties just entering the epidemic. Specifically, model parameters between counties are tied together, allowing data from counties whose cases are 'plateauing' to help predict in counties with similar features that are earlier on in the curve. The model incorporates both static and dynamic county-level features, including demographics, housing conditions, comorbidities, family interaction patterns, and social mobility."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom3")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })   
              .append('text')

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom3")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")

            // Section 4
                 if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br>2) <b>Mobility-based scenarios</b>: Social mobility, based on the movement of anonymous cellphone data, is a key dynamic feature driving the predictions. Given the heterogeneity in mobility, the model can learn its explanatory power, enabling the model to provide mobility-based scenario projections. A scenario assumes that social mobility will return to normal levels according to a particular date and speed:"

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br>2) <b>Mobility-based scenarios</b>: Social mobility, based on the movement of anonymous cellphone data, is a key dynamic feature driving the predictions. Given the heterogeneity in mobility, the model can learn its explanatory power, enabling the model to provide mobility-based scenario projections. A scenario assumes that social mobility will return to normal levels according to a particular date and speed:"
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom4")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })   
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom4")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")

            // Section 4a
                 if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;&#8226; <b>Relax date</b>: The date at which mobility starts to increase back to normal levels. Before this date, the mobility level remains constant at the current value."

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;&#8226; <b>Relax date</b>: The date at which mobility starts to increase back to normal levels. Before this date, the <br>&nbsp;&nbsp;&nbsp;&nbsp;mobility level remains constant at the current value."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom4a")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })  

              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom4a")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")

            // Section 4b
                  if (windowWidth>mobileWidth) {
           text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;&#8226; <b>Speed of mobility change</b>: How quickly mobility returns to normal levels, specified as the % of normal levels recovered by one month from the relax date, e.g., a speed of 50% means that one month from<br>&nbsp;&nbsp;&nbsp;&nbsp; the relax date, the mobility level will be halfway between current and normal levels. A speed of 75% means that the mobility level will be 75% of the way from current to normal levels in one month."

                } else {
           text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>&nbsp;&nbsp;&nbsp;&#8226; <b>Speed of mobility change</b>: How quickly mobility returns to normal levels, specified as the % of <br>&nbsp;&nbsp;&nbsp;&nbsp;normal levels recovered by one month from the relax date, e.g., a speed of 50% means that one month <br>&nbsp;&nbsp;&nbsp;&nbsp;from the relax date, the mobility level will be halfway between current and normal levels. A speed of <br>&nbsp;&nbsp;&nbsp;&nbsp;75% means that the mobility level will be 75% of the way from current to normal levels in one month."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom4b")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })  

              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom4b")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")

            // Section 4b
                if (windowWidth>mobileWidth) {
           text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>The <b>Baseline</b> predictions assume that mobility remains constant at the observed value at the current date."

                } else {
           text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>The <b>Baseline</b> predictions assume that mobility remains constant at the observed value at the current date."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom4c")
             .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })  

              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom4c")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", height_vis_bottom*0.75)

             // Section 7
                       if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Model limitations" 

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Model limitations" 
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom7")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })             
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom7")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_big")
              .attr("height", height_vis_bottom*2)

            // Section 8a
                    if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>1) The predictions only include counties whose total number of cases exceeds 100."

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>1) The predictions only include counties whose total number of cases exceeds 100."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom8a")
             .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })                 
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom8a")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", height_vis_bottom*2)

            // Section 8b
                 if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>2) The social mobility scenarios do not take into account protective measures such as wearing masks or keeping a 6 feet distance."

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>2) The social mobility scenarios do not take into account protective measures such as wearing masks or keeping a 6 feet distance."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom8b")
           .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })                   
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom8b")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", height_vis_bottom*2)

             // Section 8c
               if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Technical details" 

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Technical details" 
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom8c")
                .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.75) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.75)) +")")
                }
              })              
              .append('text')
              .attr("id","svg_bottom_big")
              .attr("height", height_vis_bottom*2)

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom8c")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_big")
              .attr("height", height_vis_bottom*2)



            // Section 9
            if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Updates"

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Updates"
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom9")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.9) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.9)) +")")
                }
              })                
              .append('text')
              .attr("id","svg_bottom_big")
              

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            
            d3.selectAll("#textrectbottom9")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_big")
              .attr("height", height_vis_bottom*2) 

            // Section 10
                 if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>The data and model are updated daily (Last update: " + update_date +")."

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>The data and model are updated daily (Last update: " + update_date +")."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom10")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }})               
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom10")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", height_vis_bottom*2)

            // Section 9
            if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Contact"

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Contact"
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom91")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.85) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.85)) +")")
                }
              })                
              .append('text')
              .attr("id","svg_bottom_big")
              

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            
            d3.selectAll("#textrectbottom91")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_big")
              .attr("height", height_vis_bottom*2) 


            

 
            // Section 11
               if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><i>Website and visualizations designed and built by <a href='https://www.linkedin.com/in/claramarquardt/' target='_blank'>Clara Marquardt</a> using <a href='https://d3js.org/'>d3</a>.</i>"

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><i>Website and visualizations designed and built by <a href='https://www.linkedin.com/in/claramarquardt/' target='_blank'>Clara Marquardt</a> using <a href='https://d3js.org/'>d3</a>.</i>"
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom11")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.5) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }})               
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom11")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", height_vis_bottom*2)

 if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Reach out to us at <a href='mailto:covidalliance-model@mit.edu'>covidalliance-model@mit.edu</a>."

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Reach out to us at <a href='mailto:covidalliance-model@mit.edu'>covidalliance-model@mit.edu</a>."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom101")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.43) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.59)) +")")
                }})               
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom101")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", function() {
                if (windowWidth>mobileWidth) {
                  return(height_vis_bottom*0.87)

                } else {
                  return(height_vis_bottom*0.87)
                }
              })




            // Section 5
                              if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br>Data used" 

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>Data used" 
            }

    
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom5")
                 .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.7) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.7)) +")")
                }
              })    
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom5")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_big")
              .attr("height", height_vis_bottom*0.5)





            // Section 8d
            if (windowWidth>mobileWidth) {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>For a technical description of our methods, refer to our paper <a href='https://arxiv.org/abs/2006.06373' target='_blank'>'The Limits to Learning an SIR Process: Granular Forecasting for Covid-19'</a>."

                } else {
            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>For a technical description of our methods, refer to our paper <a href='https://arxiv.org/abs/2006.06373' target='_blank'>'The Limits to Learning an SIR Process: Granular Forecasting for Covid-19'</a>."
            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom8d")
              .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.55) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.5)) +")")
                }
              })             
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom8d")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small") 
              .attr("height",  function() {
                if (windowWidth>mobileWidth) {
                  return(height_vis_bottom*0.8)

                } else {
                  return(height_vis_bottom*0.8)
                }
              })


              

            // Section 6
                   if (windowWidth>mobileWidth) {
                            text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>- <b><a href='https://github.com/nytimes/covid-19-data' target='_blank'>The New York Times</a></b> (Number of COVID-19 cases and deaths)<br>- <b><a href='https://covidtracking.com/' target='_blank'>COVID Tracking Project</a></b> (Number of tests and hospitalizations)<br>- <b><a href='https://www.cdc.gov/dhdsp/maps/atlas/index.html' target='_blank'>CDC</a></b> (Prevalence of comorbidities such as e.g., obesity, cardiovascular diseases)<br>- <b><a href='https://www.claritas.com/' target='_blank'>Claritas</a></b> (Demographic information, including population numbers)<br>- <b><a href='https://www.cms.gov/Medicare/Medicare' target='_blank'>Medicare, websites of various state governments</a></b> (Number of nursing homes/care facilities)<br>- <b><a href='https://www.safegraph.com/' target='_blank'>Safegraph</a></b> (Mobility information)<br>- <b><a href='https://hrs.isr.umich.edu/about?_ga=2.264317707.586889544.1586731875-1980389060.1586731875' target='_blank'>Health and Retirement Study</a></b> (Proximity to family information)"


                } else {
                                              text_content = "<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>- <b><a href='https://github.com/nytimes/covid-19-data' target='_blank'>The New York Times</a></b> (Number of COVID-19 cases and deaths)<br>- <b><a href='https://covidtracking.com/' target='_blank'>COVID Tracking Project</a></b> (Number of tests and hospitalizations)<br>- <b><a href='https://www.cdc.gov/dhdsp/maps/atlas/index.html' target='_blank'>CDC</a></b> (Prevalence of comorbidities such as e.g., obesity, cardiovascular diseases)<br>- <b><a href='https://www.claritas.com/' target='_blank'>Claritas</a></b> (Demographic information, including population numbers)<br>- <b><a href='https://www.cms.gov/Medicare/Medicare' target='_blank'>Medicare, websites of various state governments</a></b> (Number of nursing homes/care facilities)<br>- <b><a href='https://www.safegraph.com/' target='_blank'>Safegraph</a></b> (Mobility information)<br>- <b><a href='https://hrs.isr.umich.edu/about?_ga=2.264317707.586889544.1586731875-1980389060.1586731875' target='_blank'>Health and Retirement Study</a></b> (Proximity to family information)"

            }
            text_node    = d3.selectAll("#svg_bottom")
              .append("g")
              .attr("id","textrectbottom6")
                   .attr("transform", function() {
                if (windowWidth>mobileWidth) {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/8)*.6) +")")
                } else {
                  return("translate(" + (0)+ "," + ((height_vis_bottom/10)*(0.6)) +")")
                }
              })              
              .append('text')
              .attr("id","svg_bottom_big")

            text_node.text(text_content)
            bounds  = d3.selectAll("#svg_bottom").node()
            padding = 10
            wrap    = d3.textwrap()
            wrap.bounds(bounds).padding(padding)
            text_node.call(wrap) 

            d3.selectAll("#textrectbottom6")
              .selectAll("#textwrapdiv")
              .attr("width", function() {
                if (windowWidth>mobileWidth) {
                  return(width*0.9)

                } else {
                  return(width_vis_main*.9)
                }
              })
              .attr("id","svg_bottom_small")
              .attr("height", function() {
 if (windowWidth>mobileWidth) {
                  return(height_vis_bottom*0.6)

                } else {
                  return(height_vis_bottom*0.6)
                }})

                
          }
          
          /// Brush functions
          // ----------------------------------------
          function dragHandler_drag_helper(x0_temp) {

              // Update other brushes
               d3.selectAll(".brush_mini")
                 .attr("x1", xScale_mini(x0_temp))
                 .attr("x2", xScale_mini(x0_temp))

               d3.selectAll(".brush_main")
                 .attr("x1", xScale_main(x0_temp))
                 .attr("x2", xScale_main(x0_temp))

               // Update position of all the focuses brush
               d3.selectAll(".focus_main")
                 .attr("x1", xScale_main(x0_temp))
                 .attr("x2", xScale_main(x0_temp))
      
               d3.selectAll(".focus_mini")
                 .attr("x1", xScale_mini(x0_temp))
                 .attr("x2", xScale_mini(x0_temp))

              // Update position of label
               d3.select("#brush_label_-1")
                  .attr("y", (xScale_main(x0_temp)-1.5*(xScale_main(timeConv(relax_date_default_plus)) 
                    - xScale_main(timeConv(relax_date_default_minus)))))

   
            }

            function dragHandler_end_helper(x0_temp) {

              // Update visualization

              UpdateVisualization(data_column=data_column_current, relax_date=x0_temp, relax_speed=relax_speed_current, agg_county=-1)
              
              drag=false

            }
          
          // brush
          dragHandler_main = d3.drag()
                               .on("drag", function () {

                                  drag=true

                                  // Get coordinates
                                  x0_date = xScale_main.invert(d3.event.x)
                                  
                                  // Condition
                                  if (xScale_main(x0_date) >= xScale_main(timeConv(today)) & xScale_main(x0_date) <= (xScale_main(timeConv(graphsetup.latest_prediction_date)))) {
                                    
                                    d3.selectAll("#div_stat_0").html("Begin relaxing restrictions on " + timeForm(x0_date))
                                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")

                                    // Execute
                                    dragHandler_drag_helper(x0_date)
                                  } else if (xScale_main(x0_date) <= (xScale_main(timeConv(graphsetup.latest_prediction_date)))) {
                                    
                                    d3.selectAll("#div_stat_0").html("Begin relaxing restrictions on " + timeForm(timeConv(today)))
                                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")

                                    // Execute
                                    dragHandler_drag_helper(timeConv(today))



                                  } else {

                                    d3.selectAll("#div_stat_0").html("Begin relaxing restrictions on " + timeForm(timeConv(graphsetup.latest_prediction_date)))
                                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")

                                    // Execute
                                    dragHandler_drag_helper(timeConv(graphsetup.latest_prediction_date))


                                  }

              
                                })
                                .on("end", function() {

                                  // Get coordinates
                                  x0_date = xScale_main.invert(d3.event.x)
                                  if (xScale_main(x0_date) >= xScale_main(timeConv(today)) & xScale_main(x0_date) <= (xScale_main(timeConv(graphsetup.latest_prediction_date)))) {

                                    // Execute
                                    d3.selectAll(".btn-xss-mini").style("color", "white")
                                    dragHandler_end_helper(x0_date)

                                  } else if (xScale_main(x0_date) <= (xScale_main(timeConv(graphsetup.latest_prediction_date)))) {

                                    d3.selectAll(".btn-xss-mini").style("color", "white")
                                    dragHandler_end_helper(timeConv(today))

                                  } else {

                                    d3.selectAll(".btn-xss-mini").style("color", "white")
                                    dragHandler_end_helper(timeConv(graphsetup.latest_prediction_date))                                    
                                  }
                                  
                                })

          // brush
          dragHandler_mini = d3.drag()
                               .on("drag", function () {

                                  drag=true

                                  // Get coordinates
                                  x0_date = xScale_mini.invert(d3.event.x)
                                  
                                  // Condition
                                  if (xScale_mini(x0_date) >= xScale_mini(timeConv(today)) & xScale_mini(x0_date) <= (xScale_mini(timeConv(graphsetup.latest_prediction_date)))) {
                      
                                    d3.selectAll("#div_stat_0").html("Begin relaxing restrictions on " + timeForm(x0_date))
                                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")
              
                                    // Execute
                                    dragHandler_drag_helper(x0_date)
                                  } else if(xScale_mini(x0_date) <= (xScale_mini(timeConv(graphsetup.latest_prediction_date)))) {
 
                                    d3.selectAll("#div_stat_0").html("Begin relaxing restrictions on " + timeForm(timeConv(today)))
                                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")
              
                                    // Execute
                                    dragHandler_drag_helper(timeConv(today))

                                  } else {

                                    d3.selectAll("#div_stat_0").html("Begin relaxing restrictions on " + timeForm(timeConv(graphsetup.latest_prediction_date)))
                                    d3.selectAll(".btn-xss-mini").style("color", "#02124c")
              
                                    // Execute
                                    dragHandler_drag_helper(timeConv(graphsetup.latest_prediction_date))

                                  }

              
                                })
                                .on("end", function() {
                  
                                  // Get coordinates
                                  x0_date = xScale_mini.invert(d3.event.x)
                                  
                                  if (xScale_mini(x0_date) >= xScale_mini(timeConv(today)) & xScale_mini(x0_date) <= (xScale_mini(timeConv(graphsetup.latest_prediction_date)))) {

                                    // Execute
                                    d3.selectAll(".btn-xss-mini").style("color", "white")
                                    dragHandler_end_helper(x0_date)

                                  } else if(xScale_mini(x0_date) <= (xScale_mini(timeConv(graphsetup.latest_prediction_date)))) {

                                    // Execute
                                    d3.selectAll(".btn-xss-mini").style("color", "white")
                                    dragHandler_end_helper(timeConv(today))

                                  } else {

                                    // Execute
                                    d3.selectAll(".btn-xss-mini").style("color", "white")
                                    dragHandler_end_helper(timeConv(graphsetup.latest_prediction_date))

                                  }

                                })



        
          /// DrawVisualization_static
          // ----------------------------------------
          function DrawVisualization_static() {
          
            // Update parameters
            data_column_current = "cumulative"

            // Empty canvas
            d3.selectAll(".divdynamic").remove()
            d3.selectAll(".multiple").remove()
            d3.selectAll("#svg_top_1").remove()
            d3.selectAll("#svg_graph_container_mini").remove()


            // Create structure
            if (windowWidth>mobileWidth) {
              row = d3.select("#viz1")
                      .append("div")
                      .attr("class", "row h-100")
                      .attr("id", "row_top_1")
                      
              row.append("div")
                 .attr("class","col")
                 .attr("id", "viz2a")

              row.append("div")
                 .attr("class","col")
                 .attr("id", "viz2b")

            } else {

              row1 = d3.select("#viz1")
                      .append("div")
                      .attr("class", "row h-100")
                      .attr("id", "row_top_1")

              row2 = d3.select("#viz2")
                      .append("div")
                      .attr("class", "row h-100")
                      .attr("id", "row_top_2")  

              row1.append("div")
                 .attr("class","col")
                 .attr("id", "viz2a")

              row2.append("div")
                 .attr("class","col")
                 .attr("id", "viz2b")

            }

            // Initialize the canvas
            svg_map = d3.select("#viz2a")
                          .append("div")
                        .attr("id","svg_map_container")
                        .attr("class","text-center divdynamic")
                        .append("svg")
                        .attr("id","svg_map")
                        .attr("preserveAspectRatio", "xMinYMin meet")
                        .attr("viewBox", "0 0 " + width_vis_main + " " + height_vis_main)
                        .classed("svg-content", true)

            svg_graph_0 = d3.select("#viz2b")
                         .append("div")
                         .attr("id","svg_graph_container") 
                         .attr("class","text-center divdynamic")
                         .append("svg")
                         .attr("id", "svg_graph_0")
                         .attr("preserveAspectRatio", "xMinYMin meet")
                         .attr("viewBox", "0 0 " + width_vis_main + " " + height_vis_main)
                         .classed("svg-content", true)

            svg_bottom = d3.select("#viz4")
                           .append("div")
                           .attr("id","svg_bottom_container")
                           .attr("class","divdynamic text-wrap")
                           .append("svg")
                           .attr("id", "svg_bottom")
                           .attr("preserveAspectRatio", "xMinYMin meet")
                           .attr("viewBox", function() {
                            if (windowWidth>mobileWidth) {
                              return("0 0 " + width + " " + height_vis_bottom)
                            } else {
                              return("0 0 " + width_vis_main + " " + height_vis_bottom)
                            }})
                           .classed("svg-content", true)    

            svg_top = d3.select("#viz")
                        .append("div")
                        .attr("id","svg_top_container")
                        .attr("class","divdynamic")
                        .append("svg")
                        .attr("id","svg_top")
                        .attr("preserveAspectRatio", "xMinYMin meet")
                        .attr("viewBox", function() {
                            if (windowWidth>mobileWidth) {
                              return("0 0 " + width + " " + height_top)
                            } else {
                              return("0 0 " + width_vis_main + " " + height_top)
                        }})                   
                        .classed("svg-content", true)
                        .attr("transform", "translate(" + 0 + ","+padding_top+")")

            svg_middle = d3.select("#vizmiddle")
                        .append("div")
                        .attr("id","svg_middle_container")
                        .attr("class","divdynamic") 


            // Draw visualizations
            DrawMap()
            DrawGraph(svg_id=-1)
            DrawBottom()
            DrawTop()
            DrawMiddle()

            // Draw mini visualizations
            if (windowWidth>mobileWidth) {
              graph_per_row = 4
            } else {
              graph_per_row = 2
            }

            // Initialize
            index_current = -1
            county_data_current_temp = jQuery.extend(true, [], county_data_current.slice(0,-1))

            // Ensure correct length
            while (county_data_current_temp.length != Math.floor(county_data_current_temp.length/graph_per_row)*graph_per_row) {
                county_data_current_temp.push([county_data_current[0]])
            }

            // Iterate
            county_data_current_temp.forEach(function(d,i) {
                
                $(function () {
                  index = Math.floor(i/graph_per_row)*graph_per_row

                  if (index>index_current) {
                    row = d3.select("#viz3")
                            .append("div")
                            .attr("class", "row multiple")
                            .attr("id", "row_"+index)
                    index_current = index
                  }

                d3.selectAll("#row_"+index)
                  .selectAll(".empty")
                  .data([county_data_current.slice(0,-1)[i]])
                  .enter()
                  .append("div")
                  .attr("class",function() {
                    return("col")
                    })
                  .append("div")
                  .attr("id","svg_graph_container_mini")  
                  .attr("class","text-center divdynamic")  
                  .append("svg")
                  .attr("id","svg_graph_"+(i+1))
                  .attr("preserveAspectRatio", "xMinYMin meet")
                  .attr("viewBox", "0 0 " + width_vis_mini + " " + height_vis_mini)
                  .classed("svg-content", true)

                if(i<county_data_current.length-1) {
                    DrawGraph(svg_id=i)}
                 })
              
            })

            d3.selectAll("#download_icon")
                 .on("click", function() {
                  blob       = new Blob([headersVal + Papa.unparse(InitializeDownload(county_data_raw_master), config)], {type: "text/csv"});
                  url        = window.URL.createObjectURL(blob);
                  a          = document.createElement("a");
                  a.href     = url;
                 a.download = "covidalliance_infectiontrajectories_" + state_choice + "_" + today +".csv";
                  a.click();
          })

          }


          // UpdateData
          //--------------------
          function UpdateData(state) {
            
            d3.selectAll("#textrect").remove()
             d3.selectAll("#textrect0")
             .attr("opacity",0)
             .transition()
             .duration(10)
             .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.9) +")")

           d3.selectAll("#textrect1")
           .attr("opacity",0)
             .transition()
             .duration(10)
             .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.85)+")")

           d3.selectAll("#textrect2")
           .attr("opacity",0)
             .transition()
             .duration(10)
             .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.8)+")")

            // Update init
            init            = 0
            county_mode     = false
            county_selected = ''

            // Loading opacity
            county_data_current.forEach(function(d,i) {
              d3.selectAll("#svg_graph_"+i)
              .attr("opacity","0.3")
            })

            d3.selectAll("#svg_bar")
              .attr("opacity","0.3")
 
            d3.selectAll("#svg_map")
              .attr("opacity","0.3")

            // Update parameters
            relax_date_current  = timeConv(relax_date_default)
            relax_speed_current = relax_speed_default
            data_column_current = data_column_default

            county_data_link = d3.csv("/load_county_data?state="+state+"&relax_year="+year(relax_date_current)+"&relax_month="+month(relax_date_current)+"&relax_day="+day(relax_date_current)+"&relax_speed="+relax_speed_current)
            map_data_link    = d3.json("/frontend/data/map/us_temp_final_"+state_conv(county_data_crosswalk_current,state, "id")+".json")

            county_data_link.then(function(county_data_raw) {
              map_data_link.then(function(map_data_raw) {

                // Loading opacity
                county_data_current.forEach(function(d,i) {
                  d3.selectAll("#svg_graph_"+i)
                  .attr("opacity","1")
                })

                d3.selectAll("#svg_bar")
                  .attr("opacity","1")
 
                d3.selectAll("#svg_map")
                  .attr("opacity","1")

                // Initialize the data
                county_data_raw_master = county_data_raw
                county_data_master     = jQuery.extend(true, [], county_data_raw_master)
                county_data_master     = InitializeData(raw_data=county_data_master)
                map_data_current       = map_data_raw

                config = {
                  quotes: true, 
                  quoteChar: '"',
                  escapeChar: '"',
                  delimiter: ",",
                  header: false,
                  newline: "\r\n",
                  skipEmptyLines: false, 
                  columns: null 
                }


                // Scale the data
                county_data_current = jQuery.extend(true, [], county_data_master)

              d3.selectAll("#download_icon")
                 .on("click", function() {
                  blob       = new Blob([headersVal + Papa.unparse(InitializeDownload(county_data_raw_master), config)], {type: "text/csv"});
                  url        = window.URL.createObjectURL(blob);
                  a          = document.createElement("a");
                  a.href     = url;
                 a.download = "covidalliance_infectiontrajectories_" + state_choice + "_" + today +".csv";
                 a.click();
                })

                // Initialize the axes
                yScale_mini_array = []
                county_data_current.slice(0,-1).forEach(function(d,i) {
                  yScale_mini_array.push(yScale_mini)
                })

                // Update any parameters

                /// Colour scales
                rt_values = []
                county_data_current.slice(0,county_data_current.length-1).forEach(function(d) {
                    rt_values.push(d.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t)
                })
                color_min     = d3.min(rt_values)
                color_min_max = 1
                color_max_min = 1.0000000000000001
                color_max     = d3.max(rt_values)
                step_good.range([color_min, color_min_max])
                step_bad.range([color_max_min, color_max])
                color_good.domain([step_good(1), step_good(2), step_good(3), color_min])
                color_bad.domain([color_max, step_bad(3), step_bad(2), step_bad(1)])

                /// County & state choice
                state_choice      = state
                state_choice_rank = states(county_data_crosswalk_current)[0].indexOf(state_choice)
                state_choice_id   = +county_data_crosswalk_current[state_choice_rank].values[0].county_id.slice(0,-3)

                county_choice    = ""
                county_choice_id = 0
                   
                // Update the mini canvas
                d3.selectAll(".multiple > *").remove()

                d3.selectAll(".multiple")
                   .selectAll(null)
                   .data(county_data_current.slice(0,-1))
                   .enter()
                   .append("div")
                   .attr("id","svg_graph_container_mini")  
                   .attr("class","text-center divdynamic")  
                   .append("svg")
                   .attr("id",function(d,i) {
                     return("svg_graph_"+(i+1))
                   })
                   .attr("preserveAspectRatio", "xMinYMin meet")
                   .attr("viewBox", "0 0 " + width_vis_mini + " " + height_vis_mini)
                   .classed("svg-content", true)


                // Redraw
                DrawVisualization_static()
                d3.selectAll(".btn-xss-mini").style("color", "white") 

              })
            })
          }
        
          // UpdateVisualization
          //--------------------
          function UpdateVisualization(data_column, relax_date, relax_speed, agg_county) {

          d3.selectAll("#textrect").remove()
             d3.selectAll("#textrect0")
             .attr("opacity",0)
             .transition()
             .duration(10)
             .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.9) +")")

           d3.selectAll("#textrect1")
           .attr("opacity",0)
             .transition()
             .duration(10)
             .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.85)+")")

           d3.selectAll("#textrect2")
           .attr("opacity",0)
             .transition()
             .duration(10)
             .attr("transform", "translate(" + (0)+ "," + ((yScale_main.range()[0]-yScale_main.range()[1])*-.8)+")")

          if (agg_county==-1) {
            
            // Loading opacity
            county_data_current.forEach(function(d,i) {
              d3.selectAll("#svg_graph_"+i)
                .attr("opacity","0.3")
            })

            d3.selectAll("#svg_bar")
              .attr("opacity","0.3")
 
            d3.selectAll("#svg_map")
              .attr("opacity","0.3")

            // Update brush
            d3.selectAll("#brush_-1")
              .attr("x1", xScale_main(relax_date))
              .attr("x2", xScale_main(relax_date))

            d3.selectAll("#brush_label_-1")
              .attr("y", (xScale_main(relax_date))-1.5*(xScale_main(timeConv(relax_date_default_plus))-
                              xScale_main(timeConv(relax_date_default_minus))))

            county_data_current.forEach(function(d,i) {
              d3.selectAll("#brush_"+i)
                .attr("x1", xScale_mini(relax_date))
                .attr("x2", xScale_mini(relax_date))
              
              d3.selectAll("#brush_label_"+i)
                .attr("y", (xScale_mini(relax_date))+
                              padding_hor_main-1.5*(xScale_mini(timeConv(relax_date_default_plus))-
                              xScale_mini(timeConv(relax_date_default_minus))))

            })

            county_data_link = d3.csv("/load_county_data?state="+state_choice+"&relax_year="+year(relax_date_current)+"&relax_month="+month(relax_date)+"&relax_day="+day(relax_date)+"&relax_speed="+relax_speed)     
            county_data_link.then(function(county_data_raw) { 

              // Loading opacity
              county_data_current.forEach(function(d,i) {
                d3.selectAll("#svg_graph_"+i)
                 .attr("opacity","1")
              })

              d3.selectAll("#svg_bar")
                .attr("opacity","1")
 
              d3.selectAll("#svg_map")
                .attr("opacity","1")


              // Initialize the data
              county_data_raw_master = county_data_raw
              county_data_master     = jQuery.extend(true, [], county_data_raw_master)
              county_data_master     = InitializeData(raw_data=county_data_master)

              config = {
                  quotes: true, 
                  quoteChar: '"',
                  escapeChar: '"',
                  delimiter: ",",
                  header: false,
                  newline: "\r\n",
                  skipEmptyLines: false, 
                  columns: null 
                }

              d3.selectAll("#download_icon")
                 .on("click", function() {
                  blob       = new Blob([headersVal + Papa.unparse(InitializeDownload(county_data_raw_master), config)], {type: "text/csv"});
                  url        = window.URL.createObjectURL(blob);
                  a          = document.createElement("a");
                  a.href     = url;
                 a.download = "covidalliance_infectiontrajectories_" + state_choice + "_" + today +".csv";
                  a.click();
                })

              // Scale the data
              county_data_current = jQuery.extend(true, [], county_data_master)

              // Update data_column
              data_column_current     = data_column
              relax_speed_current     = relax_speed
              relax_date_current      = relax_date

              if (county_mode==true) {
                county_index = counties( county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county_selected)
                county_data_current[county_data_current.length-1] = county_data_current[county_index]
              }

              // Update axes
              graphsetup = InitializeGraph()
              
              // Main scale
              if ((graphsetup.maxValue_main > yScale_main.domain()[1]) | (graphsetup.maxValue_main < 0.5* yScale_main.domain()[1])) {
                graphsetup.maxValue_main = graphsetup.maxValue_main

              } else {

                graphsetup.maxValue_main = yScale_main.domain()[1]
              }

              // Mini scale
              county_data_current.slice(0,-1).forEach(function(d,i) {
                if ((graphsetup.maxValue_mini[i] > yScale_mini_array[i].domain()[1])| (graphsetup.maxValue_mini[i] < 0.5* yScale_mini_array[i].domain()[1])) {
                  graphsetup.maxValue_mini[i] = graphsetup.maxValue_mini[i]

                } else {

                  graphsetup.maxValue_mini[i] = yScale_mini_array[i].domain()[1]
                } 
              })           

              // Helper function
              function UpdateVisualizationHelper(svg_id) {

                // Initialize
                if (svg_id==-1) {
               
                  yScale_main.domain([graphsetup.minValue,graphsetup.maxValue_main]).ticks(10)
              
                  xScale_temp = xScale_main
                  yScale_temp = yScale_main

                } else {
                  
                  if (data_column_current=="cumulative") {
                    yScale_mini_array[svg_id].domain([graphsetup.minValue,graphsetup.maxValue_mini[svg_id]]).ticks(5)
                  } else {
                    yScale_mini_array[svg_id].domain([graphsetup.minValue,graphsetup.maxValue_mini[svg_id]]).ticks(3)
                   
                  }
                  xScale_temp = xScale_mini
                  yScale_temp = yScale_mini_array[svg_id]
                }
                
                yaxis = d3.axisLeft().scale(yScale_temp)
           
                // Update graph
                line = d3.line()
                         .x(function(d) { return xScale_temp(d.date) })
                         .y(function(d) { return yScale_temp(d[data_column_current])})
 
                line_baseline = d3.line()
                         .x(function(d) { return xScale_temp(d.date) })
                         .y(function(d) { return yScale_temp(d["baseline_"+data_column_current])})

                line_actual = d3.line()
                         .x(function(d) { return xScale_temp(d.date) })
                         .y(function(d) { return yScale_temp(d["actual_"+data_column_current])})
              
                yaxis_graph = d3.selectAll("#svg_graph_"+(svg_id+1))
                  .selectAll(".y")
                  .call(yaxis)

                yaxis_graph
                  .selectAll('text')
                  .attr("text-anchor","start")

                yaxis_graph
                  .selectAll('path')
                  .style("opacity",0)

                yaxis_graph
                 .selectAll('line')
                  .style("opacity",0)


                d3.selectAll("#svg_graph_"+(svg_id+1))
                  .selectAll(".line") 
                  .data(function() {
                    if (svg_id==-1) {
                      return([county_data_current[county_data_current.length-1]])
                    } else {
                      return([county_data_current[svg_id]])
                    }
                  })         
                  .transition()
                  .duration(500)
                  .attr("d", function(d) { 
                    return line(d.values.filter(function(e) {
                      return(xScale_main(e.date)>xScale_main(timeConv(today)))
                    })) 
                  }) 
      
                 d3.selectAll("#svg_graph_"+(svg_id+1))
                   .selectAll(".ghost-line-baseline")  
                   .data(function() {
                    if (svg_id==-1) {
                      return([county_data_current[county_data_current.length-1]])
                    } else {
                      return([county_data_current[svg_id]])
                    }
                   })
                   .transition()
                   .duration(500)
                   .attr("d", function(d) { 
                    return line_baseline(d.values) })

                 d3.selectAll("#svg_graph_"+(svg_id+1))
                   .selectAll(".ghost-line-actual")
                  .data(function() {
                    if (svg_id==-1) {
                      return([county_data_current[county_data_current.length-1]])
                    } else {
                      return([county_data_current[svg_id]])
                    }
                   })  
                   .transition()
                   .duration(500)
                    .attr("d", function(d) { return line_actual(d.values.filter(function(e) {return (e.actual_daily!=-1)})) }) 

              }

              // Execute update
              UpdateVisualizationHelper(svg_id=-1)
              county_data_current.slice(0,-1).forEach(function(d,i) {
                UpdateVisualizationHelper(svg_id=i)
              })

              d3.selectAll(".btn-xss-mini").style("color", "white")  



            })
          
          } else {

            // Mode
            county_mode     = true
            county_selected = counties(county_data_crosswalk_current, state_choice)[agg_county]



            // Loading opacity
            county_data_current.forEach(function(d,i) {
              d3.selectAll("#svg_graph_"+i)
                .attr("opacity","0.3")
            })

            d3.selectAll("#svg_bar")
              .attr("opacity","0.3")
 
            d3.selectAll("#svg_map")
              .attr("opacity","0.3")

            // Update brush
            d3.selectAll("#brush_-1")
              .attr("x1", xScale_main(relax_date))
              .attr("x2", xScale_main(relax_date))

            d3.selectAll("#brush_label_-1")
              .attr("y", (xScale_main(relax_date))-1.5*(xScale_main(timeConv(relax_date_default_plus))-
                              xScale_main(timeConv(relax_date_default_minus))))

            county_data_current.forEach(function(d,i) {
              d3.selectAll("#brush_"+i)
                .attr("x1", xScale_mini(relax_date))
                .attr("x2", xScale_mini(relax_date))
              
              d3.selectAll("#brush_label_"+i)
                .attr("y", (xScale_mini(relax_date))+
                              padding_hor_main-1.5*(xScale_mini(timeConv(relax_date_default_plus))-
                              xScale_mini(timeConv(relax_date_default_minus))))

            })
            
            county_data_current[county_data_current.length-1]=county_data_current[agg_county]
            d3.selectAll("#button_mode_compare_div")
               .style("opacity",1)

            d3.selectAll("#button_mode_compare")   
               .on("click", function() {
                    county_index = counties( county_data_crosswalk_current, state_choice,lowercase=false).indexOf(county_selected)
                    target       = "#svg_graph_"+(county_index+1);
                    $('html,body').animate({scrollTop: $(target).offset().top},'slow'); 
                  
                    county_data_current.forEach(function(d,i) {
                        d3.selectAll("#svg_graph_"+i)
                          .attr("opacity","0.3")
                          .transition()
                          .delay(200)
                          .duration(2000)
                          .attr("opacity","1")
                      })

                      d3.selectAll("#svg_graph_"+(county_index+1))
                      .attr("opacity","1")

              })
              .style("cursor","pointer")


              // Loading opacity
              county_data_current.forEach(function(d,i) {
                d3.selectAll("#svg_graph_"+i)
                 .attr("opacity","1")
              })

              d3.selectAll("#svg_bar")
                .attr("opacity","1")
 
              d3.selectAll("#svg_map")
                .attr("opacity","1")

              
              // Update title
              d3.selectAll("#RHStitle")
                .html("Infection projections for " + county_selected + ", " + state_choice +   "<tspan id='download_icon' class='fa fa-sm'>&nbsp;&nbsp;&nbsp;\uf381</tspan>")
              
              d3.selectAll("#download_icon")
                .style("dominant-baseline", "central")
                .on("mouseover", function() {

                 d3.selectAll("#div_stat_0").html("Click to download the infection projection data for " + state_conv(county_data_crosswalk_current, state_choice,"name"))           

                })
                .on("mouseout", function() {

                if (windowWidth>mobileWidth) {
                  d3.selectAll("#div_stat_0").html("Move your cursor over the graph to see details or hover over the info icon to learn more")           
                } else {
                  d3.selectAll("#div_stat_0").html("Tap on and move over the graph to see details or click on the info icon to learn more")           
                } 

                })

                config = {
                  quotes: true, 
                  quoteChar: '"',
                  escapeChar: '"',
                  delimiter: ",",
                  header: false,
                  newline: "\r\n",
                  skipEmptyLines: false, 
                  columns: null 
                }

              d3.selectAll("#download_icon")
                 .on("click", function() {
                  blob       = new Blob([headersVal + Papa.unparse(InitializeDownload(county_data_raw_master), config)], {type: "text/csv"});
                  url        = window.URL.createObjectURL(blob);
                  a          = document.createElement("a");
                  a.href     = url;
                 a.download = "covidalliance_infectiontrajectories_" + state_choice + "_" + today +".csv";
                  a.click();
                })

              // Update axes
              graphsetup = InitializeGraph()
              
              // Main scale
              if ((graphsetup.maxValue_main > yScale_main.domain()[1]) | (graphsetup.maxValue_main < 0.5* yScale_main.domain()[1])) {
                graphsetup.maxValue_main = graphsetup.maxValue_main

              } else {

                graphsetup.maxValue_main = yScale_main.domain()[1]
              }

              // Mini scale
              county_data_current.slice(0,-1).forEach(function(d,i) {
                if ((graphsetup.maxValue_mini[i] > yScale_mini_array[i].domain()[1])| (graphsetup.maxValue_mini[i] < 0.5* yScale_mini_array[i].domain()[1])) {
                  graphsetup.maxValue_mini[i] = graphsetup.maxValue_mini[i]

                } else {

                  graphsetup.maxValue_mini[i] = yScale_mini_array[i].domain()[1]
                } 
              })           

              // Helper function
              function UpdateVisualizationHelper(svg_id) {

                // Initialize
                if (svg_id==-1) {
               
                  yScale_main.domain([graphsetup.minValue,graphsetup.maxValue_main]).ticks(10)
              
                  xScale_temp = xScale_main
                  yScale_temp = yScale_main

                } else {
                  
                  if (data_column_current=="cumulative") {
                    yScale_mini_array[svg_id].domain([graphsetup.minValue,graphsetup.maxValue_mini[svg_id]]).ticks(5)
                  } else {
                    yScale_mini_array[svg_id].domain([graphsetup.minValue,graphsetup.maxValue_mini[svg_id]]).ticks(3)
                   
                  }
                  xScale_temp = xScale_mini
                  yScale_temp = yScale_mini_array[svg_id]
                }
                
                yaxis = d3.axisLeft().scale(yScale_temp)
           
                // Update graph
                line = d3.line()
                         .x(function(d) { return xScale_temp(d.date) })
                         .y(function(d) { return yScale_temp(d[data_column_current])})
 
                line_baseline = d3.line()
                         .x(function(d) { return xScale_temp(d.date) })
                         .y(function(d) { return yScale_temp(d["baseline_"+data_column_current])})

                line_actual = d3.line()
                         .x(function(d) { return xScale_temp(d.date) })
                         .y(function(d) { return yScale_temp(d["actual_"+data_column_current])})
              
                yaxis_graph = d3.selectAll("#svg_graph_"+(svg_id+1))
                  .selectAll(".y")
                  .call(yaxis)

                yaxis_graph
                  .selectAll('text')
                  .attr("text-anchor","start")

                yaxis_graph
                  .selectAll('path')
                  .style("opacity",0)

                yaxis_graph
                 .selectAll('line')
                  .style("opacity",0)


                d3.selectAll("#svg_graph_"+(svg_id+1))
                  .selectAll(".line") 
                  .data(function() {
                    if (svg_id==-1) {
                      return([county_data_current[county_data_current.length-1]])
                    } else {
                      return([county_data_current[svg_id]])
                    }
                  })         
                  .transition()
                  .duration(500)
                  .attr("d", function(d) { 
                    return line(d.values.filter(function(e) {
                      return(xScale_main(e.date)>xScale_main(timeConv(today)))
                    })) 
                  }) 
      
                 d3.selectAll("#svg_graph_"+(svg_id+1))
                   .selectAll(".ghost-line-baseline")  
                   .data(function() {
                    if (svg_id==-1) {
                      return([county_data_current[county_data_current.length-1]])
                    } else {
                      return([county_data_current[svg_id]])
                    }
                  })   
                   .transition()
                   .duration(500)
                   .attr("d", function(d) { 
                    return line_baseline(d.values) })

                 d3.selectAll("#svg_graph_"+(svg_id+1))
                   .selectAll(".ghost-line-actual") 
                   .data(function() {
                    if (svg_id==-1) {
                      return([county_data_current[county_data_current.length-1]])
                    } else {
                      return([county_data_current[svg_id]])
                    }
                  })    
                   .transition()
                   .duration(500)
                    .attr("d", function(d) { return line_actual(d.values.filter(function(e) {return (e.actual_daily!=-1)})) }) 

              }

              // Execute update
              UpdateVisualizationHelper(svg_id=-1)
              county_data_current.slice(0,-1).forEach(function(d,i) {
                UpdateVisualizationHelper(svg_id=i)

              d3.selectAll(".btn-xss-mini").style("color", "white")  



            })
          }

        }

          // ----------------------------------------
          // ### EXECUTE
          // ----------------------------------------

          // Initialize the data
          county_data_raw_master = county_data_raw
          county_data_master     = jQuery.extend(true, [], county_data_raw_master)
          county_data_master     = InitializeData(raw_data=county_data_master)
          map_data_current       = map_data_raw

          config = {
                  quotes: true, 
                  quoteChar: '"',
                  escapeChar: '"',
                  delimiter: ",",
                  header: false,
                  newline: "\r\n",
                  skipEmptyLines: false, 
                  columns: null 
                }




          // Update parameters
          relax_date_current = timeConv(relax_date_default)
          
          // Scale the data
          county_data_current = jQuery.extend(true, [], county_data_master)

          // Initialize the axes
          county_data_current.slice(0,-1).forEach(function(d,i) {
            yScale_mini_array.push(yScale_mini)
          })

          // Update any parameters
          rt_values = []
          county_data_current.slice(0,county_data_current.length-1).forEach(function(d) {
                   rt_values.push(d.values[bisect(county_data_current[county_data_current.length-1].values, timeConv(today), 1)].r_t)
          })

          color_min     = d3.min(rt_values)
          color_min_max = 1
          color_max_min = 1.0000000000000001
          color_max     = d3.max(rt_values)
             step_good.range([color_min, color_min_max])
                step_bad.range([color_max_min, color_max])
                color_good.domain([step_good(1), step_good(2), step_good(3), color_min])
                color_bad.domain([color_max, step_bad(3), step_bad(2), step_bad(1)])

          // Draw 
          DrawVisualization_static()

          // Update
          init = 1

        }) 
      })
    })
  })
})